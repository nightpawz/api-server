<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeContentFieldTypeToTelescopeEntriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('telescope_entries', function (Blueprint $table) {
            DB::statement('ALTER TABLE `telescope_entries` MODIFY COLUMN `content` MEDIUMTEXT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('telescope_entries', function (Blueprint $table) {
            DB::statement('ALTER TABLE `telescope_entries` MODIFY COLUMN `content` TEXT');
        });
    }
}
