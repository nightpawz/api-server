<?php

namespace App\Providers\View;

use Illuminate\Support\ServiceProvider;
use Blade;

class AdminServiceProvider extends ServiceProvider
{
    /**
     * Conpoment name prefix.
     *
     * @var string
     */
    protected $name_prefix = 'fe';

    /**
     * The folder location for components views.
     *
     * @var string
     */
    protected $view_component_folder = 'admin._components';

    /**
     * The folder location for includes views.
     *
     * @var string
     */
    protected $view_include_folder = 'admin._includes';

    /**
     * An array of views and the component name.
     * Defined as $view => $name array
     *
     * @var array
     */
    protected $components = [
    ];

    /**
     * An array of views and the includes name.
     * Defined as $view => $name array
     *
     * @var array
     */
    protected $includes = [

    ];


    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerComponentAlias();

        $this->registerIncludeAlias();
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Register view component alias.
     *
     * @return void
     */
    public function registerComponentAlias()
    {
        foreach($this->components as $view => $name) {
            Blade::component("{$this->view_component_folder}.{$view}", "{$this->name_prefix}{$name}");
        }
    }

    /**
     * Register view include alias.
     *
     * @return void
     */
    public function registerIncludeAlias()
    {
        Blade::include("admin.pages._sections._section-form", "sectionform");

        foreach($this->includes as $view => $name) {
            Blade::include("{$this->view_include_folder}.{$view}", "{$this->name_prefix}{$name}");
        }
    }
}
