<?php

namespace App\Providers\View;

use Illuminate\Support\ServiceProvider;
use Blade;

class WebServiceProvider extends ServiceProvider
{
    /**
     * Conpoment name prefix.
     *
     * @var string
     */
    protected $name_prefix = 'w';

    /**
     * The folder location for components views.
     *
     * @var string
     */
    protected $view_component_folder = 'web._components';

    /**
     * The folder location for includes views.
     *
     * @var string
     */
    protected $view_include_folder = 'web._includes';

    /**
     * An array of views and the component name.
     * Defined as $view => $name array
     *
     * @var array
     */
    protected $components = [
        'carousel.index' => 'carousel',
        'carousel.carousel-item' => 'carouselitem',
        'swiper.index' => 'swiper',
        'swiper.swiper-slide' => 'swiperslide',
        'section.index' => 'section',
        'cards.card-icon' => 'cardicon',
        'cards.card-listing' => 'cardlisting'
    ];

    /**
     * An array of views and the includes name.
     * Defined as $view => $name array
     *
     * @var array
     */
    protected $includes = [
        'input' => 'input',
        'img' => 'img',
        'img-logo' => 'imglogo'
    ];


    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerComponentAlias();

        $this->registerIncludeAlias();
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Register view component alias.
     *
     * @return void
     */
    public function registerComponentAlias()
    {
        foreach($this->components as $view => $name) {
            Blade::component("{$this->view_component_folder}.{$view}", "{$this->name_prefix}{$name}");
        }
    }

    /**
     * Register view include alias.
     *
     * @return void
     */
    public function registerIncludeAlias()
    {
        foreach($this->includes as $view => $name) {
            Blade::include("{$this->view_include_folder}.{$view}", "{$this->name_prefix}{$name}");
        }
    }
}
