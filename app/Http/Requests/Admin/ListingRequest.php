<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class ListingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'listing.category_id' => 'required|integer',
            'listing.name' => 'required|string|max:191',
            'listing.translations.en.description' => 'required|string',
            'listing.translations.id.description' => 'required|string',
            'listing.email' => 'required|string|max:191',
            'listing.logo' => 'mimes:png,jpeg,jpg|max:512',
            'owner.id' => 'integer|nullable',
            'deal.sales_id' => 'integer|nullable',
            'deal.price_list_id' => 'required|integer',
            'deal.price' => 'integer|nullable',
            'deal.custom_price' => 'string|nullable'
        ];
    }
}
