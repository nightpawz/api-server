<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Modules\JB\Entities\{Category, Listing};
use Modules\Eris\Entities\Page;
use App\Http\Requests\MessageRequest;

class PageController extends Controller
{
    /**
     * Show the home page.
     *
     * @return Response
     */
    public function index()
    {
        return view('web.pages.index', [
            'categories' => Category::withCount('childs')->onlyParents()->take(8)->get(),
            'latest_listings' => Listing::latest()->take(6)->get(),
            'page' => Page::where('view', 'home')->firstOrFail()
        ]);
    }

    public function about()
    {
        return view('web.pages.index', [
            'page' => Page::where('view', 'about')->firstOrFail()
        ]);
    }

    public function contact()
    {
        return view('web.pages.index', [
            'page' => Page::where('view', 'contact')->firstOrFail()
        ]);
    }

    public function storeMessage(MessageRequest $request)
    {
        Message::create($request->all());

        flash(__('Your message is being sent.'))->success();

        return redirect()->route('contact');
    }
}
