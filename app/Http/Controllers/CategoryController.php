<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Modules\JB\Entities\{Category, Listing};
use Modules\Eris\Entities\{Page};

class CategoryController extends Controller
{
    /**
 	 * Show listing of resource.
 	 *
 	 * @return Response
	 */
    public function index()
    {
        return view('web.pages.index', [
            'categories' => Category::onlyParents()->withCount('childs')->get(),
            'page' => Page::where('view', 'category')->firstOrFail()
        ]);
    }

    /**
 	 * Show the specified resource.
 	 *
 	 * @param string $slug
 	 * @return Response
	 */
    public function show($slug)
    {
        return view('web.categories.show', [
            'category' => Category::whereTranslation('slug', $slug)->withCount('listings')->with('childs')->firstOrFail()
        ]);
    }
}
