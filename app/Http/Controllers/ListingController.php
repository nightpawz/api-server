<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Modules\JB\Entities\{Listing, Category};
use Modules\Eris\Entities\Page;

class ListingController extends Controller
{
    public function index(Request $request)
    {
        $listings = new Listing;

        if($request->location) {
          $listings = $listings->whereCity($request->location);
        }

        if($request->category) {
          $listings = $listings->whereCategory($request->category);
        }

        if($request->search) {
          $listings = $listings->search($request->search);
        }

        return view('web.listings.index', [
            'listings' => $listings->latest()->paginate(20)
        ]);
    }

    public function category($slug)
    {
        return view('web.listings.category', [
            'category' => Category::whereTranslation('slug', $slug)->firstOrFail()
        ]);
    }

    public function show($slug)
    {
        return view('web.listings.show', [
            'listing' => Listing::where('slug', $slug)->firstOrFail(),
            'page' => Page::where('view', 'home')->firstOrFail()
        ]);
    }
}
