<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\JB\Entities\{Listing, Category, PriceList, Deal};
use App\Http\Requests\Admin\{ListingRequest};
use Yajra\Datatables\Datatables;
use App\User;

class ListingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.listings.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.listings.create', [
            'categories' => Category::all(),
            'owner' => User::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ListingRequest $request)
    {
        $listing = Listing::create($request->listing);

        $this->updateListingMedia($listing, $request);

        // $price_list = PriceList::findOrFail($request->input('deal.price_list_id'));
        //
        // $deal = Deal::create([
        //     'sales_id' => $request->input('deal.sales_id'),
        //     'sales_name' => User::find($request->input('deal.sales_id'))->name ?? null,
        //     'item_name' => "Register listing {$price_list->duration} {$price_list->unit}(s) - {$listing->name} ({$listing->no_contract})",
        //     'item_price' => $price_list->price,
        //     'dealable_type' => 'Modules\JB\Entities\Listing',
        //     'dealable_id' => $listing->id
        // ]);

        // $listing->register_date = now();
        // $listing->expired_date = now()->addMonths($price_list->duration);

        $listing->fill($request->input('listing.translations'));
        $listing->save();

        flash(__('Record Added!'))->success();

        return redirect()->route('admin.listings.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Listing $listing)
    {
        return view('admin.listings.edit', [
            'listing' => $listing
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Listing $listing)
    {
        $listing->update($request->listing);
        $listing->fill($request->input('listing.translations'));

        $this->updateListingMedia($listing, $request);

        $listing->save();

        // $deal = Deal::find($request->deal_id);
        // $deal->update($request->deal);
        // $deal->save();

        flash(__('Record Updated!'))->success();

        return redirect()->route('admin.listings.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Listing $listing)
    {
        $listing->delete();

        flash(__('Record Deleted!'))->success();

        return redirect()->route('admin.listings.index');
    }

    public function updateListingMedia($model, $request)
    {
        if($request->input('listing.banner')) {
            $model->updateMedia($request->input('listing.banner'), 'banner');
        }

        if($request->input('listing.thumbnail')) {
            $model->updateMedia($request->input('listing.thumbnail'), 'thumbnail');
        }

        if($request->hasFile('listing.logo')) {
            $model->updateMedia($request->file('listing.logo'), 'logo');
        }

        $model->save();
    }

    /**
 	 * Show resource data parsed in datatable format.
 	 *
 	 * @param type
 	 * @return void
	 */
     public function datatable()
     {
         $listings = Listing::latest()->get();

         return Datatables::of($listings)

            ->addColumn('thumbnail', function($listing) {
                return "
                    <img src='{$listing->thumbnail_url}'>
                ";
            })

            ->editColumn('name', function($listing) {
                return "
                    <p class='mb-0'>{$listing->name}</p>
                    <strong><small>" . __('Category') . " : {$listing->category->name}</small></strong>
                ";
            })

            ->editColumn('category', function($listing) {
                return $listing->category->name;
            })

            ->editColumn('owner', function($listing) {
                if($listing->owner) {
                    return "{$listing->owner->name} ({$listing->owner->email})";
                } else {
                    return "";
                }
            })

            ->addColumn('contact', function($listing) {
                $str = "";

                if($listing->phone) {
                    $str.= "<a href='tel:{$listing->phone}' class='ui secondary'><i class='w-20 fa fa-phone'></i> {$listing->phone}</a>";
                }

                if($listing->email) {
                    $str.= "<br><a href='mailto:{$listing->email}' class='ui secondary'><i class='w-20 fa fa-envelope-o'></i> {$listing->email}</a>";
                }

                return $str;
            })

            ->addColumn('action', function ($listing) {
                return '
                    <a href="'.route('admin.listings.edit', $listing).'" class="ui primary basic button"><i class="fa fa-pencil"></i></a>
                    <a href="'.route('admin.listings.destroy', $listing).'" class="ui negative basic button dt__delete"><i class="fa fa-trash"></i></a>
                ';
            })

            ->addIndexColumn()
            ->rawColumns(['thumbnail', 'action', 'contact', 'name'])
            ->make(true);
     }


}
