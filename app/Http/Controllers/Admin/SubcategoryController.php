<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\JB\Entities\{Listing, Category};
use Yajra\Datatables\Datatables;

class SubcategoryController extends Controller
{
    /**
 	 * Show resource data parsed in datatable format.
 	 *
 	 * @param type
 	 * @return void
	 */
     public function datatable(Request $request)
     {
         $collections = Category::withCount(['listings'])->latest()->where('parent_id', $request->parent_id)->get();
         return Datatables::of($collections)

             ->addColumn('thumbnail', function ($item) {
                 return "<img class='thumbnai' src='{$item->image_url}'>";
             })

             ->editColumn('name', function($item) {
                 return "<p>{$item->name}</p><small>{$item->listings_count} " . __('Listings') . "</small>";
             })

            ->addColumn('action', function ($item) {
                return '
                    <a href="'.route('admin.categories.edit', $item).'" class="ui primary basic button"><i class="fa fa-pencil"></i></a>
                    <a href="'.route('admin.categories.destroy', $item).'" class="ui negative basic button dt__delete"><i class="fa fa-trash"></i></a>
                ';
            })

            ->addIndexColumn()
            ->rawColumns(['action', 'thumbnail', 'name'])
            ->make(true);
     }
}
