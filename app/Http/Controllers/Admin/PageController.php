<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\JB\Entities\{Category, Listing};
use Modules\Eris\Entities\Page;
use App\Http\Requests\MessageRequest;

class PageController extends Controller
{
    public function home()
    {
        return view('admin.pages.index', [
            'categories' => Category::withCount('childs')->onlyParents()->take(8)->get(),
            'latest_listings' => Listing::latest()->take(6)->get(),
            'page' => Page::where('view', 'home')->firstOrFail()
        ]);
    }

    public function category()
    {
        return view('admin.pages.index', [
            'page' => Page::where('view', 'category')->firstOrFail()
        ]);
    }

    public function about()
    {
        return view('admin.pages.index', [
            'page' => Page::where('view', 'about')->firstOrFail()
        ]);
    }

    public function contact()
    {
        return view('admin.pages.index', [
            'page' => Page::where('view', 'contact')->firstOrFail()
        ]);
    }

    public function update(Request $request, Page $page)
    {
        $page->update($request->all());

        if($request->image) {
            $page->updateMedia($request->image);
        }

        flash(__('Record Updated!'))->success();

        return redirect()->route("admin.pages.{$page->view}");
    }
}
