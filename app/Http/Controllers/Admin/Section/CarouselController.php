<?php

namespace App\Http\Controllers\Admin\Section;

use Illuminate\Http\Request;

class CarouselController extends BaseController
{
    protected $view_folder = 'carousel';

    protected $page_slug = 'home';
}
