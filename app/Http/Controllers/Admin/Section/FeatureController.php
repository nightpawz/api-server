<?php

namespace App\Http\Controllers\Admin\Section;

use Illuminate\Http\Request;

class FeatureController extends BaseController
{
    protected $view_folder = 'feature';

    protected $page_slug = 'home';
}
