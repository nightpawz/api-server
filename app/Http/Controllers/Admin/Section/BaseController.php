<?php

namespace App\Http\Controllers\Admin\Section;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\Eris\Entities\Section;

class BaseController extends Controller
{
    protected $page_slug;
    protected $view_folder;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view("admin.pages._sections._resources.{$this->view_folder}.create", [
            'page_id' => $request->input('page_id', null),
            'parent_id' => $request->input('parent_id', null)
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $section = Section::create($request->all());

        if($request->image) {
            $section->updateMedia($request->image);
        }

        $section->fill($request->translations);

        $section->save();

        flash(__("Record Added"))->success();

        return redirect()->route("admin.pages.{$this->page_slug}");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Section $section)
    {
        return view("admin.pages._sections._resources.{$this->view_folder}.edit", [
            'page_id' => $section->page_id,
            'parent_id' => $section->parent_id,
            'section' => $section
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Section $section)
    {
        $section->update($request->all());

        if($request->image) {
            $section->updateMedia($request->image);
        }

        $section->fill($request->translations);

        $section->save();

        flash(__("Record Updated"))->success();

        return redirect()->route("admin.pages.{$this->page_slug}");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Section $section)
    {
        $section->delete();

        flash(__('Record Deleted!'))->success();

        return redirect()->route("admin.pages.{$this->page_slug}");
    }
}
