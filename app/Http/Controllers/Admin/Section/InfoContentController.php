<?php

namespace App\Http\Controllers\Admin\Section;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InfoContentController extends BaseController
{
    protected $view_folder = 'info';

    protected $page_slug = 'about';
}
