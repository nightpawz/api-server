<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Modules\JB\Entities\Member;
use App\Http\Requests\UserRequest;

class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.members.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.members.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Member::create($request->all());

        flash(__('Record Added!'))->success();

        return redirect()->route('admin.members.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Member $member)
    {
        return view('admin.members.edit', [
            'member' => $member
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Member $member)
    {
        $member->update($request->all());

        flash(__('Record Updated!'))->success();

        return redirect()->route('admin.members.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Member $member)
    {
        $member->delete();

        flash(__('Record Deleted!'))->success();

        return redirect()->route('admin.members.index');
    }

    /**
 	 * Show resource data parsed in datatable format.
 	 *
 	 * @param type
 	 * @return void
	 */
     public function datatable(Request $request)
     {
         $collections = Member::latest();

         return Datatables::of($collections->get())

            ->addColumn('action', function ($item) {
                return '
                    <a href="'.route('admin.members.show', $item).'" class="ui secondary basic button"><i class="fa fa-bars"></i></a>
                    <a href="'.route('admin.members.edit', $item).'" class="ui primary basic button"><i class="fa fa-pencil"></i></a>
                    <a href="'.route('admin.members.destroy', $item).'" class="ui negative basic button dt__delete"><i class="fa fa-trash"></i></a>
                ';
            })

            ->addIndexColumn()
            ->rawColumns(['action', 'thumbnail'])
            ->make(true);
     }
}
