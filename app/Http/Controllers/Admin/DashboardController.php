<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    /**
 	 * Show the dashboard page.
 	 *
 	 * @return Response
	 */
	public function index()
    {
        return view('admin.dashboard.index');
    }
}
