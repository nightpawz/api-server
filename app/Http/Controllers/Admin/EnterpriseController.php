<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\JB\Entities\{Enterprise, Listing};
use Yajra\Datatables\Datatables;

class EnterpriseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('admin.enterprises.create', [
            'listings' => Listing::latest()->get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Enterprise::create($request->all());

        return redirect()->route('admin.pages.home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Enterprise $enterprise)
    {
        return view('admin.enterprises.edit', [
            'listings' => Listing::latest()->get(),
            'enterprise' => $enterprise
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Enterprise $enterprise)
    {
        $enterprise->update($request->all());

        return redirect()->route('admin.pages.home');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Enterprise $enterprise)
    {
        $enterprise->update($request->all());

        return redirect()->route('admin.pages.home');
    }

    /**
 	 * Show resource data parsed in datatable format.
 	 *
 	 * @param type
 	 * @return void
	 */
     public function datatable()
     {
         $collections = Enterprise::with(['listing', 'listing.owner'])->latest()->get();
         return Datatables::of($collections)

            ->editColumn('listing', function($item) {
                $listing = $item->listing;
                $owner = $listing->owner;
                $t = "<p class='mb-0'>{$listing->name}</p>";

                if($owner) {
                    $t .= "<strong><small>" . __('Owner') . " : {$owner->name}</small></strong>";
                }

                return $t;
            })

            ->addColumn('action', function ($listing) {
                return '
                    <a href="'.route('admin.enterprises.edit', $listing).'" class="ui primary basic button"><i class="fa fa-pencil"></i></a>
                    <a href="'.route('admin.enterprises.destroy', $listing).'" class="ui negative basic button dt__delete"><i class="fa fa-trash"></i></a>
                ';
            })

            ->addIndexColumn()
            ->rawColumns(['action', 'listing', 'name'])
            ->make(true);
     }
}
