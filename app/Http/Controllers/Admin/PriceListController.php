<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\JB\Entities\PriceList;
use Yajra\Datatables\Datatables;

class PriceListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.price-lists.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.price-lists.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        PriceList::create($request->all());

        flash(__('Record Added!'))->success();

        return redirect()->route('admin.price-lists.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(PriceList $price_list)
    {
        return view('admin.price-lists.edit', [
            'price_list' => $price_list
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(PriceList $price_list)
    {
        return view('admin.price-lists.edit', [
            'price_list' => $price_list
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PriceList $price_list)
    {
        $price_list->update($request->all());

        flash(__('Record Updated!'))->success();

        return redirect()->route('admin.price-lists.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(PriceList $price_list)
    {
        $price_list->delete();

        flash(__('Record Deleted!'))->success();

        return redirect()->route('admin.price-lists.index');
    }

    /**
 	 * Show resource data parsed in datatable format.
 	 *
 	 * @param type
 	 * @return void
	 */
     public function datatable()
     {
         $collections = PriceList::all();

         return Datatables::of($collections)

            ->editColumn('price', function($item) {
                return format_idr($item->price);
            })

            ->addColumn('action', function($item) {
                return '
                    <a href="'.route('admin.price-lists.edit', $item).'" class="ui primary basic button"><i class="fa fa-pencil"></i></a>
                    <a href="'.route('admin.price-lists.destroy', $item).'" class="ui negative basic button dt__delete"><i class="fa fa-trash"></i></a>
                ';
            })

            ->addIndexColumn()
            ->rawColumns(['action'])
            ->make(true);
     }
}
