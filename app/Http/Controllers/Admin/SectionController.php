<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\Eris\Entities\Section;

class SectionController extends Controller
{
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Section $section)
    {
        $section->update($request->all());

        if($request->image) {
            $section->updateMedia($request->image);
        }
        
        $section->fill($request->translations);

        $section->save();

        flash(__("Record Updated"))->success();

        return redirect()->route("admin.pages.{$section->page->view}");
    }
}
