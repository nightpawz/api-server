<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\JB\Entities\{Listing, Category};
use Yajra\Datatables\Datatables;
use Modules\JB\Http\Requests\CategoryRequest;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.categories.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('admin.categories.create', [
            'categories' => Category::onlyParents()->get(),
            'parent' => Category::find($request->input('parent_id', ''))
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {
        $category = Category::create($request->all());

        if($request->image) {
            $category->updateMedia($request->image, 'image');
        }

        if($request->hasFile('icon')) {
            $category->updateMedia($request->file('icon'), 'icon', 'png');
        }

        $category->fill($request->translations);

        $category->save();

        flash(__('Record Added!'))->success();

        return redirect()->route('admin.categories.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        return view('admin.categories.show', [
            'category' => $category
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        return view('admin.categories.edit', [
            'category' => $category,
            'categories' => Category::onlyParents()->get()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryRequest $request, Category $category)
    {
        $category->update($request->all());

        if($request->image) {
            $category->updateMedia($request->image, 'image');
        }

        if($request->hasFile('icon')) {
            $category->updateMedia($request->file('icon'), 'icon', 'png');
        }

        $category->fill($request->translations);

        $category->save();

        return redirect()->route('admin.categories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        $category->delete();

        return redirect()->route('admin.categories.index');
    }

    /**
 	 * Show resource data parsed in datatable format.
 	 *
 	 * @param type
 	 * @return void
	 */
     public function datatable()
     {
         $collections = Category::withCount(['childs'])->latest()->onlyParents()->get();
         return Datatables::of($collections)

             ->addColumn('thumbnail', function ($item) {
                 return "<img class='thumbnai' src='{$item->image_url}'>";
             })

             ->editColumn('name', function($item) {
                 return "<p>{$item->name}</p><small>{$item->childs_count} " . __('Subcategories') . "</small>";
             })

            ->addColumn('action', function ($item) {
                return '
                    <a href="'.route('admin.categories.show', $item).'" class="ui secondary basic button"><i class="fa fa-bars"></i></a>
                    <a href="'.route('admin.categories.edit', $item).'" class="ui primary basic button"><i class="fa fa-pencil"></i></a>
                    <a href="'.route('admin.categories.destroy', $item).'" class="ui negative basic button dt__delete"><i class="fa fa-trash"></i></a>
                ';
            })

            ->addIndexColumn()
            ->rawColumns(['action', 'thumbnail', 'name'])
            ->make(true);
     }
}
