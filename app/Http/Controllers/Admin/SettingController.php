<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Helpers;

class SettingController extends Controller
{
    protected $fillable = [
        'app.name',
        'app.description'
    ];


    public function index()
    {
        return view('admin.settings.index');
    }

    public function store(Request $request)
    {
        if($request->hasFile('logo')) {
            $img = Helpers::image($request->file('logo'))->folder('/img/')->prefix('logo')->save();

            setting()->set('app.logo', $img);
        }

        foreach ($this->fillable as $key) {
            setting()->set($key, $request->input($key));
        }

        setting()->save();

        flash(__('Record Updated!'))->success();

        return redirect()->route('admin.settings.store');
    }
}
