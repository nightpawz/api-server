<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use App\{User, Role};
use App\Http\Requests\UserRequest;

class SalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.sales.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.sales.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        $user = User::create($request->all());

        Role::firstOrCreate([
            'name' => 'sales',
            'display_name' => 'Sales',
            'description' => 'desc'
        ]);

        $user->attachRole('sales');

        flash(__('Record Added!'))->success();

        return redirect()->route('admin.sales.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.sales.edit', [
            'sales' => User::findOrFail($id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
 	 * Show resource data parsed in datatable format.
 	 *
 	 * @param type
 	 * @return void
	 */
     public function datatable(Request $request)
     {
         $collections = User::latest();

         if($request->has('role')) {
             $collections = $collections->whereHas('roles', function($q) use ($request) {
                return $q->where('name', $request->role);
             });
         }
         return Datatables::of($collections->get())

             ->addColumn('thumbnail', function ($item) {
                 return "<img class='thumbnai' src='{$item->image_url}'>";
             })

            ->addColumn('action', function ($item) {
                return '
                    <a href="'.route('admin.sales.show', $item).'" class="ui secondary basic button"><i class="fa fa-bars"></i></a>
                    <a href="'.route('admin.sales.edit', $item).'" class="ui primary basic button"><i class="fa fa-pencil"></i></a>
                    <a href="'.route('admin.sales.destroy', $item).'" class="ui negative basic button dt__delete"><i class="fa fa-trash"></i></a>
                ';
            })

            ->addIndexColumn()
            ->rawColumns(['action', 'thumbnail'])
            ->make(true);
     }
}
