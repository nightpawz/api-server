<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\JB\Entities\{Category, Listing};

class PhantomController extends Controller
{
    public function categories()
    {
        return response()->json(Category::onlyParents()->get());
    }

    public function listings()
    {
        return response()->json(Listing::whereNotNull('email')->get());
    }

    public function updateListing(Request $request)
    {
        $listing = Listing::where('ref_id', $request->ref_id)->firstOrFail();

        $listing->update($request->listing);

        $listing->fill($request->input('listing.translations', []));

        $listing->save();

        // foreach ($listing->features as $i) {
        //     $i->delete();
        // }

        // foreach ($request->features as $i) {
        //     $f = $listing->features()->create($i);
        //     $f->fill($i->transtations);
        //     $f->save();
        // }
    }

    public function subcategories()
    {
        $latest = Listing::latest()->first()->category_id;

        return response()->json(Category::onlyChilds()->where('id', '>=', $latest)->get());
    }

    public function storeCategory(Request $request)
    {
        if(!$this->categoryExists($request->name)) {
            $category = Category::create($request->all());

            if($request->image_url) {
                $category->updateMedia($request->image_url, 'image');
            }
        }
    }

    public function storeCategoryEn(Request $request)
    {
        $category = Category::find($request->id);

        $category->{'name:en'} = $request->name;

        $category->save();
    }

    public function storeListing(Request $request)
    {
        if(!$this->listingExists($request->ref_id)) {
            $listing = Listing::create($request->all());

            if($request->thumbnail_url) {
                $listing->updateMedia($request->thumbnail_url, 'thumbnail');
            }
        }
    }

    public function categoryExists($name)
    {
        return Category::whereTranslation('name', $name)->exists();
    }

    public function listingExists($id)
    {
        return Listing::where('ref_id', $id)->exists();
    }
}
