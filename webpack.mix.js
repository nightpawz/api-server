const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css')
   .version()
   .browserSync({
       proxy: "http://jb.imaginer.id",
       files: [
           'public/js/app.js',
           'public/css/app.css',
           'resources/views/**/*.php',
           'Modules/**/*.php',
           'public/ui/custom.css'
       ],
       open: false
   });
