<?php

namespace Modules\Socialable\Entities;

use Illuminate\Database\Eloquent\Model;

class Social extends Model
{
    protected $table = 'socials';

    protected $fillable = [
        'sociallist_id',
        'link',
        'socialable_id',
        'socialable_type'
    ];

    public function social_data()
    {
        return $this->belongsTo(Socialist::class, 'sociallist_id');
    }
}
