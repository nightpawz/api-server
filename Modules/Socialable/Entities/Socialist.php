<?php

namespace Modules\Socialable\Entities;

use Illuminate\Database\Eloquent\Model;

class Socialist extends Model
{
    protected $table = 'socialists';

    protected $fillable = [
        'name',
        'placeholder',
        'icon',
        'default'
    ];
}
