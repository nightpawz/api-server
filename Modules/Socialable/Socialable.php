<?php

namespace Modules\Socialable;

use Modules\Socialable\Entities\{Social, Socialist};

trait Socialable
{
    /**
     * Get all of the resource's media
     *
     * @return \Illuminate\Database\Eloquent\MorphMany
     */
    public function socials()
    {
        return $this->morphMany(Social::class, 'socialable');
    }
}
