<?php

namespace Modules\Socialable\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Socialable\Entities\Socialist;

class SocialableDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'facebook',
                'placeholder' => 'https://facebook.com/example',
                'icon' => 'fa fa-facebook',
                'format_link' => '[value]'
            ],
            [
                'name' => 'whatsapp',
                'placeholder' => '+6200 000 000',
                'icon' => 'fa fa-whatsapp',
                'format_link' => 'https://wa.me/[value]'
            ],
            [
                'name' => 'twitter',
                'placeholder' => 'https://twitter.com/example',
                'icon' => 'fa fa-twitter',
                'format_link' => '[value]'
            ],
            [
                'name' => 'instagram',
                'placeholder' => 'https://instagram.com/example',
                'icon' => 'fa fa-instagram',
                'format_link' => 'https://instagram.com/[value]'
            ]
        ];

        foreach ($data as $value) {
            Socialist::create($value);
        }
    }
}
