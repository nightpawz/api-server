<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSocialistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('socialists', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('placeholder')->nullable();
            $table->string('icon')->nullable();
            $table->string('default')->nullable();
            $table->string('format_link')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('socialists');
    }
}
