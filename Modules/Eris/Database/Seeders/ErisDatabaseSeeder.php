<?php

namespace Modules\Eris\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class ErisDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $this->call(SettingSeeder::class);
        $this->call(PageSeeder::class);
        $this->call(TeamSeeder::class);

    }
}
