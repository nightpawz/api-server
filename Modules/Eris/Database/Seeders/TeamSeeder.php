<?php

namespace Modules\Eris\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Eris\Entities\Team;
use LaravelLocalization;
use Faker\Factory;

class TeamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach(range(1,10) as $i) {
            $this->createData();
        }
    }

    /**
     * Create category data
     *
     * @return void
     */
    public function createData()
    {
        $faker = Factory::create();

        $model = Team::create([
            'name' => $faker->name,
            'job_title' => $faker->jobTitle,
            'description' => "<p>{$faker->paragraph}</p><p>{$faker->paragraph}</p>"
        ]);

        $this->createMedia($model);

    }

    public function createMedia($model)
    {
        $model->media()->create([
            'slug' => 'image',
            'filename' => '/img/mockup/team/image.jpg'
        ]);

    }
}
