<?php

namespace Modules\Eris\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Eris\Entities\{Page, Menu, Section};
use LaravelLocalization;

class PageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach(Page::all() as $page) {
            $page->delete();
        }

        $seeds = json_decode(file_get_contents(getcwd() . '/Modules/Eris/Database/Seeders/seeds/pages.json'), true);

        foreach($seeds['pages'] as $data) {
            $this->createPage($data);
        }
    }

    /**
     * Create page data.
     *
     * @param array $data
     * @return void
     */
    public function createPage($data)
    {
        $data_page = array_except($data, 'sections');

        $page = Page::create($data['translations']);

        $page->update(array_except($data, ['sections', 'translations']));

        // $menu = Menu::create(array_only($data['translations'], config('translatable.locales')));
        // $menu->target_url = LaravelLocalization::getNonLocalizedURL(route('pages.show', $page->slug));
        // $menu->increment('menu_order');
        // $menu->save();

        // if($menu->name === 'Home')
        //     $menu->delete();

        if(array_has($data, 'sections'))
            foreach($data['sections'] as $data) {
                $this->createSection($page, $data);
            }
    }

    /**
     * Create section data.
     *
     * @param Page $page
     * @param array $data
     * @return void
     */
    public function createSection(Page $page, $data, $parent_id = NULL)
    {
        if($parent_id)
            $data = array_add($data, 'parent_id', $parent_id);

        $section = $page->sections()->create(array_except($data, ['childs', 'image', 'title', 'subtitle']));

        if($section->name)
            $section->update([
                'view' => $page->slug .'.'. $this->getSectionView($section)
            ]);

        if(array_has($data, 'image'))
            $this->createMedia($section, $data['image']);

        if(array_has($data, 'childs'))
            foreach($data['childs'] as $data) {
                $this->createSection($page, $data, $section->id);
            }
    }

    /**
     * Create media data.
     *
     * @param Section $section
     * @param string $filename
     */
    public function createMedia(Section $section, $filename)
    {
        $folder = str_replace('.', '/', $this->getSectionView($section));

        $section->media()->create([
            'filename' => "img/mockup/{$folder}/{$filename}"
        ]);
    }

    public function getSectionView(Section $section, $view = '')
    {
        if($view !== '' && $section->name) {
            $view = $section->name. '.' . $view;
        } elseif($section->name) {
            $view = $section->name;
        }

        if($section->parent && $section->parent->name !== NULL)
            $view = $this->getSectionView($section->parent, $view);

        return $view;
    }
}
