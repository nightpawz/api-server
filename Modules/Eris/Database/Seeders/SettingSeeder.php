<?php

namespace Modules\Eris\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Setting;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $seeds = json_decode(file_get_contents(getcwd() . '/Modules/Eris/Database/Seeders/seeds/settings.json'), true);

        foreach($seeds['settings'] as $key => $data) {
            Setting::set($key, $data);
        }

        Setting::save();
    }
}
