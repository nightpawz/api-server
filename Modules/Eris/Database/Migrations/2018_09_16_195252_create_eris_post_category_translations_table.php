<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateErisPostCategoryTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eris_post_category_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('post_category_id')->unsigned()->index();
            $table->foreign('post_category_id')->references('id')->on('eris_post_categories')->onDelete('cascade');

            $table->string('locale');
            $table->string('name');
            $table->string('slug');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eris_post_category_translations');
    }
}
