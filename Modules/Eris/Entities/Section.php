<?php

namespace Modules\Eris\Entities;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;
use Modules\Mediable\Mediable;
use Modules\Eris\Traits\Section\SectionLogic;

class Section extends Model
{
    use Translatable, Mediable, SectionLogic;

    /**
     * The table of the model.
     *
     * @var string
     */
    protected $table = 'eris_sections';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'page_id',
        'parent_id',
        'name',
        'view',
        'section_order'
    ];

    /**
     * The attrobutes that are translatable.
     *
     * @var array
     */
    public $translatedAttributes = [
        'bg_text',
        'title',
        'subtitle',
        'content'
    ];

    /**
     * The attributes that are appended.
     *
     * @var array
     */
    protected $appends = [
        'image_url'
    ];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = [
        'media',
        'translations'
    ];

    /**
     * The accessor for image_url attribute.
     *
     * @return string
     */
    public function getImageUrlAttribute()
    {
        return asset($this->getMediaName());
    }

    /**
     * Define relationship with Page model.
     *
     * @return Illuminate\Database\Eloquent\Relationship\BelongsTo
     */
    public function page()
    {
        return $this->belongsTo(Page::class);
    }

    /**
     * Define childs relations
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function childs()
    {
        return $this->hasMany(Section::class, 'parent_id');
    }

    /**
     * Define parent relationship.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(Section::class, 'parent_id');
    }

    /**
     * Scope a query to only include parent categories.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOnlyParents($query)
    {
        return $query->where('parent_id', NULL);
    }
}
