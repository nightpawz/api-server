<?php

namespace Modules\Eris\Entities;

use Illuminate\Database\Eloquent\Model;

class TeamTranslation extends Model
{
    /**
     * The table of the model.
     *
     * @var string
     */
    protected $table = 'eris_team_translations';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'team_id',
        'description'
    ];

    /**
     * Disable laravel from automatically create timestamps.
     *
     * @var boolean
     */
    public $timestamps = false;

}
