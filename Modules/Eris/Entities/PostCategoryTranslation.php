<?php

namespace Modules\Eris\Entities;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class PostCategoryTranslation extends Model
{
    use Sluggable;

    /**
     * The table of the model.
     * 
     * @var string
     */
    protected $table = 'eris_post_category_translations';

    /**
     * The attributes that are mass assignable.
     * 
     * @var array
     */
    protected $fillable = [
        'post_category_id',
        'name',
        'slug',
        'locale'
    ];

    /**
     * Disable laravel from automatically create timestamps.
     * 
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Return the sluggable configuration for this Model.
     * 
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
}
