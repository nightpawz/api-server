<?php

namespace Modules\Eris\Entities;

use Illuminate\Database\Eloquent\Model;

class SectionTranslation extends Model
{
    /**
     * The table of the model.
     *
     * @var string
     */
    protected $table = 'eris_section_translations';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'section_id',
        'locale',
        'bg_text',
        'title',
        'subtitle',
        'content'
    ];

    /**
     * Disable laravel from automatically generate timestamps.
     *
     * @var bool
     */
    public $timestamps = false;
}
