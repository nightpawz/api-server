<?php

namespace Modules\Eris\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Mediable\Mediable;

class Client extends Model
{
    use Mediable;
    
    /**
     * The table of the model.
     * 
     * @var string
     */
    protected $table = 'eris_clients';

    /**
     * The attributes that are mass assignable.
     * 
     * @var array
     */
    protected $fillable = [
        'name',
        'title'
    ];

     /**
     * The attributes that are appended.
     * 
     * @var array
     */
    protected $appends = [
        'image_url'
    ];

    /**
     * The accessor for image_url attribute.
     * 
     * @return string
     */
    public function getImageUrlAttribute()
    {
        return asset($this->getMediaName());
    }

}
