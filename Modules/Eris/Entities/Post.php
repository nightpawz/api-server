<?php

namespace Modules\Eris\Entities;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;
use Modules\Mediable\Mediable;

class Post extends Model
{
    use Translatable, Mediable;

    /**
     * The table of the model.
     *
     * @var string
     */
    protected $table = 'eris_posts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'category_id'
    ];

    /**
     * The attributes that are translatable.
     *
     * @var array
     */
    public $translatedAttributes = [
        'title',
        'slug',
        'content'
    ];

    /**
     * The attributes that are appended.
     *
     * @var array
     */
    protected $appends = [
        'image_url',
        'excerpt'
    ];


    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = [
        'media',
        'translations'
    ];

     /**
     * The accessor for image_url attribute.
     *
     * @return string
     */
    public function getImageUrlAttribute()
    {
        return asset($this->getMediaName());
    }

    /**
     * Accessor for excerpt attribute
     *
     * @return string
     */
    public function getExcerptAttribute()
    {
        return str_limit(strip_tags($this->content), 100);
    }

    /**
     * Define ralationship with PostCategory model.
     *
     * @return Illumimate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(PostCategory::class, 'category_id');
    }
}
