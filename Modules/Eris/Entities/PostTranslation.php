<?php

namespace Modules\Eris\Entities;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class PostTranslation extends Model
{
    use Sluggable;

    /**
     * The table of the model.
     * 
     * @var string
     */
    protected $table = 'eris_post_translations';

    /**
     * The attributes that are mass assignable.
     * 
     * @var array
     */
    protected $fillable = [
        'post_id',
        'title',
        'slug',
        'locale',
        'content'
    ];

    /**
     * Disable laravel from automatically create timestamps.
     * 
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Return the sluggable configuration for this Model.
     * 
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}
