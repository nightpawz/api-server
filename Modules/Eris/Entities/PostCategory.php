<?php

namespace Modules\Eris\Entities;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;

class PostCategory extends Model
{
    use Translatable;

    /**
     * The table of the model.
     * 
     * @var string
     */
    protected $table = 'eris_post_categories';

    /**
     * The attributes that are mass assignable.
     * 
     * @var array
     */
    protected $fillable = [
        'parent_id'
    ];

    /**
     * The attributes that are translatable.
     * 
     * @var array
     */
    public $translatedAttributes = [
        'name',
        'slug'
    ];

    /**
     * Define childs relations
     * 
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function childs()
    {
        return $this->hasMany(PostCategory::class, 'parent_id');
    }

    /**
     * Define parent relationship.
     * 
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(PostCategory::class, 'parent_id');
    }

    /**
     * Define relationship with Post model.
     * 
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function posts()
    {
        return $this->hasMany(Post::class, 'category_id');
    }

    /**
     * Disable laravel from automatically create timestamps.
     * 
     * @var bool
     */
    public $timestamps = false;
}
