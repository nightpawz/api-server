<?php

namespace Modules\Eris\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Mediable\Mediable;

class Partner extends Model
{
    use Mediable;

    protected $table = 'eris_partners';

    protected $fillable = [
        'name',
        'site_url'
    ];

    protected $appends = [
        'image_url'
    ];

    /**
     * The accessor for image_url attribute.
     *
     * @return string
     */
    public function getImageUrlAttribute()
    {
        return asset($this->getMediaName());
    }
}
