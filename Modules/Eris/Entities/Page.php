<?php

namespace Modules\Eris\Entities;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;
use Modules\Mediable\Mediable;

class Page extends Model
{
    use Translatable, Mediable;

    /**
     * The table of the model.
     *
     * @var string
     */
    protected $table = 'eris_pages';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'is_static',
        'is_published',
        'view',
        'parent'
    ];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = [
        'media',
        'translations'
    ];

    /**
     * The attributes that are translatable.
     *
     * @var array
     */
    public $translatedAttributes = [
        'name',
        'slug',
        'title',
        'meta_title',
        'meta_description'
    ];

    protected $appends = [
        'image_url'
    ];

    /**
     * The accessor for image_url attribute.
     *
     * @return string
     */
    public function getImageUrlAttribute()
    {
        return asset($this->getMediaName());
    }

    /**
     * Define relationship with Section model.
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sections()
    {
        return $this->hasMany(Section::class);
    }

    /**
     * Find specified section
     *
     * @param string $name
     */
    public function findSection($name)
    {
        return $this->sections()->where('name', $name)->first();
    }
}
