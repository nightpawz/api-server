<?php

namespace Modules\Eris\Entities;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;
use Modules\Mediable\Mediable;

class Team extends Model
{
    use Translatable, Mediable;
    /**
     * The table of the model.
     *
     * @var string
     */
    protected $table = 'eris_teams';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'job_title'
    ];

    /**
     * The attributes that are translatable.
     *
     * @var array
     */
    public $translatedAttributes = [
        'description'
    ];

    /**
     * The attributes that are appended.
     *
     * @var array
     */
    protected $appends = [
        'image_url'
    ];

     /**
     * The accessor for image_url attribute.
     *
     * @return string
     */
    public function getImageUrlAttribute()
    {
        return asset($this->getMediaName());
    }

}
