<?php

namespace Modules\Eris\Entities;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class PageTranslation extends Model
{
    use Sluggable;

    /**
     * The table of the model.
     * 
     * @var string
     */
    protected $table = 'eris_page_translations';

    /**
     * The attributes that are mass assignable.
     * 
     * @var array
     */
    protected $fillable = [
        'page_id',
        'locale',
        'name',
        'slug',
        'title',
        'meta_title',
        'meta_description'
    ];

    /**
     * Configure laravel not to automatically 
     * create timestamps
     * 
     * @var bool
     */
    public $timestamps = false;

    /**
     * Return the sluggable configuration for this Model.
     * 
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
}
