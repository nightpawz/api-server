<?php

namespace Modules\Eris\Traits\Page;
use Modules\Eris\Entities\PageTranslation;

trait PageTranslationListener
{
    /**
     * The boot event of the model.
     * 
     * @return void
     */
    public static function boot()
    {
        parent::boot();

        /**
         * Listent to Page "creating" event to generate page's slug.
         * 
         * @param PageTranslation $page
         * @return void
         */
        PageTranslation::creating(function($page) {
            $page->slug = str_slug($page->name);
        });
    }
}