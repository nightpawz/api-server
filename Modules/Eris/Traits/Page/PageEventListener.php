<?php

namespace Modules\Eris\Traits\Page;
use Modules\Eris\Entities\Page;

trait PageEventListener
{
    /**
     * The boot event of the model.
     * 
     * @return void
     */
    public static function boot()
    {
        parent::boot();

        /**
         * Listent to Page "deleted" event to delete it's sections.
         * 
         * @param Page $page
         * @return void
         */
        Page::deleted(function($page) {
            foreach($page->sections as $section) {
                $section->delete();
            }
        });
    }
}