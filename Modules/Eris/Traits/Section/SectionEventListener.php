<?php

namespace Modules\Eris\Traits\Section;
use Modules\Eris\Entities\Section;

trait SectionEventListener
{
    /**
     * The boot event of the model.
     * 
     * @return void
     */
    public static function boot()
    {
        parent::boot();

        /**
         * Listent to Section "deleted" event to delete it's childs.
         * 
         * @param Section $section
         * @return void
         */
        Section::deleted(function($section) {
            foreach($section->childs as $child) {
                $child->delete();
            }
        });
    }
}
