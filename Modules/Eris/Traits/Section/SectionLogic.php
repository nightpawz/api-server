<?php

namespace Modules\Eris\Traits\Section;

trait SectionLogic
{
    /**
     * Find section's child by it's name.
     * 
     * @param string $name
     */
    public function findSection($name)
    {
        return $this->childs()->where('name', $name)->first();
    }
}