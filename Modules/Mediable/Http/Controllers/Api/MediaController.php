<?php

namespace Modules\Mediable\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Mediable\Entities\Media;
use Helpers;

class MediaController extends Controller
{
    public function upload(Request $request)
    {
        $image = Helpers::image($request->file('image'))->folder("/img/media/")->encode($request->input('encode', 'jpg'))->save();

        return response()->json([
            'url' => $image
        ]);
    }
}
