<?php

namespace Modules\Mediable\Entities;

use Illuminate\Database\Eloquent\Model;

class MediaTranslation extends Model
{
    /**
     * The attributes that are mass assignable.
     * 
     * @var array
     */
    protected $fillable = [
        'media_id',
        'title',
        'alt_text',
        'locale'
    ];

    /**
     * Disable laravel from automatically create timestamps.
     * 
     * @var bool
     */
    public $timestamps = false;
}
