<?php

namespace Modules\Mediable\Entities;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;
use File;

class Media extends Model
{
    use Translatable;

    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = [
        'filename',
        'mime_type',
        'slug',
        'mediable_id',
        'mediable_type',
    ];

    /**
     * The attributes that are appended
     *
     * @var array
     */
    protected $appends = [
        'url'
    ];

    /**
     * The attributes that are translatable.
     *
     * @var array
     */
    public $translatedAttributes = [
        'alt_text',
        'title'
    ];


    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = [
        'translations'
    ];

    /**
     * The accessor for url attribute
     *
     * @return string
     */
    public function getUrlAttribute()
    {
        return $this->filename ? asset($this->filename) : '';
    }

    /**
     * Scope a query to filter resource from given slug.
     *
     * @param string $slug
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWhereSlug($query, $slug)
    {
        if($slug ==='') return;
        return $query->where('slug', $slug);
    }

    /**
     * Add event listener
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();

        /**
         * Listen to the Media deleted event. Delete media files
         * from storage when a media is deleted
         *
         * @param \App\Media $media
         * @return void
         */
        Media::deleted(function($media) {
            $file = public_path() . $media->filename;

            if(File::isFile($file)) {
                File::delete($file);
            }
        });
    }
}
