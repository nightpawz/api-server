<?php

namespace Modules\Mediable;

use Modules\Mediable\Entities\Media;
use Helpers;

trait Mediable
{
    /**
     * Check if resource has media
     *
     * @return boolean
     */
    public function hasMedia(): bool
    {
        return $this->media->isNotEmpty();
    }

    /**
     * Get all of the resource's media
     *
     * @return \Illuminate\Database\Eloquent\MorphMany
     */
    public function media()
    {
        return $this->morphMany(Media::class, 'mediable');
    }

    /**
     * Get media url
     *
     * @param string $key
     * @return string
     */
    public function getMediaName($slug = '')
    {
        if(!$this->hasMedia()) return '';

        $media = $this->media->where('slug', $slug)->first();

        if(empty($media)) return '';

        return $media->url;
    }

    /**
     * Delete all resource's media.
     *
     * @return void
     */
    public function clearMedia()
    {
        foreach($this->media as $media) {
            $media->delete();
        }
    }

    /**
     * Asign a media to resource.
     *
     * @param File $file
     * @param array $data
     */
    public function addMedia($file, $data = [], $slug = null, $encode)
    {
        $media = $this->media()->create($data);

        $image = Helpers::image($file)->folder("/img/" . $this->getTable() . "/")->encode($encode)->name($media->filename)->save();

        $media->update(['filename' => $image, 'slug' => $slug]);
    }

    /**
     * Asign a media to resource.
     *
     * @param File $file
     * @param array $data
     */
    public function addMediaUrl($url, $data = [], $slug = null, $encode)
    {
        $media = $this->media()->create($data);

        $media->update(['filename' => $url, 'slug' => $slug]);
    }

    /**
     * Delete a specified media.
     *
     * @param string $slug
     */
    public function deleteMedia($slug = null)
    {
        $media = $this->media();

        if($slug) $media = $media->whereSlug($slug);

        $rows = $media->get();

        foreach($rows as $row) {
            $row->delete();
        }
    }

    /**
     * Update resource media. Delete old media
     * and save newly uploaded one.
     *
     * @param File $file
     * @param array $data
     */
    public function updateMedia($file, $slug = null, $encode = 'jpg', $data = [])
    {
        $this->deleteMedia($slug);

        $this->addMedia($file, $data, $slug, $encode = $encode);
    }

    /**
     * Update resource media. Delete old media
     * and save newly uploaded one.
     *
     * @param File $file
     * @param array $data
     */
    public function updateMediaUrl($url, $slug = null, $encode = 'jpg', $data = [])
    {
        $this->deleteMedia($slug);

        $this->addMediaUrl($url, $data, $slug, $encode = $encode);
    }

    /**
     * Delete all resource's media
     * and add newly uploaded media.
     *
     * @param File $file
     * @param array $data
     */
    public function clearAndUpdateMedia($file, $data)
    {
        $this->clearMedia();

        $this->addMedia($file, $data);
    }
}
