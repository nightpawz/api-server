<?php

namespace Modules\UI\Providers;

use Illuminate\Support\ServiceProvider;
use Blade;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Conpoment name prefix.
     *
     * @var string
     */
    protected $name_prefix = 'ui';

    /**
     * The folder location for components views.
     *
     * @var string
     */
    protected $view_component_folder = 'ui::_components';

    /**
     * The folder location for includes views.
     *
     * @var string
     */
    protected $view_include_folder = 'ui::_includes';

    /**
     * An array of views and the component name.
     * Defined as $view => $name array
     *
     * @var array
     */
    protected $components = [
        'sortable' => 'sortable',
        'sortable-item' => 'sortableitem',
        'sidebar' => 'sidebar',
        'topbar' => 'topbar',
        'segment' => 'segment',
        'form' => 'form'
    ];

    /**
     * An array of views and the includes name.
     * Defined as $view => $name array
     *
     * @var array
     */
    protected $includes = [
        'sidebar-item' => 'sidebaritem',
        'logout' => 'logout',
        'datatable' => 'datatable',
        'inputs.multiselect' => 'multiselect',
        'inputs.select' => 'select',
        'inputs.baseinput' => 'input',
        'inputs.textarea' => 'textarea',
        'inputs.editor' => 'editor',
        'inputs.image' => 'inputimage',
        'inputs.inputgroup' => 'inputgroup',
        'inputs.checkbox' => 'checkbox',
        'inputs.daterange' => 'daterange',
        'inputs.search' => 'search',
        'inputs.tags-input' => 'tagsinput',
        'cropit' => 'cropit',
        'btn-edit' => 'btnedit',
        'btn-delete' => 'btndelete'
    ];


    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerComponentAlias();

        $this->registerIncludeAlias();

        $this->registerGlobalComponents();
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Register view component alias.
     *
     * @return void
     */
    public function registerComponentAlias()
    {
        foreach($this->components as $view => $name) {
            Blade::component("{$this->view_component_folder}.{$view}", "{$this->name_prefix}{$name}");
        }
    }

    /**
     * Register view include alias.
     *
     * @return void
     */
    public function registerIncludeAlias()
    {
        foreach($this->includes as $view => $name) {
            Blade::include("{$this->view_include_folder}.{$view}", "{$this->name_prefix}{$name}");
        }
    }

    /**
 	 * Register global components
 	 *
 	 * @param type
 	 * @return void
	 */
     public function registerGlobalComponents()
     {
         Blade::directive('uidividingheader', function($text) {
            return "<?php echo \"<h4 class='ui dividing header'>\" . __($text) . \"</h4>\" ?>";
         });
     }

}
