<a href="{{ $action ?? '' }}" class="ui red icon button js__destroy"><i class="fa fa-trash"></i></a>

@pushonce('footer:btn-delete')
    <form action="" class="js__destroy_form" method="post">
        @csrf
        @method('DELETE')
    </form>

    <script type="text/javascript">
        $('.js__destroy').on('click', function(e) {
            e.preventDefault()

            var btn = $(this)
            $('.js__destroy_form').attr('action', btn.attr('href')).submit()
        })
    </script>
@endpushonce
