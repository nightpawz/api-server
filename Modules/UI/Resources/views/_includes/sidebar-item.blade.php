<a href="{{ $link ?? 'javascript:void(0)'}}" class="item">
    @isset($icon)
        <i class="{{ $icon }}"></i>
    @endisset

    {{ $label }}
</a>
