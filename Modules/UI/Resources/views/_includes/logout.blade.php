<form id="form-logout" action="{{ route('logout') }}" method="post" style="display: none;">
    @csrf
    @method('POST')
</form>

@push('footer')
    <script type="text/javascript">
        $('.js__logout').on('click', function(e) {
            e.preventDefault()

            $('#form-logout').submit()
        })
    </script>
@endpush
