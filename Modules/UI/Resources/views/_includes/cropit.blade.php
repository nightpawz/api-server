<div class="field">
    @isset($label)
        <label>{{ $label }}</label>
    @endisset
    <div class="js__cropit" data-height="{{ $h ?? 789 }}" data-width="{{ $w ?? 1583 }}" data-max-size="{{ $maxSize ?? '2MB' }}" data-input-name="{{ $name ?? 'image' }}" data-src="{{ $src ?? '' }}">
    </div>
</div>

@pushonce('head:ui-cropit')
    <style media="screen">
        .js__cropit .cropit-preview-image-container {
            border: 1px solid #eeeeee;
        }
        .js__cropit {
            text-align: center;
        }
        .js__cropit .cropit-controls {
            display: inline-flex;
            align-items: center;
            position: absolute;
            margin: 0;
            z-index: 11;
            background-color: rgba(0, 0, 0, .5);
            bottom: 0;
            left: 0;
            right: 0;
            padding: 5px;
        }

        .js__cropit .cropit-controls {
            color: white;
        }

        .js__cropit .cropit-controls input {
            width: 100%;
            margin: 0 5px;
        }

            .js__cropit .cropit-controls i:hover {
            cursor: pointer;
        }
    </style>
@endpushonce


@pushonce('footer:ui-cropit')

<script src="//cdnjs.cloudflare.com/ajax/libs/cropit/0.5.1/jquery.cropit.min.js" charset="utf-8"></script>
<script type="text/javascript">
    var cropitElements = document.getElementsByClassName("js__cropit")

    /**
     * Get cropper data attributes.
     *
     * @param obj cropper
     * @return array
     */
    function getCropperData(cropper) {
        var data = {
            height: cropper.attr("data-height"),
            width: cropper.attr("data-width"),
            size: cropper.attr("data-max-size"),
            src: cropper.attr("data-src"),
            name: cropper.attr("data-input-name")
        }

        return data
    }

    /**
     * Create HTML Element.
     *
     * @param string name
     * @param string className
     * @param bool type
     * @return obj
     */
    function createElement(name, className = null, type = null) {
        var createdElement = document.createElement(name)
        if (className != '') {
            createdElement.className = className
        }
        if (type != '') {
            createdElement.type = type
        }
        return createdElement
    }

    /**
     * Get cropit element real width.
     *
     * @param obj obj
     * @return int
     */
    function getRealWidth(obj) {
        if (obj.is(".modal-dialog")) {
            var clone = obj.clone()
            clone.css("visibility", "hidden");
            $('body').append(clone)
            var width = clone.find(".js__cropit").parent().outerWidth()
            clone.remove()
        } else {
            var width = obj.outerWidth()
        }

        return width
    }


    /**
     * Get object's parent element
     *
     * @param obj
     * @return obj
     */
    function getParent(obj) {
        var modal = obj.closest(".modal-dialog")
        if (modal.length > 0) {
            return modal
        } else {
            return obj.parent()
        }
    }

    /**
     * Calculate cropit dimension.
     *
     * @param int num
     * @param int exportZoom
     * @return int
     */
    function calculateDimension(num, exportZoom) {
        if (exportZoom < 1) {
            return (num * exportZoom)
        } else {
            return (num / exportZoom)
        }
    }

    for (const cropper of cropitElements) {
        var container = $(cropper)
        var data = getCropperData(container)
        var exportZoom = getRealWidth(getParent(container)) / data.width


        var inputName = createElement("input", "input-name", "hidden")
        inputName.setAttribute("name", data.name)

        var cropitPreview = createElement("div", "cropit-preview")
        cropitPreview.style.height = calculateDimension(data.height, exportZoom) + "px"
        cropitPreview.style.width = calculateDimension(data.width, exportZoom) + "px"
        cropitPreview.style.margin = "0 auto"

        var imageRules = createElement("div", "image-rules")
        var ruleHeight = createElement("small", "rule")
        ruleHeight.innerHTML = 'Min height: ' + data.height + 'px '
        var ruleWidth = createElement("small", "rule")
        ruleWidth.innerHTML = 'Min width: ' + data.width + 'px '
        var ruleSize = createElement("small", "rule")
        ruleSize.innerHTML = 'Max size: ' + data.size
        var ruleOverlay = createElement("p", "cropit-rule-overlay")
        ruleOverlay.innerHTML = `${data.width}x${data.height}`

        var inputImage = createElement("input", "cropit-image-input", "file")
        inputImage.style.maxWidth = calculateDimension(data.width, exportZoom) + "px"

        var inputRange = createElement("input", "cropit-image-zoom-input range-slider", "range")
        var rotateLeft = createElement("i", "fa fa-undo js-rotate-left")
        var rotateRight = createElement("i", "fa fa-repeat js-rotate-right")
        var cropitControls = createElement("div", "cropit-controls")
        cropitControls.append(rotateLeft)
        cropitControls.append(inputRange)
        cropitControls.append(rotateRight)

        imageRules.appendChild(ruleHeight)
        imageRules.appendChild(ruleWidth)
        imageRules.appendChild(ruleSize)

        cropitPreview.append(cropitControls)
        cropitPreview.append(ruleOverlay)
        container.append(cropitPreview)
        // container.append(imageRules)
        container.append(inputImage)
        container.append(inputName)
        container.cropit({
            minZoom: 'fit',
            smallImage: 'allow',
            initialZoom: 'min',
            freeMove: false,
            maxZoom: 3,
            allowDragNDrop: false,
            imageBackgroundBorderWidth: [15, 0, 15, 0]
        })
        if (data.src != '') {
            container.cropit('imageSrc', data.src)
        }

    }

    window.onresize = function(e) {
        for (var cropper of cropitElements) {
            setCropperSize($(cropper))
        }
    }

    function setCropperSize(container) {
        var data = getCropperData(container)
        var exportZoom = getRealWidth(getParent(container)) / data.width

        var cropitPreview = container.find(".cropit-preview")
        var cropitInput = container.find(".cropit-image-input")

        var w = calculateDimension(data.width, exportZoom)
        var h = calculateDimension(data.height, exportZoom)
        cropitPreview.css("height", h + "px")
        cropitPreview.css("width", w + "px")
        cropitInput.css("max-width", w + "px")

        container.cropit('previewSize', {
            width: w,
            height: h
        });
        container.cropit('exportZoom', exportZoom)
    }

    // Handle rotation
    $('.js-rotate-left').click(function() {
        $(this).closest('.js__cropit').cropit('rotateCCW')
    })
    $('.js-rotate-right').click(function() {
        $(this).closest('.js__cropit').cropit('rotateCW')
    })

    // Validate image dimension
    $(".cropit-image-input").bind("change", function() {
        var cropperHeight = $(this).closest('.js__cropit').attr("data-height"),
            cropperWidth = $(this).closest('.js__cropit').attr("data-width"),
            tmp = this.files && this.files[0]

        if (tmp) {
            var img = new Image()

            img.src = window.URL.createObjectURL(tmp)

            img.onload = function() {
                var width = img.naturalWidth,
                    height = img.naturalHeight

                if (width < cropperWidth || height < cropperHeight) {
                    alert("For better result please select image with minimum dimension of " + cropperWidth + "x" + cropperHeight + " px")
                }

            }
        }
    });

    function setCropit(el, w, h) {
        el.attr('data-height', h)
        el.attr('data-width', w)

        setCropperSize(el)
    }

    $('.js__cropit-switch').on('click', function() {
        var cropperRadio = $(this)
        setCropit($('#' + cropperRadio.attr('data-target')), cropperRadio.attr('data-width'), cropperRadio.attr('data-height'))
    })

    $(".js__export").on("click", function(e) {
        e.preventDefault()
        var form = $(this).closest("form")
        if (form.find(".js__cropit").length) {
            $.each(form.find(".js__cropit"), function(i) {
                var exCropper = $(this)
                var erealWidth = exCropper.attr("data-width")
                var ecropperWidth = exCropper.cropit("previewSize")
                var eexportZoom = erealWidth / ecropperWidth.width
                exCropper.cropit("exportZoom", eexportZoom)
                var image = exCropper.cropit("export", {
                    type: 'image/jpeg'
                })

                exCropper.find(".input-name").val(image)
            })
        }
        form.submit()
    })
</script>
@endpushonce
