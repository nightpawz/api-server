<div class="field {{ $class ?? '' }}">
    @isset($label)
        <label>{{ $label }}</label>
    @endisset
    <input type="{{ $type ?? 'text' }}" name="{{ $name ?? '' }}" value="{{ $value ?? '' }}" placeholder="{{ $placeholder ?? '' }}">
</div>
