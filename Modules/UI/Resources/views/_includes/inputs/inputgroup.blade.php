<div class="field {{ $class ?? '' }}">

    @isset($label)
        <label>{{ $label }}</label>
    @endisset
    <div class="ui right labeled input">

        @php
            $id = str_random(6);
        @endphp

        @isset($label_right)
            <label for="{{ $id }}" class="ui label">{{ $label_right }}</label>
        @endisset
        <input id="{{ $id }}" type="{{ $type ?? 'text' }}" name="{{ $name ?? '' }}" value="{{ $value ?? '' }}" placeholder="{{ $placeholder ?? '' }}">

        @isset($label_left)
            <div class="ui basic label">{{ $label_left }}</div>
        @endisset

    </div>

</div>
