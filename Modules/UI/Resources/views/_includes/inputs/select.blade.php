<div class="field">
    @isset($label)
        <label>{{ $label }}</label>
    @endisset

    <div class="ui fluid search selection dropdown ui__select">
        <input type="hidden" name="{{ $name ?? '' }}" value="{{ $value ?? '' }}">
        <i class="dropdown icon"></i>
        <div class="default text">{{ $placeholder ?? '' }}</div>
        <div class="menu">

            <div class="item" data-value="" data-text="-">
                <img class="ui mini avatar image" src="">
                -
            </div>
            @foreach ($options as $option)
                <div class="item" data-value="{{ $option['id'] }}" data-text="{{ $option['text'] }}">
                    @isset($option['image_url'])
                        <img class="ui mini avatar image" src="{{ $option['image_url'] }}">
                    @endisset
                    {{ $option['text'] }}
                </div>
            @endforeach
        </div>
    </div>
</div>


@pushonce('footer:uiselect')

    <script type="text/javascript">
        $(".ui__select").dropdown()
    </script>

@endpushonce
