<div class="field">
    @isset($label)
        <label>{{ $label }}</label>
    @endisset
    <textarea name="{{ $name ?? '' }}" rows="{{ $rows ?? 2 }}" placeholder="{{ $placeholder ?? '' }}">{{ $value ?? '' }}</textarea>
</div>
