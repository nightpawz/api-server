<div class="field">
    @isset($label)
        <label>{{ $label }}</label>
    @endisset
    <div class="js__input-image {{ $class ?? '' }} text-center">
        <img src="{{ $src ?? '' }}" class="img-responsive js__img-preview" style="max-height:100px; width:auto; background-color: #eee;">
        <div class="upload-box">
            <div class="hold">
                <a href="javascript:void(0)">Max Size 2MB</a>
                <span class="btn-file"> {{ __('Browse') }}
                    <input type="file" name="{{ $name ?? '' }}" class="js__img-button">
                </span>
            </div>
        </div>
    </div>
</div>

@pushonce('footer:image-input')
<script type="text/javascript">

    $(".js__img-button").change(function(){
        var parent = $(this).parents("div.js__input-image");
        readURLTrack(this);

        function readURLTrack(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

                reader.onload = function (e) {
                    parent.find('img.js__img-preview').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
    });

</script>
@endpushonce
