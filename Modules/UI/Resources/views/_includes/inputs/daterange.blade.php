<div class="field">
    <label>{{ $label ?? '' }}</label>

    <div class="two fields ui__daterange">
        <div class="field">
            <div class="ui calendar ui__start_calendar">
                <div class="ui input left icon">
                    <i class="calendar icon"></i>
                    <input type="text" placeholder="{{ __('Start') }}" name="{{ $name_start }}" value="{{ $value_start ?? '' }}">
                </div>
            </div>
        </div>
        <div class="field">
            <div class="ui calendar ui__end_calendar">
                <div class="ui input left icon">
                    <i class="calendar icon"></i>
                    <input type="text" placeholder="{{ __('End') }}" name="{{ $name_end }}" value="{{ $value_end ?? '' }}">
                </div>
            </div>
        </div>
    </div>
</div>

@pushonce('footer:date-range')

<script type="text/javascript">
    $(".ui__daterange").each(function(index) {
        var table = $(this)

        var start_date = table.find('.ui__start_calendar')
        var end_date = table.find('.ui__end_calendar')

        $(start_date).calendar({
            type: 'date',
            endCalendar: $(end_date)
        });
        $(end_date).calendar({
            type: 'date',
            startCalendar: $(start_date)
        });
    })
</script>

@endpushonce
