<div class="ui checkbox {{ $class ?? '' }}">
  <input type="checkbox" name="{{ $name ?? '' }}" class="{{ $input_class ?? '' }}">
  <label>{{ $label ?? '' }}</label>
</div>
