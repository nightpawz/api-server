<div class="field {{ $class ?? '' }}">
    @isset($label)
        <label>{{ $label }}</label>
    @endisset
    <input class="js__tags-input" type="{{ $type ?? 'text' }}" name="{{ $name ?? '' }}" value="{{ $value ?? '' }}" placeholder="{{ $placeholder ?? '' }}">
</div>

@pushonce('head:tags-input')
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery-tagsinput/1.3.6/jquery.tagsinput.min.css">
@endpushonce

@pushonce('footer:tags-input')
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-tagsinput/1.3.6/jquery.tagsinput.min.js" charset="utf-8"></script>

    <script type="text/javascript">
        $('.js_tags-input').tagsInput()
    </script>
@endpushonce
