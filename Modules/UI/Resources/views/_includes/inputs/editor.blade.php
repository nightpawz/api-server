<div class="field">
    @isset($label)
        <label>{{ $label }}</label>
    @endisset
    <textarea class="ui__editor" name="{{ $name ?? '' }}" rows="{{ $rows ?? 2 }}" placeholder="{{ $placeholder ?? '' }}">{{ $value ?? '' }}</textarea>
</div>

@pushonce('head:ui-editor')
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/medium-editor/5.23.3/css/medium-editor.min.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/medium-editor/5.23.3/css/themes/beagle.min.css">
@endpushonce

@pushonce('footer:ui-editor')
    <script src="//cdnjs.cloudflare.com/ajax/libs/medium-editor/5.23.3/js/medium-editor.min.js" charset="utf-8"></script>
    {{-- <script src="{{ asset('ui/plugins/medium-editor-list/medium-editor-list.min.js') }}" type="text/javascript"></script> --}}
    <script type="text/javascript">
        $(".ui__editor").each(function(index) {
            var element = $(this)

            var editor = new MediumEditor('.ui__editor', {
                // toolbar: {
                //     buttons: ['bold', 'italic', 'underline', 'list-extension']
                // },
                placeholder: {
                    text: element.attr('placeholder')
                },
                // extensions: {
                //     'list-extension': new MediumEditorList()
                // },
                // keyboardCommands: {
                //     /* This example includes the default options for keyboardCommands,
                //        if nothing is passed this is what it used */
                //     commands: [
                //         {
                //             command: 'bold',
                //             key: 'b',
                //             meta: true,
                //             shift: false
                //         },
                //         {
                //             command: 'italic',
                //             key: 'i',
                //             meta: true,
                //             shift: false
                //         },
                //         {
                //             command: 'underline',
                //             key: 'u',
                //             meta: true,
                //             shift: false
                //         }
                //     ],
                // }
            })

        })

    </script>
@endpushonce
