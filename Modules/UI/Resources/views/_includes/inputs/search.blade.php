<div class="field {{ $class ?? '' }}">
    @isset($label)
        <label>{{ $label }}</label>
    @endisset

    @php
        $id = str_random(4);
    @endphp
    <div class="ui search ui__search" data-url="{{ $url ?? '' }}" data-id="{{ $id }}">
        <div class="ui icon input">
            <input type="hidden" class="ui__search-result" name="{{ $name }}" value="{{ $value ?? '' }}">
            <input id="{{ $id }}" class="prompt" type="text" placeholder="{{ $placeholder ?? '' }}" value="{{ $value_text ?? '' }}">
            <i class="search icon"></i>
        </div>
        <div class="results"></div>
    </div>
</div>

@pushonce('footer:uisearch')

<script type="text/javascript">
    $(".ui__search").each(function(index) {
        var el = $(this)

        el.search({

            onSelect(result, response) {
                el.find('.ui__search-result').val(result.id)
            },

            apiSettings: {
                minCharacters: 1,
                searchOnFocus: false,
                searchDelay: 1000,
                url: el.attr('data-url'),
                data: {
                    "q": ''
                },
                beforeSend: function(settings) {
                    settings.data = {
                        q: document.getElementById(el.attr('data-id')).value
                    }
                    return settings
                },
                contentType: "application/json; charset=utf-8",
                dataType: "json",


                onResponse: function(response){
                    var convertedResponse = {
                        results : []
                    };

                    // translate response to work with search
                    $.each(response.data, function(index, item) {
                        // add result to category
                        convertedResponse.results.push({
                            title       : item.name,
                            id          : item.id
                            // image : item.onekey_id,
                        });
                    });
                    return convertedResponse;
                },
            }
        })
    })
</script>

@endpushonce
