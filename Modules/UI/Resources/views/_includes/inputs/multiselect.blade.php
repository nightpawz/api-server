<div class="field">
    <div class="ui fluid multiple search selection dropdown ui__multiselect">
        <input type="hidden" name="{{ $name ?? '' }}" value="{{ $value ?? '' }}">
        <i class="dropdown icon"></i>
        <div class="default text">{{ $placeholder ?? '' }}</div>
        <div class="menu">

            @foreach ($options as $option)
                <div class="item" data-value="{{ $option->id }}" data-text="{{ $option->name }}">
                    <img class="ui mini avatar image" src="{{ $option->image_url }}">
                    {{ $option->name }}
                </div>
            @endforeach
        </div>
    </div>
</div>

@pushonce('footer:uimultiselect')

    <script type="text/javascript">
        $('.ui__multiselect').dropdown()
    </script>

@endpushonce
