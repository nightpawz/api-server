<table class="ui celled table nowrap table table-hover ui__datatable" data-url="{{ $url }}" style="width: 100%s">
    <thead>
	    <tr>
            @foreach ($columns as $label => $column)
                <th data-column="{{ $column }}" data-search="false">{{ $label }}</th>
            @endforeach
	    </tr>
    </thead>
    <tbody></tbody>
</table>

@pushonce('head:datatable')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/dataTables.semanticui.min.css">

    <style media="screen">
        .ui__datatable i.w-20 {
            width: 20px;
            text-align: center;
        }

        .ui__datatable .mb-0 {
            margin-bottom: 0;
        }

        .ui__datatable p {
            margin: 0;
        }

        .ui__datatable img {
            max-height: 80px;
            margin: 0 auto;
        }

        table.dataTable thead .sorting_asc,
        table.dataTable thead .sorting_desc,
        table.dataTable thead .sorting
        {
            background-image: none;
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button:hover {
            color: #000!important;
            /* border: none!important; */
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button:before {
            width: 0!important;
        }

        .dataTables_wrapper .dataTables_paginate .paginate_button {
            margin-left: 0!important;
        }

        .dataTables_wrapper .dataTables_processing {
            height: auto;
            max-width: 300px;
            margin: 0 auto;
        }
    </style>
@endpushonce

@pushonce('footer:datatable')
    <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" charset="utf-8"></script>
    <script src="//cdn.datatables.net/1.10.19/js/dataTables.semanticui.min.js" charset="utf-8"></script>
    <script src="//cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js" charset="utf-8"></script>
    <script src="//cdn.datatables.net/responsive/2.2.3/js/responsive.semanticui.min.js" charset="utf-8"></script>

    <form action="" id="dt-delete-form" style="display: none;" method="post">
        @csrf
        @method('DELETE')
    </form>

    <script type="text/javascript">
        $(".ui__datatable").each(function(index) {
            var table = $(this)
            $.each(table, function(i) {
                initiateTable(table)
            })
        })

        $(document).on('click', '.dt__delete', function(e) {
            e.preventDefault()
            // alert('awdwd')
            var el = $(this)

            $('#dt-delete-form').attr('action', el.attr('href')).submit()
        })

        function initiateTable(table, formData={}) {
            var columns = [];
            $.each(table.find("th"), function(i) {
                var th = $(this),
                    name = th.attr("data-column"),
                    order = true,
                    search = true
                if(th.is("data-order")) {
                    order = false
                }
                if (th.is("data-search")) {
                    search = false
                }
                columns.push({ data: name, name: name, orderable: order, searchable: search })
            });

            table.DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: table.attr("data-url"),
                    data: formData
                },
                columns: columns
            })
        }

    </script>
@endpushonce
