<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('ui/semantic.min.css') }}" rel="stylesheet">
    <link href="{{ asset('ui/custom.css') }}" rel="stylesheet">

    @stack('head')
</head>
<body>

    @uitopbar

    @enduitopbar

    @uisidebar
        @stack('sidebar')
    @enduisidebar

    <div class="pusher">
        <div class="ui equal width left aligned grid stackable container ui-content">

            <div class="row">
                <div class="sixteen wide column centered">
                    @include('flash::message')

                    @if ($errors->any())
                        <div class="ui negative message">
                            <div class="header">
                                Error
                            </div>
                            <ul class="list">
                                {!! implode('', $errors->all('<li>:message</li>')) !!}
                            </ul>
                        </div>
                    @endif
                </div>
            </div>

            @yield('content')
        </div>
    </div>

    <footer>
        <!-- Scripts -->
        <script src="{{ asset('js/jquery.min.js') }}"></script>
        <script src="{{ asset('ui/semantic.min.js') }}"></script>

        <script type="text/javascript">
            $('.menu .item').tab()
        </script>

        @stack('footer')
    </footer>

</body>
</html>
