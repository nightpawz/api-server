<form
    action="{{ $action ??'' }}"
    class="ui form {{ $class ?? '' }}"
    method="{{ isset($method) && $method === 'GET' ? 'GET' : 'POST' }}"

    @if (isset($file) && $file === true)
        enctype="multipart/form-data"
    @endif

    >


    @csrf
    @method($method ?? 'GET')

    {{ $slot }}

    <button type="submit" class="ui button js__export" tabindex="0" style="margin-top: 20px; margin-bottom: 20px;">{{ __('Submit') }}</button>

</form>
