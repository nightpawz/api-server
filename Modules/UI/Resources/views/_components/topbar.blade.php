<div class="ui fixed inverted menu">
    <div class="ui container">
        <a href="javascript:void(0)" class="item ui__sidebar" data-transition="slide along"><i class="fa fa-bars"></i></a>
        <a href="/" class="header item">
            <img class="logo" src="@yield('logo')" alt="Logo">
        </a>
        <a href="#" class="item">Home</a>
        <div class="ui simple dropdown item ml-auto">
            @auth
                {{ Auth::user()->name ?? 'Dropdown' }}
            @endauth
            <i class="dropdown icon"></i>
            <div class="menu">
                <a class="item" href="#">Link Item</a>
                <a class="item" href="#">Link Item</a>
                <div class="divider"></div>
                <div class="header">Header Item</div>
                <div class="item">
                    <i class="dropdown icon"></i>
                    Sub Menu
                    <div class="menu">
                        <a class="item" href="#">Link Item</a>
                        <a class="item" href="#">Link Item</a>
                    </div>
                </div>
                @uilogout
                <a class="item js__logout" href="javascript:void(0)">{{ __('Logout') }}</a>
            </div>
        </div>
    </div>
</div>
