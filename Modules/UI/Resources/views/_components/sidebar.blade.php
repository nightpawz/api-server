<div class="ui left thin pushable sidebar vertical inverted menu">
    @stack('sidebar')
</div>

@push('head')
    <style media="screen">
        .ui.sidebar.visible ~ .pusher {
            width: calc(100% - 260px);
        }
    </style>
@endpush

@push('footer')
    <script type="text/javascript">
        // $('.ui.sidebar').sidebar('toggle')
        $('.sidebar')
            .sidebar({
              dimPage: false,
              transition: 'slide along'
            });

        $('.ui__sidebar').on("click", function() {
            $('.ui.sidebar').sidebar('toggle')
        })

    </script>
@endpush
