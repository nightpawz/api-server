<div class="ui segments {{ $class ?? '' }}">
    <div class="ui segment ui-header">
        {{ $header ?? '' }}
    </div>
    <div class="ui segment">

        @if (isset($space) && $space === true)
            <div class="ui grid">
                <div class="row centered">
                    <div class="sixteen wide mobile twelve wide tablet ten wide computer column">
        @endif

        {{ $slot }}

        @if (isset($space) && $space === true)
                    </div>
                </div>
            </div>
        @endif

    </div>
</div>

@pushonce('head:segment')
    <style media="screen">
        .ui.segment.ui-header {
            display: flex;
            align-items: baseline;
        }

        .ui.segment.ui-header a.button-right {
            margin-left: auto;
            margin-right: 0;
        }

    </style>
@endpushonce
