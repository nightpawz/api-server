<div class="ui relaxed divided list js__sortable">

    {{ $slot }}
    
</div>
@pushonce('head:sortable')


@endpushonce


@pushonce('footer:sortable')

    <script src="https://cdn.jsdelivr.net/npm/sortablejs@latest/Sortable.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-sortablejs@latest/jquery-sortable.js"></script>

    <script type="text/javascript">
        $('.js__sortable').sortable({
            handle: '.js__sortable-handle',
            animation: 150
        });
    </script>

@endpushonce
