<div class="item sortable-item">
    <i class="large arrows alternate middle aligned icon js__sortable-handle"></i>
    <div class="content">
        <div class="content-wrapper">
            {{ $slot }}
        </div>
    </div>
</div>

@pushonce('head:sortable-item')

<style media="screen">
    .sortable-item img {
        max-width: 200px;
        max-height: 100px;
    }

    .sortable-item .arrows.alternate {
        cursor: move;
        cursor: -webkit-grabbing;
    }

    .sortable-item .content-wrapper {
        display: inline-flex;
        width: 100%;
        align-items: center;
    }

    .sortable-item .content-wrapper .action {
        display: inline-block;
        margin-left: auto;
        margin-right: 0;
        min-width: 100px;
    }

    .sortable-item .content-wrapper .font-icon {
        font-size: 44px;
        width: 60px;
        text-align: center;
        padding: 10px;
        border: 1px solid #eee;
        margin-right: 20px;
    }

    .sortable-item .content-wrapper .text .title {
        font-weight: 600;
        margin-bottom: 5px;
    }
</style>

@endpushonce
