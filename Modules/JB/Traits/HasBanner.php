<?php

namespace Modules\JB\Traits;
use Modules\JB\Entities\Banner;

trait HasBanner
{
    public function banners()
    {
        return $this->hasMany(Banner::class, 'category_id');
    }

    public function createBanner($group, $display_date, $expired_date)
    {
        return $this->banners()->create([
            'group' => $group,
            'display_date' => $display_date,
            'expired_date' => $expired_date
        ]);
    }

    public function getBanners($group)
    {
        return $this->banners()->where('group', $group)->get();
    }
}


?>
