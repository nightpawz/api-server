<?php

namespace Modules\JB\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class CategoryResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'parent_id' => $this->parent_id,
            'slug' => $this->slug,
            'image_url' => $this->image_url,
            'icon_url' => $this->icon_url,
            'parent' => $this->parent,
            'childs' => $this->childs
        ];
    }
}
