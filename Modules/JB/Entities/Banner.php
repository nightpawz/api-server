<?php

namespace Modules\JB\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Mediable\Mediable;
use Carbon\Carbon;

class Banner extends Model
{
    use Mediable;

    /**
 	 * The table of the model.
 	 *
 	 * @var string
	 */
    protected $table="jb_banners";

    /**
 	 * The attributes that are mass assignable.
 	 *
 	 * @var array
	 */
    protected $fillable = [
        'category_id',
        'group',
        'display_date',
        'expired_date'
    ];

    // public function setDisplayDateAttribute($value)
    // {
    //     $this->attributes['display_date'] = Carbon::createFromFormat('F j, Y', $value);
    // }
    //
    // public function setExpiredDateAttribute($value)
    // {
    //     $this->attributes['expired_date'] = Carbon::createFromFormat('F j, Y', $value);
    // }

    /**
 	 * The attributes that are appended.
 	 *
 	 * @var array
	 */
     protected $appends = [
         'image_url'
     ];

     /**
      * The relations to eager load on every query.
      *
      * @var array
      */
     protected $with = [
         'media',
         'category'
     ];

     /**
 	 * The accessor for image_url attribute.
 	 *
 	 * @return string
	 */
     public function getImageUrlAttribute()
     {
         return asset($this->getMediaName());
     }

     public function category()
     {
         return $this->belongsTo(Category::class, 'category_id');
     }
}
