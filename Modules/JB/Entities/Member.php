<?php

namespace Modules\JB\Entities;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $table = 'jb_members';

    protected $fillable = [
        'user_id',
        'name',
        'company',
        'phone',
        'email',
        'gender',
        'birthday'
    ];

    public function user()
    {
        return $this->belongsTo(App\User::class, 'user_id');
    }

    public function listings()
    {
        return $this->hasMany(Listing::class, 'owner_id');
    }

    public function scopeSearch($query, $q)
    {
       return $query->where('name', 'like', "%{$q}%")
                    ->orWhere('email', 'like', "%{$q}%");
    }
}
