<?php

namespace Modules\JB\Entities;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;

class ListingFeature extends Model
{
    use Translatable;

    protected $table="jb_listing_features";

    protected $fillable = [
        'listing_id',
    ];

    /**
     * The attributes that are translatable.
     *
     * @var array
     */
    public $translatedAttributes = [
        'nama',
    ];

    public $timestamps = true;
}
