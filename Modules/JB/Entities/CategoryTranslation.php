<?php

namespace Modules\JB\Entities;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class CategoryTranslation extends Model
{
    use Sluggable;

    /**
 	 * The table of the model.
 	 *
 	 * @var string
	 */
    protected $table="jb_category_translations";

    /**
 	 * The attributes that are mass assignable.
 	 *
 	 * @var array
	 */
    protected $fillable = [
        'locale',
        'category_id',
        'slug',
        'name'
    ];

    /**
     * Disable laravel from automatically create timestamps.
     *
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Return the sluggable configuration for this Model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
}
