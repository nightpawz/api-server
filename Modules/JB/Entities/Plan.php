<?php

namespace Modules\JB\Entities;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    protected $table = 'jb_plans';

    protected $fillable = [
        'name',
        'description',
        'price',
        'duration'
    ];
}
