<?php

namespace Modules\JB\Entities;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;
use Modules\Mediable\Mediable;
use Modules\JB\Traits\HasBanner;

class Category extends Model
{
    use Translatable, Mediable, HasBanner;

    /**
 	 * The table of the model.
 	 *
 	 * @var string
	 */
    protected $table="jb_categories";

    /**
 	 * The attributes that are mass assignable.
 	 *
 	 * @var array
	 */
    protected $fillable = [
        'parent_id',
        'source_url'
    ];

    /**
     * The attributes that are translatable.
     *
     * @var array
     */
    public $translatedAttributes = [
    	'name',
        'slug'
    ];

    /**
 	 * The attributes that are appended.
 	 *
 	 * @var array
	 */
     protected $appends = [
         'image_url',
         'icon_url'
     ];

     /**
      * The relations to eager load on every query.
      *
      * @var array
      */
     protected $with = [
         'media',
         'translations'
     ];

     /**
 	 * The accessor for image_url attribute.
 	 *
 	 * @return string
	 */
     public function getImageUrlAttribute()
     {
         return asset($this->getMediaName('image'));
     }

     /**
 	 * The accessor for image_url attribute.
 	 *
 	 * @return string
	 */
     public function getIconUrlAttribute()
     {
         return asset($this->getMediaName('icon'));
     }

     /**
 	 * Define relationship with Listing model.
 	 *
 	 * @return Illuminate\Database\Eloquent\Relations\HasMany
	 */
     public function listings()
     {
         return $this->hasMany(Listing::class, 'category_id');
     }

     /**
      * Define childs relations
      *
      * @return Illuminate\Database\Eloquent\Relations\HasMany
      */
     public function childs()
     {
         return $this->hasMany(Category::class, 'parent_id');
     }

     /**
      * Define parent relationship.
      *
      * @return Illuminate\Database\Eloquent\Relations\BelongsTo
      */
     public function parent()
     {
         return $this->belongsTo(Category::class, 'parent_id');
     }

     /**
     * Scope a query to only include parent categories.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
     public function scopeOnlyParents($query)
     {
         return $query->where('parent_id', NULL);
     }

      /**
      * Scope a query to only include parent categories.
      *
      * @param \Illuminate\Database\Eloquent\Builder $query
      * @return \Illuminate\Database\Eloquent\Builder
      */
      public function scopeOnlyChilds($query)
      {
          return $query->whereNotNull('parent_id');
      }

      public function scopeSearch($query, $q)
      {
          return $query->whereHas('translations', function($query) use ($q) {
             return $query->where('name', 'like', "%{$q}%");
          });
      }
}
