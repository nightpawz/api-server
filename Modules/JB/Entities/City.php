<?php

namespace Modules\JB\Entities;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Modules\Mediable\Mediable;

class City extends Model
{
    use Sluggable, Mediable;

    protected $table = 'jb_cities';

    protected $fillable = [
        'name',
        'slug'
    ];

    /**
 	 * The attributes that are appended.
 	 *
 	 * @var array
	 */
    protected $appends = [
        'banner_url',
        'thumbnail_url'
    ];

    /**
     * The accessor for banner_url attribute.
     *
     * @return string
     */
    public function getBannerUrlAttribute()
    {
        return asset($this->getMediaName('banner'));
    }

    /**
     * The accessor for thumbnail_url attribute.
     *
     * @return string
     */
    public function getThumbnailUrlAttribute()
    {
        return asset($this->getMediaName('thumbnail'));
    }

    /**
     * Return the sluggable configuration for this Model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function listings()
    {
        return $this->hasMany(Listing::class, 'city_id');
    }
}
