<?php

namespace Modules\JB\Entities;

use Illuminate\Database\Eloquent\Model;

class Sales extends Model
{
    protected $table = 'jb_sales';

    protected $fillable = [
        'user_id',
        'name',
        'phone',
        'email',
        'gender',
        'birthday',
        'address'
    ];

    public function user()
    {
        return $this->belongsTo(App\User::class, 'user_id');
    }

    public function listings()
    {
        return $this->hasMany(Listing::class, 'sales_id');
    }

    public function scopeSearch($query, $q)
    {
       return $query->where('name', 'like', "%{$q}%")
                    ->orWhere('email', 'like', "%{$q}%");
    }
}
