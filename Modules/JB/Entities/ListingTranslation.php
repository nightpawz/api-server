<?php

namespace Modules\JB\Entities;

use Illuminate\Database\Eloquent\Model;

class ListingTranslation extends Model
{
    /**
     * The table of the model.
     *
     * @var string
     */
    protected $table="jb_listing_translations";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'locale',
        'listing_id',
        'description'
    ];

    // Primary Key
    public $primaryKey = 'id';
    public $foreignKey = 'listing_id';

    /**
     * Disable laravel from automatically create timestamps.
     *
     * @var boolean
     */
    public $timestamps = false;

}
