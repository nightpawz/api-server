<?php

namespace Modules\JB\Entities;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Enterprise extends Model
{
    /**
 	 * The table of the model.
 	 *
 	 * @var string
	 */
    protected $table="jb_enterprises";

    /**
 	 * The attributes that are mass assignable.
 	 *
 	 * @var array
	 */
    protected $fillable = [
        'listing_id',
        'group',
        'display_date',
        'expired_date'
    ];

    // public function setDisplayDateAttribute($value)
    // {
    //     $this->attributes['display_date'] = Carbon::createFromFormat('F j, Y', $value);
    // }
    //
    // public function setExpiredDateAttribute($value)
    // {
    //     $this->attributes['expired_date'] = Carbon::createFromFormat('F j, Y', $value);
    // }

    public function listing()
    {
        return $this->belongsTo(Listing::class, 'listing_id');
    }
}
