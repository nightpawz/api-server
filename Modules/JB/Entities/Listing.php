<?php

namespace Modules\JB\Entities;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;
use Cviebrock\EloquentSluggable\Sluggable;
use Modules\Mediable\Mediable;
use Modules\Socialable\Socialable;
use App\User;

class Listing extends Model
{
    use Translatable, Sluggable, Mediable, Socialable;

    /**
     * The table of the model.
     *
     * @var string
     */
    protected $table="jb_listings";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ref_id',
        'owner_id',
        'category_id',
        'features',
        'name',
        'slug',
        'address',
        'phone',
        'email',
        'site_url',
        'register_date',
        'expired_date',
        'latlong',
        'hit_count',
        'no_contract'
    ];

    /**
 	 * The attributes that are appended.
 	 *
 	 * @var array
	 */
    protected $appends = [
        'logo_url',
        'banner_url',
        'thumbnail_url',
        'excerpt'
    ];

    /**
     * The attributes that are translatable.
     *
     * @var array
     */
    public $translatedAttributes = [
        'description',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        // 'features' => 'array',
    ];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = [
        'owner',
        'translations',
        'category',
        'media',
    ];

    /**
     * The accessor for logo_url attribute.
     *
     * @return string
     */
    public function getLogoUrlAttribute()
    {
        return $this->getMediaName('logo');
    }

    /**
     * The accessor for banner_url attribute.
     *
     * @return string
     */
    public function getBannerUrlAttribute()
    {
        return $this->getMediaName('banner');
    }

    /**
     * The accessor for thumbnail_url attribute.
     *
     * @return string
     */
    public function getThumbnailUrlAttribute()
    {
        return $this->getMediaName('thumbnail');
    }

    public function getExcerptAttribute()
    {
        return str_limit(strip_tags($this->description), 50);
    }

    /**
     * Return the sluggable configuration for this Model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    /**
     * Define parent relationship.
     *
     * @return Illumimate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function city()
    {
        return $this->belongsTo(City::class, 'city_id');
    }

    /**
 	 * Block comment
 	 *
 	 * @param type
 	 * @return void
	 */
	  public function owner()
    {
        return $this->belongsTo(Member::class, 'owner_id');
    }

    public function features()
    {
        return $this->hasMany(ListingFeature::class, 'listing_id');
    }

    public function scopeSearch($query, $q)
    {
        return $query->where('name', 'like', "%{$q}%");
    }

    public function scopeWhereCity($query, $q)
    {
        return $query->whereHas('city', function($query) use ($q) {
          return $query->where('slug', $q);
        });
    }

    public function scopeWhereCategory($query, $q)
    {
        return $query->whereHas('category', function($query) use ($q) {
          return $query->whereTranslation('slug', $q);
        });
    }
}
