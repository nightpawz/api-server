<?php

namespace Modules\JB\Entities;

use Illuminate\Database\Eloquent\Model;

class Business extends Model
{
    protected $fillable = [
        'category_id',
        'name',
        'ref_id'
    ];
}
