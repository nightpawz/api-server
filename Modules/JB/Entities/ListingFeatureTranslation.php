<?php

namespace Modules\JB\Entities;

use Illuminate\Database\Eloquent\Model;

class ListingFeatureTranslation extends Model
{

    protected $table="jb_listing_feature_translations";
    protected $fillable = [
        'locale', 'listing_id', 'name',
    ];

    public $primaryKey = 'id';
    public $foreignKey = 'listing_id';
    public $timestamps = false;

}
