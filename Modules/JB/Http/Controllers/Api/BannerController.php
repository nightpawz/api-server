<?php

namespace Modules\JB\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\JB\Transformers\BannerResource;
use Modules\JB\Entities\Banner;
use Modules\JB\Http\Requests\BannerRequest;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $query = new Banner;

        if($request->has('q')) {
            $query = $query->search($request->input('q'));
        }

        $collections = $query->latest()->paginate($request->input('limit', 10));

        return BannerResource::collection($collections);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(BannerRequest $request)
    {
        $banner = Banner::create($request->all());

        if($request->has('new_image_url')) {
            $banner->updateMediaUrl($request->new_image_url);
        }

        $banner->save();

        return response()->json([
            'message' => __('Banner Added'),
            'data' => $banner
        ]);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show(Banner $banner)
    {
        return response()->json(new BannerResource($banner));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(BannerRequest $request, Banner $banner)
    {
        $banner->update($request->all());

        if($request->has('new_image_url')) {
            $banner->updateMediaUrl($request->new_image_url);
        }

        $banner->save();

        return response()->json([
            'message' => __('Banner Updated'),
            'data' => $banner
        ]);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Banner $banner)
    {
        $banner->delete();

        return response()->json([
            'message' => __('Banner Deleted')
        ]);
    }
}
