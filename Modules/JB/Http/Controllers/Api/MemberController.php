<?php

namespace Modules\JB\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\JB\Entities\Member;
use Modules\JB\Transformers\MemberResource;
use Modules\JB\Http\Requests\MemberRequest;

class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $query = new Member;

        $collections = $query->latest()->paginate($request->input('limit', 10));

        return MemberResource::collection($collections);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(MemberRequest $request)
    {
        $member = Member::create($request->all());

        return response()->json([
            'message' => __('New Member Created'),
            'data' => $member
        ]);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show(Member $member)
    {
        return response()->json(['data' => $member]);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('jb::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Member $member)
    {
        $member->delete();

        return response()->json([
            'message' => __('Member Deleted')
        ]);
    }
}
