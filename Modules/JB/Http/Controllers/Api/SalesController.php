<?php

namespace Modules\JB\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\JB\Entities\Sales;
use Modules\JB\Transformers\SalesResource;
use Modules\JB\Http\Requests\SalesRequest;

class SalesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
     public function index(Request $request)
     {
         $query = new Sales;

         $collections = $query->latest()->paginate($request->input('limit', 10));

         return SalesResource::collection($collections);
     }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(SalesRequest $request)
    {
        $sale = Sales::create($request->all());

        return response()->json([
            'message' => __('Sales Added'),
            'data' => $sale
        ]);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show(Sales $sale)
    {
        return response()->json([
            'data' => new SalesResource($sale)
        ]);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(SalesRequest $request, Sales $sale)
    {
        $sale->update($request->all());

        return response()->json([
            'message' => __('Sales Updated'),
            'data' => new SalesResource($sale)
        ]);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Sales $sale)
    {
        $sale->delete();

        return response()->json([
            'message' => __('Sales Deleted')
        ]);
    }
}
