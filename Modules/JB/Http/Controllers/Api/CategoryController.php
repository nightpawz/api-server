<?php

namespace Modules\JB\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\JB\Entities\Category;
use Modules\JB\Transformers\CategoryResource;
use Modules\JB\Http\Requests\CategoryRequest;

class CategoryController extends Controller
{
    protected $fillable_columns = ['parent_id', 'name', 'slug'];

    /**
     * Display a listing of the resource.
     * @return Response
     */
     public function index(Request $request)
     {
         $query = new Category;

         if($request->has('q')) {
             $query = $query->search($request->input('q'));
         }

         $collections = $query->latest()->paginate($request->input('limit', 10));

         return CategoryResource::collection($collections);
     }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(CategoryRequest $request)
    {
        $category = Category::create($request->only($this->fillable_columns));

        $this->updateImage($category, $request);

        return response()->json([
            'message' => __('Category Added'),
            'data' => new CategoryResource($category)
        ]);
    }

    public function updateImage(Category $category, $request)
    {
        if($request->has('new_image_url') && $request->new_image_url) {
            $category->updateMediaUrl($request->new_image_url, 'image');
        }

        if($request->has('new_icon_url')  && $request->new_icon_url) {
            $category->updateMediaUrl($request->new_icon_url, 'icon');
        }

        $category->save();
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show(Category $category)
    {
        return response()->json([
            'data' => new CategoryResource($category)
        ]);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(CategoryRequest $request, Category $category)
    {

        $category->update($request->only($this->fillable_columns));

        $this->updateImage($category, $request);

        return response()->json([
            'message' => __('Category Updated'),
            'data' => new CategoryResource($category)
        ]);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Category $category)
    {
        $category->delete();

        return response()->json([
            'message' => __('Category Deleted')
        ]);
    }
}
