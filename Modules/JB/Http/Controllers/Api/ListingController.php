<?php

namespace Modules\JB\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\JB\Entities\Listing;
use Modules\JB\Transformers\ListingResource;

class ListingController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $listings = new Listing;

        if($request->has('q')) {
            $listings = $listings->search($request->input('q'));
        }

        $listings = $listings->latest()->paginate($request->input('limit', 10));

        return ListingResource::collection($listings);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('jb::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('jb::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('jb::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Listing $listing)
    {
        $listing->delete();

        return response()->json([
            'message' => __('Listing Deleted')
        ]);
    }
}
