<?php

namespace Modules\JB\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\JB\Transformers\Api\PlanResource;
use Modules\JB\Entities\Plan;
use Modules\JB\Http\Requests\PlanRequest;

class PlanController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $query = new Plan;

        $collections = $query->latest()->paginate($request->input('limit', 10));

        return PlanResource::collection($collections);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(PlanRequest $request)
    {
        $plan = Plan::create($request->all());

        return response()->json([
            'message' => __('New Plan Added'),
            'data' => $plan
        ]);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show(Plan $plan)
    {
        return response()->json([
            'data' => new PlanResource($plan)
        ]);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(PlanRequest $request, Plan $plan)
    {
        $plan->update($request->all());

        return response()->json([
            'message' => __('Plan Information Updated'),
            'data' => $plan
        ]);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Plan $plan)
    {
        $plan->delete();

        return response()->json([
            'message' => __('Plan Deleted')
        ]);
    }
}
