<?php

namespace Modules\JB\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class ApiLocalization
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if($request->has('_lang')) {
            app()->setLocale($request->input('_lang'));
        }
        
        return $next($request);
    }
}
