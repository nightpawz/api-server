<?php

Route::apiResources([
    'members' => 'MemberController',
    'sales' => 'SalesController',
    'plans' => 'PlanController',
    'categories' => 'CategoryController',
    'banners' => 'BannerController',
    'listings' => 'ListingController'
]);
