<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnterprisesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jb_enterprises', function (Blueprint $table) {
            $table->increments('id');
            $table->string('group');
            $table->integer('listing_id')->unsigned()->index();
            $table->foreign('listing_id')->references('id')->on('jb_listings')->onDelete('cascade');
            $table->date('display_date');
            $table->date('expired_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enterprises');
    }
}
