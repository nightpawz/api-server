<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoryTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jb_category_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('locale');
            $table->integer('category_id')->unsigned()->index();
            $table->foreign('category_id')->references('id')->on('jb_categories')->onDelete('cascade');
            
            $table->string('name');
            $table->string('slug');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jb_category_translations');
    }
}
