<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jb_listings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ref_id')->nullable();
            $table->integer('no_contract')->nullable();
            $table->integer('owner_id')->nullable();
            $table->integer('category_id')->unsigned()->index();
            $table->foreign('category_id')->references('id')->on('jb_categories')->onDelete('cascade');
            $table->string('name')->nullable();
            $table->string('slug')->nullable();
            $table->text('features')->nullable();
            $table->text('address')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->string('site_url')->nullable();
            $table->date('register_date')->nullable();
            $table->date('expired_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jb_listings');
    }
}
