<?php

namespace Modules\JB\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\JB\Entities\{Category, Listing, Enterprise};
use Faker\Factory;

class ListingSeederTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach(Category::all() as $category) {
            $this->createListing($category);
        }

        foreach(Listing::latest()->take(10)->pluck('id') as $i) {
            $this->createEnterpriseList($i);
        }
    }

        /**
     * Create listing data
     *
     * @return void
     */
    public function createListing($category)
    {
        $faker = Factory::create();

        foreach (range(1, rand(3, 32)) as $i) {
            $listing = $category->listings()->create([
                'city_id' => rand(1, 5),
                'name' => $faker->catchPhrase,
                'address' => $faker->address,
                'phone' => $faker->phoneNumber,
                'email' => $faker->email,
                'register_date' => $faker->date,
                'expired_date' => $faker->date,
                'description' => $faker->paragraphs(5, true),
            ]);

            $this->createMedia($listing);
        }

    }


    /**
     * Create listing data
     *
     * @return void
     */
    public function createListingFeature()
    {
        $faker = Factory::create();

        $listingfeature = ListingFeature::create([
            'listing_id' => rand(1,50),
        ]);
    }

    public function createMedia($model)
    {
        $model->media()->create([
            'slug' => 'banner',
            'filename' => '/img/mockup/listing/banner.jpg'
        ]);

        $model->media()->create([
            'slug' => 'logo',
            'filename' => '/img/mockup/listing/logo.png'
        ]);

        $model->media()->create([
            'slug' => 'thumbnail',
            'filename' => '/img/mockup/listing/thumbnail.jpg'
        ]);
    }

    public function createEnterpriseList($i)
    {
        Enterprise::create([
            'listing_id' => $i,
            'group' => 'home-top-listing',
            'display_date' => now(),
            'expired_date' => now()->addDays(rand(3,8))
        ]);
    }

}
