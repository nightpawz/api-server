<?php

namespace Modules\JB\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\JB\Entities\{Member};
use Faker\Factory;

class MemberSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        foreach (range(1, 30) as $i) {
            Member::create([
                'name' => $faker->name,
                'company' => $faker->company,
                'phone' => $faker->phoneNumber,
                'email' => $faker->freeEmail,
                'gender' => $faker->randomElement(['M', 'F']),
                'birthday' => $faker->date
            ]);
        }
    }
}
