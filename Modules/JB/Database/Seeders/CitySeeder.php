<?php

namespace Modules\JB\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\JB\Entities\{City};
use LaravelLocalization;
use Faker\Factory;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach([
            'Kota Yogyakarta',
            'Sleman',
            'Bantul',
            'Kulon Progo',
            'Gunung Kidul',
            'Kota Lainnya'
            ] as $i) {

            $this->createCity($i);
        }
    }

    /**
     * Create category data
     *
     * @return void
     */
    public function createCity($name)
    {
        $city = City::create(['name' => $name]);

        $this->createMedia($city);
    }

    public function createMedia($city)
    {
        $city->media()->create([
            'slug' => 'banner',
            'filename' => "/img/mockup/category/banner-{$city->slug}.jpg"
        ]);

        $city->media()->create([
            'slug' => 'thumbnail',
            'filename' => "/img/mockup/category/thumbnail-{$city->slug}.jpg"
        ]);
    }
}
