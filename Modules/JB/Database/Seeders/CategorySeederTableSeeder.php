<?php

namespace Modules\JB\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\JB\Entities\{Category, CategoryTranslation};
use LaravelLocalization;
use Faker\Factory;

class CategorySeederTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (range(1, 20) as $i) {
            $this->createCategory();
        }
        
        foreach(Category::all() as $category) {

            $this->createBanner($category, 'top', 3);

            $this->createBanner($category, 'side', 2);
        }
    }

    /**
     * Create category data
     *
     * @return void
     */
    public function createCategory($parent_id = null)
    {
        $faker = Factory::create();

        $category = Category::create([
            'name' => ucfirst($faker->word),
            'parent_id' => $parent_id
        ]);

        $this->createBanner($category, 'top', 3);

        $this->createBanner($category, 'side', 2);

        $this->createMedia($category);

        if($parent_id === null) {
            foreach (range(1, rand(3, 12)) as $i) {
                $this->createCategory($category->id);
            }
        }
    }

    public function createMedia($category)
    {
        $category->media()->create([
            'slug' => 'image',
            'filename' => '/img/mockup/category/image.jpg'
        ]);

        $category->media()->create([
            'slug' => 'icon',
            'filename' => '/img/mockup/category/icon.png'
        ]);
    }

    public function createBanner($category, $group, $amount)
    {
        foreach (range(1, $amount) as $i) {
            $banner = $category->createBanner($group, now(), now()->addDays(rand(4, 5)));

            $banner->media();
        }
    }
}
