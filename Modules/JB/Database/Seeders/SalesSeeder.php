<?php

namespace Modules\JB\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\JB\Entities\{Sales};
use Faker\Factory;

class SalesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        foreach (range(1, 30) as $i) {
            Sales::create([
                'name' => $faker->name,
                'address' => $faker->address,
                'phone' => $faker->phoneNumber,
                'email' => $faker->freeEmail,
                'gender' => $faker->randomElement(['M', 'F']),
                'birthday' => $faker->date
            ]);
        }
    }
}
