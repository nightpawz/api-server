<?php

namespace Modules\JB\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class JBDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call([
            CitySeeder::class,
            CategorySeederTableSeeder::class,
            ListingSeederTableSeeder::class,
            MemberSeeder::class,
            SalesSeeder::class,
            PlanSeeder::class,
        ]);
    }
}
