<?php

namespace Modules\JB\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\JB\Entities\{Plan};
use Faker\Factory;

class PlanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Plan::create([
            'name' => 'Monthly',
            'description' => '',
            'price' => 75000,
            'duration' => 30
        ]);

        Plan::create([
            'name' => 'Quarterly',
            'description' => '',
            'price' => 170000,
            'duration' => 90
        ]);

        Plan::create([
            'name' => 'Semi Annually',
            'description' => '',
            'price' => 320000,
            'duration' => 180
        ]);

        Plan::create([
            'name' => 'Annually',
            'description' => '',
            'price' => 600000,
            'duration' => 365
        ]);
    }
}
