<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{{ config('app.name', 'Laravel') }} - Admin</title>

       {{-- Laravel Mix - CSS File --}}
       <link rel="stylesheet" href="{{ mix('css/antdesign.css') }}">
       <link rel="stylesheet" href="//unpkg.com/vue-multiselect@2.1.0/dist/vue-multiselect.min.css">

    </head>
    <body>
        <div id="app">
            <app />
        </div>

        {{-- Laravel Mix - JS File --}}
        <script src="{{ mix('js/manifest.js') }}"></script>
        <script src="{{ mix('js/vendor.js') }}"></script>
        <script src="{{ mix('js/antdesign.js') }}"></script>
    </body>
</html>
