global.Vue = require('vue')
global.axios = require('axios')

import VueRouter from 'vue-router'
import toast from './services/toast'
require('./plugins/initAntDesign')

global.$eventHub = Vue.prototype.$eventHub = new Vue(); // Global event bus


axios.interceptors.response.use(
    response => {
        $eventHub.$emit('after-response')

        if(response.data.hasOwnProperty('message')) {
            toast.success(response.data.message)
        }

        return response
    },
    error => {
        $eventHub.$emit('response-error')
        if( error.config.hasOwnProperty('errorHandle') && error.config.errorHandle === false ) {
            return Promise.reject(error)
        }

        if (typeof error.response !== 'undefined') {

            // Setup Generic Response Messages
            if(error.response.status === 401){

                toast.error('401 Action UnAuthorized')

            }else if(error.response.status === 404){

                toast.error('404 API Route is Missing or Undefined')

            }else if(error.response.status === 405){

                toast.error('405 API Route Method Not Allowed')

            }else if(error.response.status === 422){

                let errors = error.response.data.errors

                for (var i in errors) {
                    if (errors.hasOwnProperty(i)) {
                        toast.error(errors[i][0])
                    }
                }

            }else if(error.response.status >= 500){

                toast.error('500 Internal Server Error')

            }else {
                toast.error(error.response.data.message)
            }
        }

        return Promise.reject(error)

    }
)
