/*
|-------------------------------------------------------------------------------
| VUEX store.js
|-------------------------------------------------------------------------------
| Builds the data store from all of the modules for the Roast app.
*/
/*
  Adds the promise polyfill for IE 11
*/
require('es6-promise').polyfill();

/*
    Imports Vue and Vuex
*/
import Vue from 'vue'
import Vuex from 'vuex'

/*
    Initializes Vuex on Vue.
*/
Vue.use( Vuex )

/*
    Imports all of the modules used in the application to build the data store.
*/
import { banners } from './store/banners.js'
import { members } from './store/members.js'
import { sales } from './store/sales.js'
import { categories } from './store/categories.js'
import { listings } from './store/listings.js'
import { plans } from './store/plans.js'

/*
  Exports our data store.
*/
export default new Vuex.Store({
    modules: {
        banners,
        categories,
        listings,
        plans,
        members,
        sales
    }
});
