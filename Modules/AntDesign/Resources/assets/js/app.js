import router from './routes/index.js'
import store from './store.js'
import App from './views/App'

require('./bootstrap')

import FeMain from './views/components/FeMain'
Vue.component('fe-main', FeMain)

const app = new Vue({
    router,
    store,
    components: { App }
}).$mount('#app')
