import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

import Hello from './views/Hello'
import Home from './views/Home'
import Plan from './views/pages/plans/Plan'
import Member from './views/pages/members/Member'
import Category from './views/pages/categories/Category'
import Listing from './views/pages/listings/Listing'
import Banner from './views/pages/banners/Banner'
import CreateBanner from './views/pages/banners/CreateBanner'
import EditBanner from './views/pages/banners/EditBanner'

const router = new VueRouter({
    mode: 'history',
    base: '/admin',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/hello',
            name: 'hello',
            component: Hello,
        },
        {
            path: '/plans',
            name: 'plans',
            component: Plan
        },
        {
            path: '/members',
            name: 'members',
            component: Member
        },
        {
            path: '/banners/create',
            name: 'banners.create',
            component: CreateBanner
        },
        {
            path: '/banner/:id/edit',
            props: true,
            name: 'banners.edit',
            component: EditBanner
        },
        {
            path: '/banners',
            name: 'banners',
            component: Banner
        },
        {
            path: '/categories',
            name: 'categories',
            component: Category
        },
        {
            path: '/listings',
            name: 'listings',
            component: Listing
        }
    ],
});

export default router
