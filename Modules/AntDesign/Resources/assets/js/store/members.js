import router from '../routes/index'

export const members = {
    state: {
        loading: false,
        data: [],
        pagination: {},
        selected: {
            name: null,
            company: null,
            phone: null,
            email: null,
            gender: null,
            birthday: null
        }
    },
    actions: {
        fetchMembers({ commit, dispatch }, filter = {}) {
            axios.get(`/api/members`, {params: filter})
                .then((response) => {
                    commit('FETCH_MEMBER', response.data.data)
                    commit('FETCH_MEMBER_PAGINATION', {
                        meta: response.data.meta,
                        links: response.data.links
                    })
                })
        },
        createMembers({ commit }, data) {
            axios.post('/api/members', data)
                .then(() => router.push({ name: 'members' }))
        },
        updateMembers({ commit }, data, id) {
            axios.post(`/api/members/${id}`, data)
                .then(() => router.push({ name: 'members' }))
        },
        deleteMembers({ commit, dispatch }, id) {
            axios.post(`/api/members/${id}`, {_method: 'DELETE'})
                .then(() => dispatch('fetchMembers'))
        },
        showMembers({ commit }, id){
            axios.get(`/api/members/${id}`)
                .then((res) => commit('FETCH_SELECTED_MEMBER', res.data.data))
        }
    },
    mutations: {
        FETCH_MEMBER(state, members) {
            return state.data = members
        },
        FETCH_MEMBER_PAGINATION(state, pagination) {
            return state.pagination = pagination
        },
        FETCH_SELECTED_MEMBER(state, member) {
            return state.selected = member
        }
    },
    getters: {
        members(state) {
            return state.data
        },
        membersPagination(state) {
            return state.pagination
        }
    }
}
