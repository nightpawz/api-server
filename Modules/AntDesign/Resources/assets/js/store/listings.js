export const listings = {
    state: {
        loading: false,
        data: [],
        pagination: {},
        selected: {
            
        }
    },
    actions: {
        fetchListings({ commit, dispatch }, filter = {}) {
            axios.get(`/api/listings`, {params: filter})
                .then((response) => {
                    commit('FETCH_LISTING', response.data.data)
                    commit('FETCT_LISTING_PAGINATION', {
                        meta: response.data.meta,
                        links: response.data.links
                    })
                })
        },
        createListings({ commit }, data) {
            axios.post('/api/listings', {params: data})
        },
        updateListings({ commit }, data, id) {
            axios.post(`/api/listings/${id}`, {params: data})
        },
        deleteListings({ commit, dispatch }, id) {
            axios.post(`/api/listings/${id}`, {_method: 'DELETE'})
                .then(() => dispatch('fetchListings'))
        },
        showListings({ commit }, id){
            axios.get(`/api/listings/${id}`)
        }
    },
    mutations: {
        FETCH_LISTING(state, listings) {
            return state.data = listings
        },
        FETCT_LISTING_PAGINATION(state, pagination) {
            return state.pagination = pagination
        }
    },
    getters: {
        listings(state) {
            return state.data
        },
        listingsPagination(state) {
            return state.pagination
        }
    }
}
