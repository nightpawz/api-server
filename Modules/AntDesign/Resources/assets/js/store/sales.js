import router from '../routes/index'

export const sales = {
    state: {
        loading: false,
        data: [],
        pagination: {},
        selected: {
            name: null,
            address: null,
            phone: null,
            email: null,
            gender: null,
            birthday: null
        }
    },
    actions: {
        fetchSales({ commit, dispatch }, filter = {}) {
            axios.get(`/api/sales`, {params: filter})
                .then((response) => {
                    commit('FETCH_SALES', response.data.data)
                    commit('FETCT_SALES_PAGINATION', {
                        meta: response.data.meta,
                        links: response.data.links
                    })
                })
        },
        createSales({ commit }, data) {
            axios.post('/api/sales', data)
                .then(() => router.push({ name: 'sales' }))

        },
        updateSales({ commit, state }, data) {
            axios.put(`/api/sales/${state.selected.id}`, data)
                .then(() => router.push({ name: 'sales' }))
        },
        deleteSales({ commit, dispatch }, id) {
            axios.post(`/api/sales/${id}`, {_method: 'DELETE'})
                .then(() => dispatch('fetchSales'))
        },
        showSales({ commit }, id){
            axios.get(`/api/sales/${id}`)
                .then((res) => commit('FETCH_SELECTED_SALES', res.data.data))
        }
    },
    mutations: {
        FETCH_SALES(state, sales) {
            return state.data = sales
        },
        FETCT_SALES_PAGINATION(state, pagination) {
            return state.pagination = pagination
        },
        FETCH_SELECTED_SALES(state, sales) {
            return state.selected = sales
        }
    },
    getters: {
        sales(state) {
            return state.data
        },
        salesPagination(state) {
            return state.pagination
        }
    }
}
