import router from '../routes/index'

export const plans = {
    state: {
        loading: false,
        data: [],
        pagination: {},
        selected: {
            name: null,
            description: null,
            duration: null,
            price: null,
        }
    },
    actions: {
        fetchPlans({ commit }, filter = {}) {
            axios.get(`/api/plans`, {params: filter})
                .then((response) => {
                    commit('FETCH_PLAN', response.data.data)
                    commit('FETCT_PLAN_PAGINATION', {
                        meta: response.data.meta,
                        links: response.data.links
                    })
                })
        },
        createPlans({ commit }, data) {
            axios.post('/api/plans', data)
                .then(() => router.push({ name: 'plans' }))
        },
        updatePlans({ commit }, data, id) {
            axios.put(`/api/plans/${id}`, data)
                .then(() => router.push({ name: 'plans' }))
        },
        deletePlans({ commit, dispatch }, id) {
            axios.post(`/api/plans/${id}`, {_method: 'DELETE'})
                .then(() => dispatch('fetchPlans'))
        },
        showPlans({ commit }, id){
            axios.get(`/api/plans/${id}`)
                .then((res) => commit('FETCH_SELECTED_PLAN', res.data.data))
        }
    },
    mutations: {
        FETCH_PLAN(state, plans) {
            return state.data = plans
        },
        FETCT_PLAN_PAGINATION(state, pagination) {
            return state.pagination = pagination
        },
        FETCH_SELECTED_PLAN(state, plans) {
            return state.selected = plans
        }
    },
    getters: {
        plans(state) {
            return state.data
        },
        plansPagination(state) {
            return state.pagination
        }
    }
}
