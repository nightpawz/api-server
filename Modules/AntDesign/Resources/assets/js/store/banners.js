import router from '../routes/index'

export const banners = {
  state: {
    loading: false,
    data: [],
    pagination: {},
    selected: {}
  },

  actions: {
    resetSelectedBanner({ commit }) {
      commit('RESET_SELECTED_BANNER')
    },

    setSelectedBanner({ commit }, data) {
      commit('SET_SELECTED_BANNER', data)
    },

    fetchBanners({ commit }, filter = {}) {
      axios.get(`/api/banners`, {params: filter})
        .then((response) => {
          commit('FETCH_BANNER', response.data.data)
          commit('FETCT_BANNER_PAGINATION', {
            meta: response.data.meta,
            links: response.data.links
          })
        })
    },

    createBanner({ state }) {
      axios.post('/api/banners', state.selected)
        .then(() => router.push({ name: 'banners' }))
    },

    updateBanner({ state }) {
      var data = state.selected
      data._method = 'PUT'

      axios.post(`/api/banners/${data.id}`, data)
        .then(() => router.push({ name: 'banners' }))
    },

    deleteBanner({ commit, dispatch }, id) {
      axios.post(`/api/banners/${id}`, {_method: 'DELETE'})
        .then(() => dispatch('fetchBanners'))
    },

    showBanner({ commit }, id){
      axios.get(`/api/banners/${id}`)
        .then((res) => commit('SET_SELECTED_BANNER', res.data))
    }
  },

  mutations: {
    FETCH_BANNER(state, banners) {
      return state.data = banners
    },
    FETCT_BANNER_PAGINATION(state, pagination) {
      return state.pagination = pagination
    },
    RESET_SELECTED_BANNER(state) {
      return state.selected = {}
    },
    SET_SELECTED_BANNER(state, data) {
      return state.selected = Object.assign({}, state.selected, data)
    },
  },

  getters: {
    banners(state) {
      return state.data
    },
    banner(state) {
      return state.selected
    },
    bannersPagination(state) {
      return state.pagination
    }
  }
}
