import router from '../routes/index'

export const categories = {
  state: {
    lang: 'id',
    loading: false,
    data: [],
    pagination: {},
    selected: {}
  },

  actions: {

    fetchCategories({ commit, state }, filter = {}) {
      axios.get(`/api/categories`, {params: { _lang: state.lang }})
        .then((response) => {
          commit('FETCH_CATEGORY', response.data.data)
          commit('FETCT_CATEGORY_PAGINATION', {
            meta: response.data.meta,
            links: response.data.links
          })
        })
    },

    createCategory({ state }) {
      var data =  state.selected

      data._lang = state.lang

      axios.post('/api/categories', data)
        .then(() => router.push({ name: 'categories' }))
    },

    updateCategory({ state }) {
      var data =  state.selected

      data._method = 'PUT'
      data._lang = state.lang

      axios.post(`/api/categories/${data.id}`, data)
        .then(() => router.push({ name: 'categories' }))
    },

    deleteCategory({ commit, dispatch }, id) {
      axios.post(`/api/categories/${id}`, {_method: 'DELETE'})
        .then(() => dispatch('fetchCategories'))
    },

    showCategory({ commit, state }, id){
      axios.get(`/api/categories/${id}`, {params: { _lang: state.lang }})
        .then((res) => {
          $eventHub.$emit('after-show-category')
          commit('SET_SELECTED_CATEGORY', res.data.data)
        })
    },

    resetSelectedCategory({ commit }) {
      commit('RESET_SELECTED_CATEGORY')
    },

    setSelectedCategory({ commit }, data) {
      commit('SET_SELECTED_CATEGORY', data)
    },

    setCategoryLang({ commit }, lang) {
      commit('SET_CATEGORY_LANG', lang)
    }
  },

  mutations: {

    FETCH_CATEGORY(state, categories) {
      return state.data = categories
    },

    FETCT_CATEGORY_PAGINATION(state, pagination) {
      return state.pagination = pagination
    },

    RESET_SELECTED_CATEGORY(state) {
      return state.selected = {}
    },

    SET_SELECTED_CATEGORY(state, data) {
      return state.selected = Object.assign({}, state.selected, data)
    },

    SET_CATEGORY_LANG(state, lang) {
      return state.lang = lang
    }
  },

  getters: {

    category(state) {
      return state.selected
    },

    categories(state) {
      return state.data
    },

    categoriesPagination(state) {
      return state.pagination
    }
  }
}
