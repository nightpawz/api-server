import Listing from '../views/pages/listings/Listing'
import CreateListing from '../views/pages/listings/CreateListing'
import EditListing from '../views/pages/listings/EditListing'

export default [
    {
        path: '/listings/create',
        name: 'listings.create',
        component: CreateListing,
    },
    {
        path: '/listings/:id/edit',
        props: true,
        name: 'listings.edit',
        component: EditListing
    },
    {
        path: '/listings',
        name: 'listings',
        component: Listing,
    }
]
