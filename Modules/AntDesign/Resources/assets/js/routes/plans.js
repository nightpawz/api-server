import Plan from '../views/pages/plans/Plan'
import CreatePlan from '../views/pages/plans/CreatePlan'
import EditPlan from '../views/pages/plans/EditPlan'


export default [
    {
        path: '/plans/create',
        name: 'plans.create',
        component: CreatePlan,
    },
    {
        path: '/plans/:id/edit',
        props: true,
        name: 'plans.edit',
        component: EditPlan
    },
    {
        path: '/plans',
        name: 'plans',
        component: Plan,
    }
]
