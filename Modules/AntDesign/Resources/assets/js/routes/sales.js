import Sales from '../views/pages/sales/Sales'
import CreateSales from '../views/pages/sales/CreateSales'
import EditSales from '../views/pages/sales/EditSales'

export default [
    {
        path: '/sales/create',
        name: 'sales.create',
        component: CreateSales
    },
    {
        path: '/sales/:id/edit',
        props: true,
        name: 'sales.edit',
        component: EditSales
    },
    {
        path: '/sales',
        name: 'sales',
        component: Sales,
    }
]
