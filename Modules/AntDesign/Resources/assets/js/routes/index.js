import Vue from 'vue'
import Router from 'vue-router'
import Home from '../views/Home.vue'

import members from './members'
import sales from './sales'

import plans from './plans'
import categories from './categories'
import listings from './listings'

import banners from './banners'
import toplists from './toplists'

Vue.use(Router)

const baseRoutes = [
    {
        path: '/',
        name: 'home',
        component: Home
    }
]

const routes = baseRoutes
    .concat(members)
    .concat(sales)
    .concat(plans)
    .concat(categories)
    .concat(listings)
    .concat(banners)
    .concat(toplists)


export default new Router({
    mode: 'history',
    base: '/admin',
    routes
})
