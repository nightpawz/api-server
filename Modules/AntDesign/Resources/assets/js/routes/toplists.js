import TopLists from '../views/pages/toplists/TopLists'

export default [
    {
        path: '/toplists',
        name: 'toplists',
        component: TopLists,
    }
]
