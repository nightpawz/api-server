import Member from '../views/pages/members/Member'
import CreateMember from '../views/pages/members/CreateMember'
import EditMember from '../views/pages/members/EditMember'

export default [
    {
        path: '/members/create',
        name: 'members.create',
        component: CreateMember
    },
    {
        path: '/members/:id/edit',
        props: true,
        name: 'members.edit',
        component: EditMember
    },
    {
        path: '/members',
        name: 'members',
        component: Member,
    }
]
