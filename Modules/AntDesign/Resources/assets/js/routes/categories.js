import Category from '../views/pages/categories/Category'
import CreateCategory from '../views/pages/categories/CreateCategory'
import EditCategory from '../views/pages/categories/EditCategory'

export default [
    {
        path: '/categories/create',
        name: 'categories.create',
        component: CreateCategory,
    },
    {
        path: '/categories/:id/edit',
        props: true,
        name: 'categories.edit',
        component: EditCategory
    },
    {
        path: '/categories',
        name: 'categories',
        component: Category,
    }
]
