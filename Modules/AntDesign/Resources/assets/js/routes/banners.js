import Banner from '../views/pages/banners/Banner'
import EditBanner from '../views/pages/banners/EditBanner'
import CreateBanner from '../views/pages/banners/CreateBanner'

export default [
  {
      path: '/banners/create',
      name: 'banners.create',
      component: CreateBanner,
  },
  {
      path: '/banners/:id/edit',
      props: true,
      name: 'banners.edit',
      component: EditBanner
  },
  {
      path: '/banners',
      name: 'banners',
      component: Banner,
  }
]
