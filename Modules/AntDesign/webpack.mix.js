const { mix } = require('laravel-mix');
require('laravel-mix-merge-manifest');

mix.setPublicPath('../../public').mergeManifest();

mix.js(__dirname + '/Resources/assets/js/app.js', 'js/antdesign.js')
    .sass( __dirname + '/Resources/assets/sass/app.scss', 'css/antdesign.css');

mix.version();

mix.extract(['ant-design-vue', 'vue', 'vue-router']);

mix.browserSync({
    proxy: "http://jb.imaginer.id",
    files: [
        'public/js/antdesign.js',
        'public/css/antdesign.css',
        'Modules/**/*.php',
    ],
    open: false
});
