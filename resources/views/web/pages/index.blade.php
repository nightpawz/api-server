@extends('web.layouts.app')

@section('title', "{$page->name} - {$page->title}")

@section('content')

    @foreach ($page->sections()->onlyParents()->orderBy('section_order')->get() as $section)
        @include("web.pages._sections.{$page->view}-{$section->name}", ['section' => $section])
    @endforeach

@endsection
