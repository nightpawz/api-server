<section class="fe-carousel">

    @wcarousel([
        'id' => 'home-carousel'
    ])

        @foreach ($section->childs as $item)
            @wcarouselitem([
                'active' => ($loop->first ? true : false),
                'src' => $item->image_url ?? ''
            ])

                <!-- the captions -->

            @endwcarouselitem
        @endforeach

        <div class="fe-carousel-caption">
            <div class="fluid">
                <div class="caption-wrapper">
                    <h1 class="fe-carousel-title">{{ __('Explore The City Of') }} <span class="cl-primary">Yogyakarta</span></h1>
                    <p class="fe-carousel-subtitle">{{ $section->subtitle ?? '' }}</p>
                </div>

                <div class="fe-carousel-menu">
                    @include('web.pages._sections._partials.filter-form')
                </div>
            </div>
        </div>

    @endwcarousel

    <div class="fe-overlay"></div>

</section>
