<form id="filterForm" action="{{ route('listings.index') }}">
    <div class="main-search-input fl-wrap">
        <div class="main-search-input-item">
            <input type="text" name="search" placeholder="Keywords:" value="">
            <input type="hidden" name="sort" id="filterSort" value="">
        </div>
        <div class="main-search-input-item location" id="autocomplete-container">
            <select data-placeholder="Location" name="location">
                <option value="" selected="">{{ __('Location') }}</option>
                @foreach (Modules\JB\Entities\City::all() as $item)
                    <option value="{{ $item->slug }}">{{ $item->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="main-search-input-item">
            <select data-placeholder="All Categories" name="category">
                <option value="" selected="">{{ __('Category') }}</option>

                @foreach (Modules\JB\Entities\Category::onlyParents()->get() as $item)
                    <option value="{{ $item->slug ?? '' }}">{{ $item->name ?? '' }}</option>
                @endforeach
            </select>
        </div>
        <button class="main-search-button ladda-button" type="submit" data-style="expand-right"><span class="ladda-label">Search
            </span><span class="ladda-spinner"></span></button>
    </div>
</form>
