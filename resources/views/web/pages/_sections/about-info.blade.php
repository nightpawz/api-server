@wsection([
    'bg_text' => $section->bg_text ?? '',
    'title' => $section->title ?? '',
    'subtitle' => $section->subtitle ?? '',
    'class' => 'fe-section-left jb-about-info'
])

<div class="container">
    <div class="row">

        @foreach ($section->childs as $item)
            <div class="col-xs-12 col-md-6 no-gutter">
                @wimg([
                    'src' => $item->image_url ?? '',
                    'ratio' => 'p-57',
                    'alt' => $item->title ?? ''
                ])
            </div>
            <div class="col-xs-12 col-md-6 no-gutter fe-align-center">
                <h2 class="fe-title">{{ $item->title ?? '' }}</h2>
                <div class="fe-content">{!! $item->content ?? '' !!}</div>
            </div>
        @endforeach
        <div class="col-12">

        </div>
    </div>
</div>

@endwsection
