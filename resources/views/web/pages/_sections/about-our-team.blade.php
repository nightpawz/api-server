@wsection([
    'bg_text' => $section->bg_text ?? '',
    'title' => $section->title ?? '',
    'subtitle' => $section->subtitle ?? '',
    'class' => 'jb-about-team'
])

    @wswiper([
        'class' => 'jb-swiper',
        'navigation' => true
    ])

        @foreach (Modules\Eris\Entities\Team::all() as $item)
            @wswiperslide([
                'class' => 'jb-swiper-slide',
                'width' => '250px'
            ])

                <div class="jb-recomend-card">
                    @wimg(['ratio' => 'r-4-3'])

                    <div class="jb-swiper-caption">
                        <div class="info-listing">
                            <a href="" class="jb-listing-link">Toko Bapak</a>
                            <p class="jb-listing-info">Beli tiga sendok disini gratis piring cantik.</p>
                        </div>
                        <div class="info-bottom">
                            <div class="jb-category-icon">
                                @wimg(['ratio' => 'r-1-1'])
                            </div>
                            <a href="" class="jb-category-link">Akomodasi</a>
                            <span class="jb-listing-status listing-open">Open</span>
                        </div>
                    </div>
                </div>

            @endwswiperslide
        @endforeach

    @endwswiper


@endwsection
