<div class="jb-banner-header">

    <div class="listing-header">
        @wimg([
            'src' => $section->image_url ?? '',
            'ratio' => 'p-21'
        ])

        <span class="overlay"></span>        
    </div>

    <div class="fe-carousel-caption">
        <div class="caption-wrapper">
            <h1 class="fe-carousel-title">{{ $section->title ?? '' }}</h1>
            <p class="fe-carousel-subtitle">{{ $section->subtitle ?? '' }}</p>
        </div>
    </div>
</div>
