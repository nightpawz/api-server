@wsection([
    'bg_text' => $section->bg_text ?? '',
    'title' => $section->title ?? '',
    'subtitle' => $section->subtitle ?? '',
    'class' => 'fe-city'
])

    <div class="container">
        <div class="row justify-content-center">

            @foreach (Modules\JB\Entities\City::withCount(['listings'])->orderBy('id', 'asc')->get() as $item)
                <div class="col-xs-12 {{ false ? 'col-sm-12 col-md-12 col-lg-12' : 'col-sm-6 col-md-6 col-lg-6' }} no-gutter">
                    <div class="fe-location-card">
                        <div class="fe-zoom-img">
                            @wimg([
                                'src' => $item->thumbnail_url,
                                'alt' => $item->name,
                                'ratio' => 'r-4-2'
                            ])
                        </div>

                        <div class="fe-overlay"></div>

                        <div class="location-info">
                            <h3 class="title"><a href="{{ route('listings.index', ['location' => $item->slug]) }}">{{ $item->name }}</a></h3>
                            <p class="meta"><i class="fa fa-edit"></i> {{ $item->listings_count }} {{ __('Listings') }}</p>
                        </div>
                    </div>
                </div>
            @endforeach

        </div>
    </div>

@endwsection
