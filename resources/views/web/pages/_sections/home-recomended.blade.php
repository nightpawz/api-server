@wswiper([
    'class' => 'fe-swiper jb-swiper',
    'navigation' => false
])

@php
    $listings = Modules\JB\Entities\Enterprise::latest()->get();
@endphp

    @foreach ($listings as $item)
        @wswiperslide([
            'class' => 'jb-swiper-slide',
            'width' => '250px'
        ])

            <div class="jb-recomend-card">
                @wimg([
                    'ratio' => 'r-4-3',
                    'src' => $item->listing->thumbnail_url ?? '',
                    'class' => 'fe-thumbnail'
                ])

                <div class="fe-overlay"></div>

                <div class="jb-swiper-caption">
                    <div class="info-listing">
                        <a href="{{ route('listings.show', $item->listing->slug) }}" class="jb-listing-link">{{ $item->listing->name ?? '' }}</a>
                        <p class="jb-listing-info">{{ $item->listing->excerpt ?? '' }}</p>
                    </div>
                    <div class="info-bottom">
                        <div class="jb-category-icon">
                            @wimg([
                                'ratio' => 'r-1-1',
                                'src' => $item->listing->category->parent->icon_url ?? ''
                            ])
                        </div>
                        <a href="" class="jb-category-link">{{ $item->listing->category->name ?? '' }}</a>
                        <span class="jb-listing-status listing-open">Open</span>
                    </div>
                </div>
            </div>

        @endwswiperslide
    @endforeach

@endwswiper
