@wsection([
    'bg_text' => '',
    'title' => '',
    'class' => 'jb-contact'
])

<div class="container">
    <div class="row">
        <div class="col-12">
            <h2 class="text-center fe-heading-title">{{ $section->title ?? '' }}</h2>
            <div class="text-content fe-content">{!! $section->content ?? '' !!}</div>
        </div>

        <div class="col-xs-12 col-sm-4 col-md-4">
            <div class="card-contact">
                <div class="card-contact-header">
                    <i class="fa fa-info"></i> {{ __('Contact Info') }}
                </div>
                <div class="card-contact-body">
                    <a class="text-elipsis">
                        <span class="icon-container"><i class="fa fa-map-marker"></i></span>
                        {{ setting('contacts.address') }}
                    </a>
                    <a class="text-elipsis">
                        <span class="icon-container"><i class="fa fa-phone"></i></span>
                        {{ setting('contacts.phone') }}
                    </a>
                    <a class="text-elipsis">
                        <span class="icon-container"><i class="fa fa-envelope"></i></span>
                        {{ setting('contacts.email') }}
                    </a>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-8 col-md-8">
            <div class="card-contact">
                <div class="card-contact-header">
                    <i class="fa fa-send"></i> {{ __('Contact Us') }}
                </div>

                <div class="card-contact-body">
                    <form action="{{ route('contact.store') }}" enctype="">
                        <div class="form-group">
                            <label class="control-label">{{ __('Name') }}</label>
                            <input type="text" class="form-control">
                        </div>
                        <div class="form-group">
                            <label class="control-label">{{ __('Email') }}</label>
                            <input type="text" class="form-control">
                        </div>
                        <div class="form-group">
                            <label class="control-label">{{ __('Subject') }}</label>
                            <input type="text" class="form-control">
                        </div>
                        <div class="form-group">
                            <label class="control-label">{{ __('Message') }}</label>
                            <textarea name="name" rows="4" class="form-control" cols="80"></textarea>
                        </div>

                        <button class="btn btn-primary btn-block">{{ __('Submit') }}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endwsection
