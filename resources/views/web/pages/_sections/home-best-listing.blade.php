@wsection([
    'bg_text' => $section->bg_text ?? '',
    'title' => $section->title ?? '',
    'subtitle' => $section->subtitle ?? '',
    'class' => 'jb-best-listing'
])

<div class="container">
    <div class="row">
        <div class="col-12">

            <ul class="nav nav-tabs justify-content-center fe-nav" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#latest" role="tab" aria-controls="latest" aria-selected="true">
                        <i class="fa fa-thumb-tack"></i>{{ __('Latest Listings') }}
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#popular" role="tab" aria-controls="popular" aria-selected="false">
                        <i class="fa fa-star-o"></i>{{ __('Popular Ratings') }}
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#near" role="tab" aria-controls="near" aria-selected="false">
                        <i class="fa fa-map-marker"></i>{{ __('Near By You') }}
                    </a>
                </li>
            </ul>
            <div class="tab-content fe-tab-content">
                <div class="tab-pane fade show active" id="latest" role="tabpanel" aria-labelledby="latest-tab">
                    <div class="row">
                        @each('web._jb.card-listing', $latest_listings, 'listing')
                    </div>
                </div>
                <div class="tab-pane fade" id="popular" role="tabpanel" aria-labelledby="popular-tab">
                    <p>popular</p>
                </div>
                <div class="tab-pane fade" id="near" role="tabpanel" aria-labelledby="near-tab">
                    near
                </div>
            </div>

        </div>
    </div>
</div>

@endwsection
