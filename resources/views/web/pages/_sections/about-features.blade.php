@wsection([
    'bg_text' => $section->bg_text ?? '',
    'title' => $section->title ?? '',
    'subtitle' => $section->subtitle ?? '',
    'class' => 'jb-features'
])

<div class="container">
    <div class="row">

        @foreach ($section->childs as $item)
            <div class="col-xs-12 col-sm-4">

                <div class="jb-card-icon">
                    <p class="card-icon"><i class="{{ $item->font_icon ?? '' }}"></i></p>
                    <h2 class="fe-title">{{ $item->title ?? '' }}</h2>
                    <p class="fe-subtitle">{{ $item->subtitle ?? '' }}</p>
                </div>

            </div>
        @endforeach
        <div class="col-12">

        </div>
    </div>
</div>

@endwsection
