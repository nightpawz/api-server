@wsection([
    'bg_text' => $section->bg_text ?? '',
    'title' => $section->title ?? '',
    'subtitle' => $section->subtitle ?? ''
])

    <div class="container">
        <div class="row">

            @each('web._jb.card-category', $categories, 'category')

        </div>

        <div class="row">
            <div class="col-12 text-right">
                <a href="{{ route('categories.index') }}" class="btn btn-link cl-primary">{{ __('All category') }}</a>
            </div>
        </div>
    </div>

@endwsection
