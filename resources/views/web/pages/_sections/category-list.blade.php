<div class="jb-categories">
    <div class="container">
        <div class="row">

            @each('web._jb.card-category', $categories, 'category')

        </div>

    </div>
</div>
