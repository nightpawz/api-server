<div class="detail-navtop_module__zo_OS js-detail-navtop">
    <div class="container">
        {{-- <div class="detail-navtop_right__KPAlw"><a target="_blank" href="#" class="wil-btn wil-btn--primary2 wil-btn--round wil-btn--md wil-btn--block"><i class="la la-cart-plus"></i> Shop Now </a></div> --}}
        <nav class="detail-navtop_nav__1j1Ti">
            <ul class="list_module__1eis9 list-none list_horizontal__7fIr5" style="min-height: 70px;">
                <li data-tab-key="home" class="list_item__3YghP active"><a href="#home" class="list_link__2rDA1 text-ellipsis color-primary--hover"><span class="list_icon__2YpTp"><i class="la la-home"></i></span>
                        <span class="list_text__35R07">Home</span></a></li>
                <li data-tab-key="content" class="list_item__3YghP"><a href="#content" class="list_link__2rDA1 text-ellipsis color-primary--hover"><span class="list_icon__2YpTp"><i class="la la-file-text"></i></span>
                        <span class="list_text__35R07">Description</span></a></li>
                <li data-tab-key="tags" class="list_item__3YghP"><a href="#tags" class="list_link__2rDA1 text-ellipsis color-primary--hover"><span class="list_icon__2YpTp"><i class="la la-list-alt"></i></span>
                        <span class="list_text__35R07">Listing Features</span></a></li>
                <li data-tab-key="photos" class="list_item__3YghP"><a href="#photos" class="list_link__2rDA1 text-ellipsis color-primary--hover"><span class="list_icon__2YpTp"><i class="la la-image"></i></span>
                        <span class="list_text__35R07">Photos</span></a></li>
                {{-- <li data-tab-key="videos" class="list_item__3YghP"><a href="https://wilcity.com/listing/chontaduro-barcelona/?tab=videos" class="list_link__2rDA1 text-ellipsis color-primary--hover"><span class="list_icon__2YpTp"><i class="la la-video-camera"></i></span>
                        <span class="list_text__35R07">Videos</span></a></li>
                <li data-tab-key="events" class="list_item__3YghP"><a href="https://wilcity.com/listing/chontaduro-barcelona/?tab=events" class="list_link__2rDA1 text-ellipsis color-primary--hover"><span class="list_icon__2YpTp"><i class="la la-calendar-check-o"></i></span>
                        <span class="list_text__35R07">Events</span></a></li>
                <li data-tab-key="reviews" class="list_item__3YghP"><a href="https://wilcity.com/listing/chontaduro-barcelona/?tab=reviews" class="list_link__2rDA1 text-ellipsis color-primary--hover"><span class="list_icon__2YpTp"><i class="la la-star-o"></i></span>
                        <span class="list_text__35R07">Reviews</span></a></li> --}}
            </ul>
        </nav>
    </div>
</div>
