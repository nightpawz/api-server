{{-- <div class="content-box_module__333d9 wilcity-single-listing-events-box">
    <header class="content-box_header__xPnGx clearfix">
        <div class="wil-float-left">
            <h4 class="content-box_title__1gBHS"><i class="la la-calendar-check-o"></i><span>Events</span></h4>
        </div>
        <div class="wil-float-right"><a href="https://wilcity.com/add-listing-plan/?listing_type=event&amp;parentID=2018" class="fs-13 color-primary">+ Create an event</a></div>
    </header>
    <div class="content-box_body__3tSRB">
        <div data-col-xs-gap="10" class="row">
            <div class="col-sm-6 js-listing-grid wilcity-grid">
                <div class=" js-grid-item  ">
                    <div class="event_module__2zicF wil-shadow mb-30 mb-sm-20 js-event" style="z-index: 12;">
                        <header class="event_header__u3oXZ"><a href="https://wilcity.com/event/wall-to-wall-george/">
                                <div class="event_img__1mVnG pos-a-full bg-cover" style="background-image: url(&quot;https://i1.wp.com/wilcity.com/wp-content/uploads/2018/05/Toronto_2_d.jpg?fit=450%2C300&amp;ssl=1&quot;);"><img
                                        src="https://i1.wp.com/wilcity.com/wp-content/uploads/2018/05/Toronto_2_d.jpg?fit=450%2C300&amp;ssl=1" alt="Wall to Wall George"></div>
                            </a></header>
                        <div class="js-grid-item-body event_body__BfZIC">
                            <div class="event_calendar__2x4Hv"><span class="event_month__S8D_o color-primary">Aug</span> <span class="event_date__2Z7TH">31</span></div>
                            <div class="event_content__2fB-4">
                                <h2 class="event_title__3C2PA"><a href="https://wilcity.com/event/wall-to-wall-george/">Wall to Wall George</a></h2>
                                <ul class="event_meta__CFFPg list-none">
                                    <li class="event_metaList__1bEBH text-ellipsis"><span>Friday 8:00 AM</span></li>
                                    <li class="event_metaList__1bEBH text-ellipsis"><a href="https://www.google.com/maps/search/Barcelona,%20Spain" target="_blank"><span>Barcelona, Spain</span></a></li>
                                    <li class="event_metaList__1bEBH text-ellipsis"><span>1 person interested</span></li>
                                </ul>
                            </div>
                        </div>
                        <footer class="js-grid-item-footer event_footer__1TsCF"><span class="event_by__23HUz"> Hosted By <a href="https://wilcity.com/author/user1/" class="color-dark-2">Jeffrey Ruiz</a></span>
                            <div class="event_right__drLk5 pos-a-center-right"><span data-tooltip="Interested" data-post-id="3427" data-tooltip-placement="top" class="wilcity-js-favorite event_interested__2RxI- is-event"><i class="la la-star-o"></i></span></div>
                        </footer>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 js-listing-grid wilcity-grid">
                <div class=" js-grid-item  ">
                    <div class="event_module__2zicF wil-shadow mb-30 mb-sm-20 js-event" style="z-index: 11;">
                        <header class="event_header__u3oXZ"><a href="https://wilcity.com/event/movistar-curiosus-fest/">
                                <div class="event_img__1mVnG pos-a-full bg-cover" style="background-image: url(&quot;https://i0.wp.com/wilcity.com/wp-content/uploads/2018/05/Barcelona_2_b.jpg?fit=450%2C300&amp;ssl=1&quot;);"><img
                                        src="https://i0.wp.com/wilcity.com/wp-content/uploads/2018/05/Barcelona_2_b.jpg?fit=450%2C300&amp;ssl=1" alt="Movistar Curiosus Fest"></div>
                            </a></header>
                        <div class="js-grid-item-body event_body__BfZIC">
                            <div class="event_calendar__2x4Hv"><span class="event_month__S8D_o color-primary">Sep</span> <span class="event_date__2Z7TH">05</span></div>
                            <div class="event_content__2fB-4">
                                <h2 class="event_title__3C2PA"><a href="https://wilcity.com/event/movistar-curiosus-fest/">Movistar Curiosus Fest</a></h2>
                                <ul class="event_meta__CFFPg list-none">
                                    <li class="event_metaList__1bEBH text-ellipsis"><span>Wednesday 8:00 AM</span></li>
                                    <li class="event_metaList__1bEBH text-ellipsis"><a href="https://www.google.com/maps/search/Carrer%20d'Aribau,%20254,%2008006,%20Barcelona,%20Spain" target="_blank"><span>Carrer d'Aribau, 254,
                                                08006, Barcelona, Spain</span></a></li>
                                    <li class="event_metaList__1bEBH text-ellipsis"><span>1 person interested</span></li>
                                </ul>
                            </div>
                        </div>
                        <footer class="js-grid-item-footer event_footer__1TsCF"><span class="event_by__23HUz"> Hosted By <a href="https://wilcity.com/author/user1/" class="color-dark-2">Jeffrey Ruiz</a></span>
                            <div class="event_right__drLk5 pos-a-center-right"><span data-tooltip="Interested" data-post-id="3554" data-tooltip-placement="top" class="wilcity-js-favorite event_interested__2RxI- is-event"><i class="la la-star-o"></i></span></div>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer class="content-box_footer__kswf3"><a href="https://wilcity.com/listing/chontaduro-barcelona/?tab=events" class="list_link__2rDA1 text-ellipsis color-primary--hover wil-text-center"> See All </a></footer>
</div> --}}
