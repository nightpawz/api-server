{{-- <div class="add-review_module__2KOQC">
    <div class="add-review_wrap__K1JtF">
        <div class="add-review_left__ziIv1">
            <h3 class="add-review_title__3ePFu"><span class="color-primary">20</span> Reviews For Chontaduro Barcelona</h3>
        </div>
        <div class="add-review_right__31XA0"><a href="#" class="wil-btn wil-btn--primary wil-btn--sm wil-btn--round"><i class="la la-star-o"></i> Add a review
            </a></div>
    </div>
</div> --}}
{{-- <div class="comment-review_module__-Z5tr wilcity-js-review-item-6492">
    <div class="comment-review_header__1si3M">
        <div class="utility-box-1_module__MYXpX utility-box-1_boxLeft__3iS6b clearfix utility-box-1_sm__mopok  review-author-avatar">
            <div class="utility-box-1_avatar__DB9c_ rounded-circle" style="background-image: url(&quot;https://wilcity.com/wp-content/uploads/2018/08/avatar_default.jpg&quot;);"><img src="https://wilcity.com/wp-content/uploads/2018/08/avatar_default.jpg"
                    alt=""></div>
            <div class="utility-box-1_body__8qd9j">
                <div class="utility-box-1_group__2ZPA2">
                    <h3 class="utility-box-1_title__1I925">abl</h3>
                </div>
                <div class="utility-box-1_description__2VDJ6 wilcity-review-date-6492">Feb 02, 2019</div>
            </div>
        </div>
        <div class="comment-review_abs__9mb1G pos-a-center-right">
            <div class="rated-small_module__1vw2B rated-small_style-1__2lG7u ml-20">
                <div data-rated="4" class="rated-small_wrap__2Eetz wilcity-data-average-review-score-6492 ">
                    <div class="rated-small_overallRating__oFmKR wilcity-average-review-score-6492">4</div>
                    <div class="rated-small_ratingWrap__3lzhB">
                        <div class="rated-small_maxRating__2D9mI">10</div>
                        <div class="rated-small_ratingOverview__2kCI_ wilcity-review-quality-6492">Terrible</div>
                    </div>
                </div>
            </div>
            <div class="dropdown_module__J_Zpj ml-20">
                <div data-toggle-button="dropdown" data-body-toggle="true" class="dropdown_threeDots__3fa2o" style="user-select: none;"><span class="dropdown_dot__3I1Rn"></span><span class="dropdown_dot__3I1Rn"></span><span class="dropdown_dot__3I1Rn"></span></div>
                <div data-toggle-content="dropdown" class="dropdown_itemsWrap__2fuze">
                    <ul data-id="6492" class="list_module__1eis9 list-none list_small__3fRoS list_abs__OP7Og arrow--top-right wilcity-review-toolbar-wrapper">
                        <li class="list_item__3YghP"><a href="#" class="list_link__2rDA1 text-ellipsis color-primary--hover"><span class="list_icon__2YpTp"><i class="la la-flag-o"></i></span><span class="list_text__35R07">Report
                                    review</span></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="comment-review_body__qhUqq">
        <div class="comment-review_content__1jFfZ">
            <h3 class="comment-review_title__2WbAh wilcity-review-title-6492">bngbg</h3>
            <div class="wilcity-review-content wilcity-review-content wilcity-review-content-6492 no-text-show loaded">
                <p>bbbgg</p>
            </div> <a href="#" class="wilcity-expand-text wilcity-show-more color-primary">Show more</a> <a href="#" class="wilcity-expand-text wilcity-show-less color-primary">Show less</a>
        </div>
        <div id="wilcity-review-gallery-6492" class="wilcity-magnific-wrapper"></div>
        <div class="comment-review_meta__1chzm"><span data-count-liked="1" class="wilcity-count-liked-6492">1 Liked</span> <span class="wilcity-count-shared-6492">0 Shared</span></div>
    </div>
    <footer class="comment-review_footer__3XR0_">
        <div class="comment-review_btnGroup__1PqPh">
            <div class="comment-review_btn__32CMP"><a href="#" data-id="6492" class="utility-meta_module__mfOnV wilcity-i-like-it "><i class="la la-thumbs-up"></i>Like</a></div>
            <div class="comment-review_btn__32CMP"><a data-id="6492" href="#" class="utility-meta_module__mfOnV wilcity-add-new-discussion"><i class="la la-comments-o"></i>Comment</a></div>
            <div class="comment-review_btn__32CMP"><a href="#" data-toggle-button="share" data-body-toggle="true" class="utility-meta_module__mfOnV" style="user-select: none;"><i class="la la-share-square-o"></i>Share </a>
                <div data-toggle-content="share" class="comment-review_shareContent__UGmyE">
                    <ul data-postid="6492" class="wilcity-social-sharing-wrapper list_module__1eis9 list-none list_social__31Q0V list_medium__1aT2c list_abs__OP7Og arrow--top-right">
                        <li class="list_item__3YghP"><a target="_blank" href="//facebook.com/sharer.php?u=https%3A%2F%2Fwilcity.com%2Freview%2Fbngbg%2F&amp;t=bngbg" class="wilcity-social-sharing list_link__2rDA1 text-ellipsis color-primary--hover"><span
                                    class="list_icon__2YpTp"><i class="fa fa-facebook"></i></span><span class="list_text__35R07">Facebook</span></a></li>
                        <li class="list_item__3YghP"><a target="_blank" href="//twitter.com/intent/tweet?text=bngbg-https%3A%2F%2Fwilcity.com%2Freview%2Fbngbg%2F&amp;source=webclient" class="wilcity-social-sharing list_link__2rDA1 text-ellipsis color-primary--hover"><span
                                    class="list_icon__2YpTp"><i class="fa fa-twitter"></i></span><span class="list_text__35R07">Twitter</span></a></li>
                        <li class="list_item__3YghP"><a target="_blank" href="//plus.google.com/share?url=https%3A%2F%2Fwilcity.com%2Freview%2Fbngbg%2F&amp;title=bngbg&amp;source=webclient" class="wilcity-social-sharing list_link__2rDA1 text-ellipsis color-primary--hover"><span
                                    class="list_icon__2YpTp"><i class="fa fa-google-plus"></i></span><span class="list_text__35R07">Google+</span></a></li>
                        <li class="list_item__3YghP"><a target="_blank" href="//www.tumblr.com/share/link?url=https%3A%2F%2Fwilcity.com%2Freview%2Fbngbg%2F&amp;name=bngbg&amp;source=webclient" class="wilcity-social-sharing list_link__2rDA1 text-ellipsis color-primary--hover"><span
                                    class="list_icon__2YpTp"><i class="fa fa-tumblr"></i></span><span class="list_text__35R07">Tumblr</span></a></li>
                        <li class="list_item__3YghP"><a target="_blank" href="//vk.com/share.php?url=https%3A%2F%2Fwilcity.com%2Freview%2Fbngbg%2F&amp;description=bngbg&amp;source=webclient" class="wilcity-social-sharing list_link__2rDA1 text-ellipsis color-primary--hover"><span
                                    class="list_icon__2YpTp"><i class="fa fa-vk"></i></span><span class="list_text__35R07">Vk</span></a></li>
                        <li class="list_item__3YghP"><a target="_blank" href="//pinterest.com/pin/create/button/?url=https%3A%2F%2Fwilcity.com%2Freview%2Fbngbg%2F&amp;description=bngbg&amp;source=webclient" class="wilcity-social-sharing list_link__2rDA1 text-ellipsis color-primary--hover"><span
                                    class="list_icon__2YpTp"><i class="fa fa-pinterest"></i></span><span class="list_text__35R07">Pinterest</span></a></li>
                        <li class="list_item__3YghP"><a target="_blank" href="//www.stumbleupon.com/submit?url=https%3A%2F%2Fwilcity.com%2Freview%2Fbngbg%2F&amp;title=bngbg&amp;source=webclient" class="wilcity-social-sharing list_link__2rDA1 text-ellipsis color-primary--hover"><span
                                    class="list_icon__2YpTp"><i class="fa fa-stumbleupon"></i></span><span class="list_text__35R07">Stumbleupon</span></a></li>
                        <li class="list_item__3YghP"><a target="_blank" href="//www.linkedin.com/shareArticle?mini=true&amp;url=https%3A%2F%2Fwilcity.com%2Freview%2Fbngbg%2F&amp;title=bngbg&amp;source=webclient" class="wilcity-social-sharing list_link__2rDA1 text-ellipsis color-primary--hover"><span
                                    class="list_icon__2YpTp"><i class="fa fa-linkedin"></i></span><span class="list_text__35R07">Linkedin</span></a></li>
                        <li class="list_item__3YghP"><a href="mailto:?Subject=bngbg&amp;Body=https://wilcity.com/review/bngbg/" class="wilcity-social-sharing list_link__2rDA1 text-ellipsis color-primary--hover"><span class="list_icon__2YpTp"><i
                                        class="fa fa-envelope"></i></span><span class="list_text__35R07">Email</span></a></li>
                        <li class="list_item__3YghP"><a href="#" data-shortlink="https://wilcity.com/review/bngbg/" data-desc="Press Ctrl+C to copy this link" class="wilcity-copy-link wilcity-social-sharing list_link__2rDA1 text-ellipsis color-primary--hover"><span
                                    class="list_icon__2YpTp"><i class="fa fa-link"></i></span><span class="list_text__35R07">Copy link</span></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div>
            <ul class="comment-review_commentlist__1LH_D list-none" style="display: none;"></ul>
            <!---->
            <div class="comment-review_form__20wWm">
                <div class="utility-box-1_module__MYXpX utility-box-1_xs__3Nipt d-inline-block mr-10 wil-float-left">
                    <div class="utility-box-1_avatar__DB9c_ rounded-circle" style="background-image: url(&quot;https://wilcity.com/wp-content/uploads/2018/08/avatar_default.jpg&quot;);"><img src="https://wilcity.com/wp-content/uploads/2018/08/avatar_default.jpg"
                            alt=""></div>
                </div>
                <div class="comment-review_comment__dJNqv">
                    <div class="field_module__1H6kT field_style4__2DBqx field-autoHeight-2">
                        <div class="field_wrap__Gv92k"><textarea data-height-default="22" class="wilcity-write-a-new-comment-box field_field__3U_Rt"></textarea><span class="field_label__2eCP7 text-ellipsis">Type a message...</span><span
                                class="bg-color-primary"></span>
                            <div class="field_rightButton__1GGWz js-field-rightButton"><span class="field_iconButton__2p3sr bg-color-primary"><i class="la la-arrow-up"></i></span></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div> --}}
{{-- <div class="comment-review_module__-Z5tr wilcity-js-review-item-6307">
    <div class="comment-review_header__1si3M">
        <div class="utility-box-1_module__MYXpX utility-box-1_boxLeft__3iS6b clearfix utility-box-1_sm__mopok  review-author-avatar">
            <div class="utility-box-1_avatar__DB9c_ rounded-circle" style="background-image: url(&quot;https://wilcity.com/wp-content/uploads/2018/08/avatar_default.jpg&quot;);"><img src="https://wilcity.com/wp-content/uploads/2018/08/avatar_default.jpg"
                    alt=""></div>
            <div class="utility-box-1_body__8qd9j">
                <div class="utility-box-1_group__2ZPA2">
                    <h3 class="utility-box-1_title__1I925">Liebe</h3>
                </div>
                <div class="utility-box-1_description__2VDJ6 wilcity-review-date-6307">Jan 28, 2019</div>
            </div>
        </div>
        <div class="comment-review_abs__9mb1G pos-a-center-right">
            <div class="rated-small_module__1vw2B rated-small_style-1__2lG7u ml-20">
                <div data-rated="1" class="rated-small_wrap__2Eetz wilcity-data-average-review-score-6307 ">
                    <div class="rated-small_overallRating__oFmKR wilcity-average-review-score-6307">1</div>
                    <div class="rated-small_ratingWrap__3lzhB">
                        <div class="rated-small_maxRating__2D9mI">10</div>
                        <div class="rated-small_ratingOverview__2kCI_ wilcity-review-quality-6307">Terrible</div>
                    </div>
                </div>
            </div>
            <div class="dropdown_module__J_Zpj ml-20">
                <div data-toggle-button="dropdown" data-body-toggle="true" class="dropdown_threeDots__3fa2o" style="user-select: none;"><span class="dropdown_dot__3I1Rn"></span><span class="dropdown_dot__3I1Rn"></span><span class="dropdown_dot__3I1Rn"></span></div>
                <div data-toggle-content="dropdown" class="dropdown_itemsWrap__2fuze">
                    <ul data-id="6307" class="list_module__1eis9 list-none list_small__3fRoS list_abs__OP7Og arrow--top-right wilcity-review-toolbar-wrapper">
                        <li class="list_item__3YghP"><a href="#" class="list_link__2rDA1 text-ellipsis color-primary--hover"><span class="list_icon__2YpTp"><i class="la la-flag-o"></i></span><span class="list_text__35R07">Report
                                    review</span></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="comment-review_body__qhUqq">
        <div class="comment-review_content__1jFfZ">
            <h3 class="comment-review_title__2WbAh wilcity-review-title-6307">scheisse</h3>
            <div class="wilcity-review-content wilcity-review-content wilcity-review-content-6307 no-text-show loaded">
                <p>scheisse</p>
            </div> <a href="#" class="wilcity-expand-text wilcity-show-more color-primary">Show more</a> <a href="#" class="wilcity-expand-text wilcity-show-less color-primary">Show less</a>
        </div>
        <div id="wilcity-review-gallery-6307" class="wilcity-magnific-wrapper"></div>
        <div class="comment-review_meta__1chzm"><span data-count-liked="1" class="wilcity-count-liked-6307">1 Liked</span> <span class="wilcity-count-shared-6307">0 Shared</span></div>
    </div>
    <footer class="comment-review_footer__3XR0_">
        <div class="comment-review_btnGroup__1PqPh">
            <div class="comment-review_btn__32CMP"><a href="#" data-id="6307" class="utility-meta_module__mfOnV wilcity-i-like-it "><i class="la la-thumbs-up"></i>Like</a></div>
            <div class="comment-review_btn__32CMP"><a data-id="6307" href="#" class="utility-meta_module__mfOnV wilcity-add-new-discussion"><i class="la la-comments-o"></i>Comment</a></div>
            <div class="comment-review_btn__32CMP"><a href="#" data-toggle-button="share" data-body-toggle="true" class="utility-meta_module__mfOnV" style="user-select: none;"><i class="la la-share-square-o"></i>Share </a>
                <div data-toggle-content="share" class="comment-review_shareContent__UGmyE">
                    <ul data-postid="6307" class="wilcity-social-sharing-wrapper list_module__1eis9 list-none list_social__31Q0V list_medium__1aT2c list_abs__OP7Og arrow--top-right">
                        <li class="list_item__3YghP"><a target="_blank" href="//facebook.com/sharer.php?u=https%3A%2F%2Fwilcity.com%2Freview%2Fscheisse%2F&amp;t=scheisse" class="wilcity-social-sharing list_link__2rDA1 text-ellipsis color-primary--hover"><span
                                    class="list_icon__2YpTp"><i class="fa fa-facebook"></i></span><span class="list_text__35R07">Facebook</span></a></li>
                        <li class="list_item__3YghP"><a target="_blank" href="//twitter.com/intent/tweet?text=scheisse-https%3A%2F%2Fwilcity.com%2Freview%2Fscheisse%2F&amp;source=webclient" class="wilcity-social-sharing list_link__2rDA1 text-ellipsis color-primary--hover"><span
                                    class="list_icon__2YpTp"><i class="fa fa-twitter"></i></span><span class="list_text__35R07">Twitter</span></a></li>
                        <li class="list_item__3YghP"><a target="_blank" href="//plus.google.com/share?url=https%3A%2F%2Fwilcity.com%2Freview%2Fscheisse%2F&amp;title=scheisse&amp;source=webclient" class="wilcity-social-sharing list_link__2rDA1 text-ellipsis color-primary--hover"><span
                                    class="list_icon__2YpTp"><i class="fa fa-google-plus"></i></span><span class="list_text__35R07">Google+</span></a></li>
                        <li class="list_item__3YghP"><a target="_blank" href="//www.tumblr.com/share/link?url=https%3A%2F%2Fwilcity.com%2Freview%2Fscheisse%2F&amp;name=scheisse&amp;source=webclient" class="wilcity-social-sharing list_link__2rDA1 text-ellipsis color-primary--hover"><span
                                    class="list_icon__2YpTp"><i class="fa fa-tumblr"></i></span><span class="list_text__35R07">Tumblr</span></a></li>
                        <li class="list_item__3YghP"><a target="_blank" href="//vk.com/share.php?url=https%3A%2F%2Fwilcity.com%2Freview%2Fscheisse%2F&amp;description=scheisse&amp;source=webclient" class="wilcity-social-sharing list_link__2rDA1 text-ellipsis color-primary--hover"><span
                                    class="list_icon__2YpTp"><i class="fa fa-vk"></i></span><span class="list_text__35R07">Vk</span></a></li>
                        <li class="list_item__3YghP"><a target="_blank" href="//pinterest.com/pin/create/button/?url=https%3A%2F%2Fwilcity.com%2Freview%2Fscheisse%2F&amp;description=scheisse&amp;source=webclient" class="wilcity-social-sharing list_link__2rDA1 text-ellipsis color-primary--hover"><span
                                    class="list_icon__2YpTp"><i class="fa fa-pinterest"></i></span><span class="list_text__35R07">Pinterest</span></a></li>
                        <li class="list_item__3YghP"><a target="_blank" href="//www.stumbleupon.com/submit?url=https%3A%2F%2Fwilcity.com%2Freview%2Fscheisse%2F&amp;title=scheisse&amp;source=webclient" class="wilcity-social-sharing list_link__2rDA1 text-ellipsis color-primary--hover"><span
                                    class="list_icon__2YpTp"><i class="fa fa-stumbleupon"></i></span><span class="list_text__35R07">Stumbleupon</span></a></li>
                        <li class="list_item__3YghP"><a target="_blank" href="//www.linkedin.com/shareArticle?mini=true&amp;url=https%3A%2F%2Fwilcity.com%2Freview%2Fscheisse%2F&amp;title=scheisse&amp;source=webclient" class="wilcity-social-sharing list_link__2rDA1 text-ellipsis color-primary--hover"><span
                                    class="list_icon__2YpTp"><i class="fa fa-linkedin"></i></span><span class="list_text__35R07">Linkedin</span></a></li>
                        <li class="list_item__3YghP"><a href="mailto:?Subject=scheisse&amp;Body=https://wilcity.com/review/scheisse/" class="wilcity-social-sharing list_link__2rDA1 text-ellipsis color-primary--hover"><span class="list_icon__2YpTp"><i
                                        class="fa fa-envelope"></i></span><span class="list_text__35R07">Email</span></a></li>
                        <li class="list_item__3YghP"><a href="#" data-shortlink="https://wilcity.com/review/scheisse/" data-desc="Press Ctrl+C to copy this link" class="wilcity-copy-link wilcity-social-sharing list_link__2rDA1 text-ellipsis color-primary--hover"><span
                                    class="list_icon__2YpTp"><i class="fa fa-link"></i></span><span class="list_text__35R07">Copy link</span></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div>
            <ul class="comment-review_commentlist__1LH_D list-none" style="display: none;"></ul>
            <!---->
            <div class="comment-review_form__20wWm">
                <div class="utility-box-1_module__MYXpX utility-box-1_xs__3Nipt d-inline-block mr-10 wil-float-left">
                    <div class="utility-box-1_avatar__DB9c_ rounded-circle" style="background-image: url(&quot;https://wilcity.com/wp-content/uploads/2018/08/avatar_default.jpg&quot;);"><img src="https://wilcity.com/wp-content/uploads/2018/08/avatar_default.jpg"
                            alt=""></div>
                </div>
                <div class="comment-review_comment__dJNqv">
                    <div class="field_module__1H6kT field_style4__2DBqx field-autoHeight-2">
                        <div class="field_wrap__Gv92k"><textarea data-height-default="22" class="wilcity-write-a-new-comment-box field_field__3U_Rt"></textarea><span class="field_label__2eCP7 text-ellipsis">Type a message...</span><span
                                class="bg-color-primary"></span>
                            <div class="field_rightButton__1GGWz js-field-rightButton"><span class="field_iconButton__2p3sr bg-color-primary"><i class="la la-arrow-up"></i></span></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>
<div class="comment-review_module__-Z5tr wilcity-js-review-item-6210">
    <div class="comment-review_header__1si3M">
        <div class="utility-box-1_module__MYXpX utility-box-1_boxLeft__3iS6b clearfix utility-box-1_sm__mopok  review-author-avatar">
            <div class="utility-box-1_avatar__DB9c_ rounded-circle" style="background-image: url(&quot;https://wilcity.com/wp-content/uploads/2018/11/Profil.png&quot;);"><img src="https://wilcity.com/wp-content/uploads/2018/11/Profil.png"
                    alt=""></div>
            <div class="utility-box-1_body__8qd9j">
                <div class="utility-box-1_group__2ZPA2">
                    <h3 class="utility-box-1_title__1I925">dzhem</h3>
                </div>
                <div class="utility-box-1_description__2VDJ6 wilcity-review-date-6210">Jan 21, 2019</div>
            </div>
        </div>
        <div class="comment-review_abs__9mb1G pos-a-center-right">
            <div class="rated-small_module__1vw2B rated-small_style-1__2lG7u ml-20">
                <div data-rated="7.8" class="rated-small_wrap__2Eetz wilcity-data-average-review-score-6210 ">
                    <div class="rated-small_overallRating__oFmKR wilcity-average-review-score-6210">7.8</div>
                    <div class="rated-small_ratingWrap__3lzhB">
                        <div class="rated-small_maxRating__2D9mI">10</div>
                        <div class="rated-small_ratingOverview__2kCI_ wilcity-review-quality-6210">Poor</div>
                    </div>
                </div>
            </div>
            <div class="dropdown_module__J_Zpj ml-20">
                <div data-toggle-button="dropdown" data-body-toggle="true" class="dropdown_threeDots__3fa2o" style="user-select: none;"><span class="dropdown_dot__3I1Rn"></span><span class="dropdown_dot__3I1Rn"></span><span class="dropdown_dot__3I1Rn"></span></div>
                <div data-toggle-content="dropdown" class="dropdown_itemsWrap__2fuze">
                    <ul data-id="6210" class="list_module__1eis9 list-none list_small__3fRoS list_abs__OP7Og arrow--top-right wilcity-review-toolbar-wrapper">
                        <li class="list_item__3YghP"><a href="#" class="list_link__2rDA1 text-ellipsis color-primary--hover"><span class="list_icon__2YpTp"><i class="la la-flag-o"></i></span><span class="list_text__35R07">Report
                                    review</span></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="comment-review_body__qhUqq">
        <div class="comment-review_content__1jFfZ">
            <h3 class="comment-review_title__2WbAh wilcity-review-title-6210">Deneme</h3>
            <div class="wilcity-review-content wilcity-review-content wilcity-review-content-6210 no-text-show loaded">
                <p>deneme deneme</p>
            </div> <a href="#" class="wilcity-expand-text wilcity-show-more color-primary">Show more</a> <a href="#" class="wilcity-expand-text wilcity-show-less color-primary">Show less</a>
        </div>
        <div id="wilcity-review-gallery-6210" class="wilcity-magnific-wrapper"></div>
        <div class="comment-review_meta__1chzm"><span data-count-liked="1" class="wilcity-count-liked-6210">1 Liked</span> <span class="wilcity-count-shared-6210">0 Shared</span></div>
    </div>
    <footer class="comment-review_footer__3XR0_">
        <div class="comment-review_btnGroup__1PqPh">
            <div class="comment-review_btn__32CMP"><a href="#" data-id="6210" class="utility-meta_module__mfOnV wilcity-i-like-it "><i class="la la-thumbs-up"></i>Like</a></div>
            <div class="comment-review_btn__32CMP"><a data-id="6210" href="#" class="utility-meta_module__mfOnV wilcity-add-new-discussion"><i class="la la-comments-o"></i>Comment</a></div>
            <div class="comment-review_btn__32CMP"><a href="#" data-toggle-button="share" data-body-toggle="true" class="utility-meta_module__mfOnV" style="user-select: none;"><i class="la la-share-square-o"></i>Share </a>
                <div data-toggle-content="share" class="comment-review_shareContent__UGmyE">
                    <ul data-postid="6210" class="wilcity-social-sharing-wrapper list_module__1eis9 list-none list_social__31Q0V list_medium__1aT2c list_abs__OP7Og arrow--top-right">
                        <li class="list_item__3YghP"><a target="_blank" href="//facebook.com/sharer.php?u=https%3A%2F%2Fwilcity.com%2Freview%2Fdeneme%2F&amp;t=Deneme" class="wilcity-social-sharing list_link__2rDA1 text-ellipsis color-primary--hover"><span
                                    class="list_icon__2YpTp"><i class="fa fa-facebook"></i></span><span class="list_text__35R07">Facebook</span></a></li>
                        <li class="list_item__3YghP"><a target="_blank" href="//twitter.com/intent/tweet?text=Deneme-https%3A%2F%2Fwilcity.com%2Freview%2Fdeneme%2F&amp;source=webclient" class="wilcity-social-sharing list_link__2rDA1 text-ellipsis color-primary--hover"><span
                                    class="list_icon__2YpTp"><i class="fa fa-twitter"></i></span><span class="list_text__35R07">Twitter</span></a></li>
                        <li class="list_item__3YghP"><a target="_blank" href="//plus.google.com/share?url=https%3A%2F%2Fwilcity.com%2Freview%2Fdeneme%2F&amp;title=Deneme&amp;source=webclient" class="wilcity-social-sharing list_link__2rDA1 text-ellipsis color-primary--hover"><span
                                    class="list_icon__2YpTp"><i class="fa fa-google-plus"></i></span><span class="list_text__35R07">Google+</span></a></li>
                        <li class="list_item__3YghP"><a target="_blank" href="//www.tumblr.com/share/link?url=https%3A%2F%2Fwilcity.com%2Freview%2Fdeneme%2F&amp;name=Deneme&amp;source=webclient" class="wilcity-social-sharing list_link__2rDA1 text-ellipsis color-primary--hover"><span
                                    class="list_icon__2YpTp"><i class="fa fa-tumblr"></i></span><span class="list_text__35R07">Tumblr</span></a></li>
                        <li class="list_item__3YghP"><a target="_blank" href="//vk.com/share.php?url=https%3A%2F%2Fwilcity.com%2Freview%2Fdeneme%2F&amp;description=Deneme&amp;source=webclient" class="wilcity-social-sharing list_link__2rDA1 text-ellipsis color-primary--hover"><span
                                    class="list_icon__2YpTp"><i class="fa fa-vk"></i></span><span class="list_text__35R07">Vk</span></a></li>
                        <li class="list_item__3YghP"><a target="_blank" href="//pinterest.com/pin/create/button/?url=https%3A%2F%2Fwilcity.com%2Freview%2Fdeneme%2F&amp;description=Deneme&amp;source=webclient" class="wilcity-social-sharing list_link__2rDA1 text-ellipsis color-primary--hover"><span
                                    class="list_icon__2YpTp"><i class="fa fa-pinterest"></i></span><span class="list_text__35R07">Pinterest</span></a></li>
                        <li class="list_item__3YghP"><a target="_blank" href="//www.stumbleupon.com/submit?url=https%3A%2F%2Fwilcity.com%2Freview%2Fdeneme%2F&amp;title=Deneme&amp;source=webclient" class="wilcity-social-sharing list_link__2rDA1 text-ellipsis color-primary--hover"><span
                                    class="list_icon__2YpTp"><i class="fa fa-stumbleupon"></i></span><span class="list_text__35R07">Stumbleupon</span></a></li>
                        <li class="list_item__3YghP"><a target="_blank" href="//www.linkedin.com/shareArticle?mini=true&amp;url=https%3A%2F%2Fwilcity.com%2Freview%2Fdeneme%2F&amp;title=Deneme&amp;source=webclient" class="wilcity-social-sharing list_link__2rDA1 text-ellipsis color-primary--hover"><span
                                    class="list_icon__2YpTp"><i class="fa fa-linkedin"></i></span><span class="list_text__35R07">Linkedin</span></a></li>
                        <li class="list_item__3YghP"><a href="mailto:?Subject=Deneme&amp;Body=https://wilcity.com/review/deneme/" class="wilcity-social-sharing list_link__2rDA1 text-ellipsis color-primary--hover"><span class="list_icon__2YpTp"><i
                                        class="fa fa-envelope"></i></span><span class="list_text__35R07">Email</span></a></li>
                        <li class="list_item__3YghP"><a href="#" data-shortlink="https://wilcity.com/review/deneme/" data-desc="Press Ctrl+C to copy this link" class="wilcity-copy-link wilcity-social-sharing list_link__2rDA1 text-ellipsis color-primary--hover"><span
                                    class="list_icon__2YpTp"><i class="fa fa-link"></i></span><span class="list_text__35R07">Copy link</span></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div>
            <ul class="comment-review_commentlist__1LH_D list-none" style="display: none;"></ul>
            <!---->
            <div class="comment-review_form__20wWm">
                <div class="utility-box-1_module__MYXpX utility-box-1_xs__3Nipt d-inline-block mr-10 wil-float-left">
                    <div class="utility-box-1_avatar__DB9c_ rounded-circle" style="background-image: url(&quot;https://wilcity.com/wp-content/uploads/2018/08/avatar_default.jpg&quot;);"><img src="https://wilcity.com/wp-content/uploads/2018/08/avatar_default.jpg"
                            alt=""></div>
                </div>
                <div class="comment-review_comment__dJNqv">
                    <div class="field_module__1H6kT field_style4__2DBqx field-autoHeight-2">
                        <div class="field_wrap__Gv92k"><textarea data-height-default="22" class="wilcity-write-a-new-comment-box field_field__3U_Rt"></textarea><span class="field_label__2eCP7 text-ellipsis">Type a message...</span><span
                                class="bg-color-primary"></span>
                            <div class="field_rightButton__1GGWz js-field-rightButton"><span class="field_iconButton__2p3sr bg-color-primary"><i class="la la-arrow-up"></i></span></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>
<div class="comment-review_module__-Z5tr wilcity-js-review-item-5709">
    <div class="comment-review_header__1si3M">
        <div class="utility-box-1_module__MYXpX utility-box-1_boxLeft__3iS6b clearfix utility-box-1_sm__mopok  review-author-avatar">
            <div class="utility-box-1_avatar__DB9c_ rounded-circle" style="background-image: url(&quot;https://wilcity.com/wp-content/uploads/2018/08/avatar_default.jpg&quot;);"><img src="https://wilcity.com/wp-content/uploads/2018/08/avatar_default.jpg"
                    alt=""></div>
            <div class="utility-box-1_body__8qd9j">
                <div class="utility-box-1_group__2ZPA2">
                    <h3 class="utility-box-1_title__1I925">Felipewolmer</h3>
                </div>
                <div class="utility-box-1_description__2VDJ6 wilcity-review-date-5709">Dec 15, 2018</div>
            </div>
        </div>
        <div class="comment-review_abs__9mb1G pos-a-center-right">
            <div class="rated-small_module__1vw2B rated-small_style-1__2lG7u ml-20">
                <div data-rated="1" class="rated-small_wrap__2Eetz wilcity-data-average-review-score-5709 ">
                    <div class="rated-small_overallRating__oFmKR wilcity-average-review-score-5709">1</div>
                    <div class="rated-small_ratingWrap__3lzhB">
                        <div class="rated-small_maxRating__2D9mI">10</div>
                        <div class="rated-small_ratingOverview__2kCI_ wilcity-review-quality-5709">Terrible</div>
                    </div>
                </div>
            </div>
            <div class="dropdown_module__J_Zpj ml-20">
                <div data-toggle-button="dropdown" data-body-toggle="true" class="dropdown_threeDots__3fa2o" style="user-select: none;"><span class="dropdown_dot__3I1Rn"></span><span class="dropdown_dot__3I1Rn"></span><span class="dropdown_dot__3I1Rn"></span></div>
                <div data-toggle-content="dropdown" class="dropdown_itemsWrap__2fuze">
                    <ul data-id="5709" class="list_module__1eis9 list-none list_small__3fRoS list_abs__OP7Og arrow--top-right wilcity-review-toolbar-wrapper">
                        <li class="list_item__3YghP"><a href="#" class="list_link__2rDA1 text-ellipsis color-primary--hover"><span class="list_icon__2YpTp"><i class="la la-flag-o"></i></span><span class="list_text__35R07">Report
                                    review</span></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="comment-review_body__qhUqq">
        <div class="comment-review_content__1jFfZ">
            <h3 class="comment-review_title__2WbAh wilcity-review-title-5709">fgfg</h3>
            <div class="wilcity-review-content wilcity-review-content wilcity-review-content-5709 no-text-show loaded">
                <p>fgfgfg</p>
            </div> <a href="#" class="wilcity-expand-text wilcity-show-more color-primary">Show more</a> <a href="#" class="wilcity-expand-text wilcity-show-less color-primary">Show less</a>
        </div>
        <div id="wilcity-review-gallery-5709" class="wilcity-magnific-wrapper"></div>
        <div class="comment-review_meta__1chzm"><span data-count-liked="5" class="wilcity-count-liked-5709">5 Liked</span> <span class="wilcity-count-shared-5709">1 Shared</span></div>
    </div>
    <footer class="comment-review_footer__3XR0_">
        <div class="comment-review_btnGroup__1PqPh">
            <div class="comment-review_btn__32CMP"><a href="#" data-id="5709" class="utility-meta_module__mfOnV wilcity-i-like-it "><i class="la la-thumbs-up"></i>Like</a></div>
            <div class="comment-review_btn__32CMP"><a data-id="5709" href="#" class="utility-meta_module__mfOnV wilcity-add-new-discussion"><i class="la la-comments-o"></i>Comment</a></div>
            <div class="comment-review_btn__32CMP"><a href="#" data-toggle-button="share" data-body-toggle="true" class="utility-meta_module__mfOnV" style="user-select: none;"><i class="la la-share-square-o"></i>Share </a>
                <div data-toggle-content="share" class="comment-review_shareContent__UGmyE">
                    <ul data-postid="5709" class="wilcity-social-sharing-wrapper list_module__1eis9 list-none list_social__31Q0V list_medium__1aT2c list_abs__OP7Og arrow--top-right">
                        <li class="list_item__3YghP"><a target="_blank" href="//facebook.com/sharer.php?u=https%3A%2F%2Fwilcity.com%2Freview%2Ffgfg%2F&amp;t=fgfg" class="wilcity-social-sharing list_link__2rDA1 text-ellipsis color-primary--hover"><span
                                    class="list_icon__2YpTp"><i class="fa fa-facebook"></i></span><span class="list_text__35R07">Facebook</span></a></li>
                        <li class="list_item__3YghP"><a target="_blank" href="//twitter.com/intent/tweet?text=fgfg-https%3A%2F%2Fwilcity.com%2Freview%2Ffgfg%2F&amp;source=webclient" class="wilcity-social-sharing list_link__2rDA1 text-ellipsis color-primary--hover"><span
                                    class="list_icon__2YpTp"><i class="fa fa-twitter"></i></span><span class="list_text__35R07">Twitter</span></a></li>
                        <li class="list_item__3YghP"><a target="_blank" href="//plus.google.com/share?url=https%3A%2F%2Fwilcity.com%2Freview%2Ffgfg%2F&amp;title=fgfg&amp;source=webclient" class="wilcity-social-sharing list_link__2rDA1 text-ellipsis color-primary--hover"><span
                                    class="list_icon__2YpTp"><i class="fa fa-google-plus"></i></span><span class="list_text__35R07">Google+</span></a></li>
                        <li class="list_item__3YghP"><a target="_blank" href="//www.tumblr.com/share/link?url=https%3A%2F%2Fwilcity.com%2Freview%2Ffgfg%2F&amp;name=fgfg&amp;source=webclient" class="wilcity-social-sharing list_link__2rDA1 text-ellipsis color-primary--hover"><span
                                    class="list_icon__2YpTp"><i class="fa fa-tumblr"></i></span><span class="list_text__35R07">Tumblr</span></a></li>
                        <li class="list_item__3YghP"><a target="_blank" href="//vk.com/share.php?url=https%3A%2F%2Fwilcity.com%2Freview%2Ffgfg%2F&amp;description=fgfg&amp;source=webclient" class="wilcity-social-sharing list_link__2rDA1 text-ellipsis color-primary--hover"><span
                                    class="list_icon__2YpTp"><i class="fa fa-vk"></i></span><span class="list_text__35R07">Vk</span></a></li>
                        <li class="list_item__3YghP"><a target="_blank" href="//pinterest.com/pin/create/button/?url=https%3A%2F%2Fwilcity.com%2Freview%2Ffgfg%2F&amp;description=fgfg&amp;source=webclient" class="wilcity-social-sharing list_link__2rDA1 text-ellipsis color-primary--hover"><span
                                    class="list_icon__2YpTp"><i class="fa fa-pinterest"></i></span><span class="list_text__35R07">Pinterest</span></a></li>
                        <li class="list_item__3YghP"><a target="_blank" href="//www.stumbleupon.com/submit?url=https%3A%2F%2Fwilcity.com%2Freview%2Ffgfg%2F&amp;title=fgfg&amp;source=webclient" class="wilcity-social-sharing list_link__2rDA1 text-ellipsis color-primary--hover"><span
                                    class="list_icon__2YpTp"><i class="fa fa-stumbleupon"></i></span><span class="list_text__35R07">Stumbleupon</span></a></li>
                        <li class="list_item__3YghP"><a target="_blank" href="//www.linkedin.com/shareArticle?mini=true&amp;url=https%3A%2F%2Fwilcity.com%2Freview%2Ffgfg%2F&amp;title=fgfg&amp;source=webclient" class="wilcity-social-sharing list_link__2rDA1 text-ellipsis color-primary--hover"><span
                                    class="list_icon__2YpTp"><i class="fa fa-linkedin"></i></span><span class="list_text__35R07">Linkedin</span></a></li>
                        <li class="list_item__3YghP"><a href="mailto:?Subject=fgfg&amp;Body=https://wilcity.com/review/fgfg/" class="wilcity-social-sharing list_link__2rDA1 text-ellipsis color-primary--hover"><span class="list_icon__2YpTp"><i
                                        class="fa fa-envelope"></i></span><span class="list_text__35R07">Email</span></a></li>
                        <li class="list_item__3YghP"><a href="#" data-shortlink="https://wilcity.com/review/fgfg/" data-desc="Press Ctrl+C to copy this link" class="wilcity-copy-link wilcity-social-sharing list_link__2rDA1 text-ellipsis color-primary--hover"><span
                                    class="list_icon__2YpTp"><i class="fa fa-link"></i></span><span class="list_text__35R07">Copy link</span></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div>
            <ul class="comment-review_commentlist__1LH_D list-none" style="display: none;"></ul>
            <!---->
            <div class="comment-review_form__20wWm">
                <div class="utility-box-1_module__MYXpX utility-box-1_xs__3Nipt d-inline-block mr-10 wil-float-left">
                    <div class="utility-box-1_avatar__DB9c_ rounded-circle" style="background-image: url(&quot;https://wilcity.com/wp-content/uploads/2018/08/avatar_default.jpg&quot;);"><img src="https://wilcity.com/wp-content/uploads/2018/08/avatar_default.jpg"
                            alt=""></div>
                </div>
                <div class="comment-review_comment__dJNqv">
                    <div class="field_module__1H6kT field_style4__2DBqx field-autoHeight-2">
                        <div class="field_wrap__Gv92k"><textarea data-height-default="22" class="wilcity-write-a-new-comment-box field_field__3U_Rt"></textarea><span class="field_label__2eCP7 text-ellipsis">Type a message...</span><span
                                class="bg-color-primary"></span>
                            <div class="field_rightButton__1GGWz js-field-rightButton"><span class="field_iconButton__2p3sr bg-color-primary"><i class="la la-arrow-up"></i></span></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div> --}}
