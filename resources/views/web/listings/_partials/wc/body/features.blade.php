<div class="content-box_module__333d9 wilcity-single-listing-tag-box">
    <header class="content-box_header__xPnGx clearfix">
        <div class="wil-float-left">
            <h4 class="content-box_title__1gBHS"><i class="la la-list-alt"></i><span>Listing Features</span></h4>
        </div>
    </header>
    <div class="content-box_body__3tSRB">
        <div class="row">

            <div class="col-sm-4">
                <div class="icon-box-1_module__uyg5F three-text-ellipsis mt-20 mt-sm-15">
                    <div class="icon-box-1_block1__bJ25J"><a href="">
                            <div class="icon-box-1_icon__3V5c0 rounded-circle"><i class="la la-dot-circle-o"></i></div>
                            <div class="icon-box-1_text__3R39g">Banquet Room</div>
                        </a></div>
                </div>
            </div>

        </div>
    </div>
</div>
