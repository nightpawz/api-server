<div class="content-box_module__333d9">
    <header class="content-box_header__xPnGx clearfix">
        <div class="wil-float-left">
            <h4 class="content-box_title__1gBHS"><i class="la la-image"></i><span>Photos</span></h4>
        </div>
    </header>
    <div class="content-box_body__3tSRB">
        <div class="gallery_module__2AbLA ">
            <div data-col-xs-gap="5" data-col-sm-gap="10" class="row">
                <div class="col-xs-6 col-sm-3">
                    <div class="gallery-item_module__1wn6T">
                        <a href="{{ asset('img/default-listing-photo.jpg') }}">
                            <div class="imageCover_module__1VM4k">
                                <div class="imageCover_img__3pxw7" style="background-image: url(&quot;{{ asset('img/default-listing-photo.jpg') }}&quot;);"></div>
                            </div>
                            <!---->
                        </a>
                    </div>
                </div>
                {{-- <div class="col-xs-6 col-sm-3">
                    <div class="gallery-item_module__1wn6T"><a href="https://i0.wp.com/wilcity.com/wp-content/uploads/2018/05/Tokyo_2_c.jpg?fit=1200%2C795&amp;ssl=1">
                            <div class="imageCover_module__1VM4k">
                                <div class="imageCover_img__3pxw7" style="background-image: url(&quot;https://i0.wp.com/wilcity.com/wp-content/uploads/2018/05/Tokyo_2_c.jpg?resize=150%2C150&amp;ssl=1&quot;);"></div>
                            </div>
                            <!---->
                        </a></div>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <div class="gallery-item_module__1wn6T"><a href="https://i2.wp.com/wilcity.com/wp-content/uploads/2018/05/Tokyo_2_d.jpg?fit=1200%2C781&amp;ssl=1">
                            <div class="imageCover_module__1VM4k">
                                <div class="imageCover_img__3pxw7" style="background-image: url(&quot;https://i2.wp.com/wilcity.com/wp-content/uploads/2018/05/Tokyo_2_d.jpg?resize=150%2C150&amp;ssl=1&quot;);"></div>
                            </div>
                            <!---->
                        </a></div>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <div class="gallery-item_module__1wn6T"><a href="https://i0.wp.com/wilcity.com/wp-content/uploads/2018/05/Singapore_1_a.jpg?fit=1200%2C779&amp;ssl=1">
                            <div class="imageCover_module__1VM4k">
                                <div class="imageCover_img__3pxw7" style="background-image: url(&quot;https://i0.wp.com/wilcity.com/wp-content/uploads/2018/05/Singapore_1_a.jpg?resize=150%2C150&amp;ssl=1&quot;);"></div>
                            </div>
                            <!---->
                        </a></div>
                </div> --}}
            </div>
        </div>
    </div>
    <footer class="content-box_footer__kswf3"><a href="https://wilcity.com/listing/chontaduro-barcelona/?tab=photos" class="wil-text-center list_link__2rDA1 text-ellipsis color-primary--hover"> See All </a></footer>
</div>
