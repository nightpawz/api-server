<div class="content-box_module__333d9 wilcity-single-listing-content-box">
    <header class="content-box_header__xPnGx clearfix">
        <div class="wil-float-left">
            <h4 class="content-box_title__1gBHS"><i class="la la-file-text"></i><span>Description</span></h4>
        </div>
    </header>
    <div class="content-box_body__3tSRB">
        <div>
            <div class="listing-content">{!! $listing->description !!}</div>

        </div>
    </div>
</div>
