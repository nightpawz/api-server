<div class="wilcity-sidebar-item-business-info content-box_module__333d9">
    <header class="content-box_header__xPnGx clearfix">
        <div class="wil-float-left">
            <h4 class="content-box_title__1gBHS"><i class="la la-map-signs"></i><span>Business Info</span></h4>
        </div>
    </header>
    <div class="content-box_body__3tSRB">
        <div class="icon-box-1_module__uyg5F one-text-ellipsis mt-20 mt-sm-15 text-pre">
            <div class="icon-box-1_block1__bJ25J">
                <a target="_blank" href="https://www.google.com/maps/search/Carrer%20dAribau,%20254,%2008006%20Barcelona,%20Spain">
                    <div class="icon-box-1_icon__3V5c0 rounded-circle"><i class="la la-map-marker"></i></div>
                    <div class="icon-box-1_text__3R39g">{{  $listing->address ?? '' }}</div>
                </a>
            </div>
        </div>
        <div class="icon-box-1_module__uyg5F one-text-ellipsis mt-20 mt-sm-15">
            <div class="icon-box-1_block1__bJ25J">
                <a href="mailto:{{ $listing->email }}">
                    <div class="icon-box-1_icon__3V5c0 rounded-circle">
                        <i class="la la-envelope"></i>
                    </div>
                    <div class="icon-box-1_text__3R39g">
                        {{ $listing->email }}
                    </div>
                </a>
            </div>
        </div>
        <div class="icon-box-1_module__uyg5F one-text-ellipsis mt-20 mt-sm-15">
            <div class="icon-box-1_block1__bJ25J">
                <a href="tel:{{ $listing->phone }}">
                    <div class="icon-box-1_icon__3V5c0 rounded-circle"><i class="la la-phone"></i></div>
                    <div class="icon-box-1_text__3R39g">{{ $listing->phone }}</div>
                </a>
            </div>
        </div>
        <div class="icon-box-1_module__uyg5F one-text-ellipsis mt-20 mt-sm-15">
            <div class="icon-box-1_block1__bJ25J">
                <a target="_blank" href="{{ $listing->site_url }}">
                    <div class="icon-box-1_icon__3V5c0 rounded-circle"><i class="la la-globe"></i></div>
                    <div class="icon-box-1_text__3R39g">{{ $listing->site_url }}</div>
                </a>
            </div>
        </div>
        <div class="icon-box-1_module__uyg5F mt-20 mt-sm-15">
            <div class="social-icon_module__HOrwr social-icon_style-2__17BFy">
                <a href="#" target="_blank" class="social-icon_item__3SLnb">
                    <i class="fa fa-facebook"></i>
                </a>
                <a href="#" target="_blank" class="social-icon_item__3SLnb">
                    <i class="fa fa-twitter"></i>
                </a>
                <a href="#" target="_blank" class="social-icon_item__3SLnb">
                    <i class="fa fa-google-plus"></i>
                </a>
                <a href="#" target="_blank" class="social-icon_item__3SLnb">
                    <i class="fa fa-tumblr"></i>
                </a>
            </div>
        </div>
        <a href="#" class="wilcity-inbox-btn wil-btn wil-btn--block mt-20 wil-btn--border wil-btn--round">
            <i class="la la-envelope color-1"></i>Inbox
        </a>
    </div>
</div>
