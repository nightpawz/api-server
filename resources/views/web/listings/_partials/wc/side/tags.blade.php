<div class="wilcity-sidebar-item-term-box wilcity-sidebar-item-listing_tag content-box_module__333d9">
    <header class="content-box_header__xPnGx clearfix">
        <div class="wil-float-left">
            <h4 class="content-box_title__1gBHS"><i class="la la-tags"></i><span>Tags</span></h4>
        </div>
    </header>
    <div class="content-box_body__3tSRB">
        <div class="row">
            <div class="col-sm-6 col-sm-6-clear">
                <div class="icon-box-1_module__uyg5F two-text-ellipsis mt-20 mt-sm-15">
                    <div class="icon-box-1_block1__bJ25J"><a href="https://wilcity.com/listing-tag/banquet-room/">
                            <div class="icon-box-1_icon__3V5c0 rounded-circle"><i class="la la-users"></i></div>
                            <div class="icon-box-1_text__3R39g">Banquet Room</div>
                        </a></div>
                </div>
            </div>
            <div class="col-sm-6 col-sm-6-clear">
                <div class="icon-box-1_module__uyg5F two-text-ellipsis mt-20 mt-sm-15">
                    <div class="icon-box-1_block1__bJ25J"><a href="https://wilcity.com/listing-tag/bar-lounge/">
                            <div class="icon-box-1_icon__3V5c0 rounded-circle"><i class="la la-glass"></i></div>
                            <div class="icon-box-1_text__3R39g">Bar/Lounge</div>
                        </a></div>
                </div>
            </div>
            <div class="col-sm-6 col-sm-6-clear">
                <div class="icon-box-1_module__uyg5F two-text-ellipsis mt-20 mt-sm-15">
                    <div class="icon-box-1_block1__bJ25J"><a href="https://wilcity.com/listing-tag/breakfast-available/">
                            <div class="icon-box-1_icon__3V5c0 rounded-circle"><i class="la la-moon-o"></i></div>
                            <div class="icon-box-1_text__3R39g">Breakfast Available</div>
                        </a></div>
                </div>
            </div>
            <div class="col-sm-6 col-sm-6-clear">
                <div class="icon-box-1_module__uyg5F two-text-ellipsis mt-20 mt-sm-15">
                    <div class="icon-box-1_block1__bJ25J"><a href="https://wilcity.com/listing-tag/concierge/">
                            <div class="icon-box-1_icon__3V5c0 rounded-circle"><i class="la la-trophy"></i></div>
                            <div class="icon-box-1_text__3R39g">Concierge</div>
                        </a></div>
                </div>
            </div>
            <div class="col-sm-6 col-sm-6-clear">
                <div class="icon-box-1_module__uyg5F two-text-ellipsis mt-20 mt-sm-15">
                    <div class="icon-box-1_block1__bJ25J"><a href="https://wilcity.com/listing-tag/expert/">
                            <div class="icon-box-1_icon__3V5c0 rounded-circle"><i class="la la-life-buoy"></i></div>
                            <div class="icon-box-1_text__3R39g">Expert</div>
                        </a></div>
                </div>
            </div>
            <div class="col-sm-6 col-sm-6-clear">
                <div class="icon-box-1_module__uyg5F two-text-ellipsis mt-20 mt-sm-15">
                    <div class="icon-box-1_block1__bJ25J"><a href="https://wilcity.com/listing-tag/food-drink/">
                            <div class="icon-box-1_icon__3V5c0 rounded-circle"><i class="la la-coffee"></i></div>
                            <div class="icon-box-1_text__3R39g">Food &amp; Drink</div>
                        </a></div>
                </div>
            </div>
            <div class="col-sm-6 col-sm-6-clear">
                <div class="icon-box-1_module__uyg5F two-text-ellipsis mt-20 mt-sm-15">
                    <div class="icon-box-1_block1__bJ25J"><a href="https://wilcity.com/listing-tag/foot-massage/">
                            <div class="icon-box-1_icon__3V5c0 rounded-circle"><i class="la la-tree"></i></div>
                            <div class="icon-box-1_text__3R39g">Foot Massage</div>
                        </a></div>
                </div>
            </div>
            <div class="col-sm-6 col-sm-6-clear">
                <div class="icon-box-1_module__uyg5F two-text-ellipsis mt-20 mt-sm-15">
                    <div class="icon-box-1_block1__bJ25J"><a href="https://wilcity.com/listing-tag/full-bar/">
                            <div class="icon-box-1_icon__3V5c0 rounded-circle"><i class="la la-headphones"></i></div>
                            <div class="icon-box-1_text__3R39g">Full Bar</div>
                        </a></div>
                </div>
            </div>
            <div class="col-sm-6 col-sm-6-clear">
                <div class="icon-box-1_module__uyg5F two-text-ellipsis mt-20 mt-sm-15">
                    <div class="icon-box-1_block1__bJ25J"><a href="https://wilcity.com/listing-tag/professional-massage/">
                            <div class="icon-box-1_icon__3V5c0 rounded-circle"><i class="la la-hand-scissors-o"></i></div>
                            <div class="icon-box-1_text__3R39g">Professional Massage</div>
                        </a></div>
                </div>
            </div>
            <div class="col-sm-6 col-sm-6-clear">
                <div class="icon-box-1_module__uyg5F two-text-ellipsis mt-20 mt-sm-15">
                    <div class="icon-box-1_block1__bJ25J"><a href="https://wilcity.com/listing-tag/real-massage/">
                            <div class="icon-box-1_icon__3V5c0 rounded-circle"><i class="la la-group"></i></div>
                            <div class="icon-box-1_text__3R39g">Real Massage</div>
                        </a></div>
                </div>
            </div>
            <div class="col-sm-6 col-sm-6-clear">
                <div class="icon-box-1_module__uyg5F two-text-ellipsis mt-20 mt-sm-15">
                    <div class="icon-box-1_block1__bJ25J"><a href="https://wilcity.com/listing-tag/relaxing-hour/">
                            <div class="icon-box-1_icon__3V5c0 rounded-circle"><i class="la la-sun-o"></i></div>
                            <div class="icon-box-1_text__3R39g">Relaxing Hour</div>
                        </a></div>
                </div>
            </div>
            <div class="col-sm-6 col-sm-6-clear">
                <div class="icon-box-1_module__uyg5F two-text-ellipsis mt-20 mt-sm-15">
                    <div class="icon-box-1_block1__bJ25J"><a href="https://wilcity.com/listing-tag/reservations/">
                            <div class="icon-box-1_icon__3V5c0 rounded-circle"><i class="la la-legal"></i></div>
                            <div class="icon-box-1_text__3R39g">Reservations</div>
                        </a></div>
                </div>
            </div>
            <div class="col-sm-6 col-sm-6-clear">
                <div class="icon-box-1_module__uyg5F two-text-ellipsis mt-20 mt-sm-15">
                    <div class="icon-box-1_block1__bJ25J"><a href="https://wilcity.com/listing-tag/restaurant/">
                            <div class="icon-box-1_icon__3V5c0 rounded-circle"><i class="la la-cutlery"></i></div>
                            <div class="icon-box-1_text__3R39g">Restaurant</div>
                        </a></div>
                </div>
            </div>
            <div class="col-sm-6 col-sm-6-clear">
                <div class="icon-box-1_module__uyg5F two-text-ellipsis mt-20 mt-sm-15">
                    <div class="icon-box-1_block1__bJ25J"><a href="https://wilcity.com/listing-tag/room-service/">
                            <div class="icon-box-1_icon__3V5c0 rounded-circle"><i class="la la-bed"></i></div>
                            <div class="icon-box-1_text__3R39g">Room Service</div>
                        </a></div>
                </div>
            </div>
            <div class="col-sm-6 col-sm-6-clear">
                <div class="icon-box-1_module__uyg5F two-text-ellipsis mt-20 mt-sm-15">
                    <div class="icon-box-1_block1__bJ25J"><a href="https://wilcity.com/listing-tag/seating/">
                            <div class="icon-box-1_icon__3V5c0 rounded-circle"><i class="la la-meh-o"></i></div>
                            <div class="icon-box-1_text__3R39g">Seating</div>
                        </a></div>
                </div>
            </div>
            <div class="col-sm-6 col-sm-6-clear">
                <div class="icon-box-1_module__uyg5F two-text-ellipsis mt-20 mt-sm-15">
                    <div class="icon-box-1_block1__bJ25J"><a href="https://wilcity.com/listing-tag/serves-alcohol/">
                            <div class="icon-box-1_icon__3V5c0 rounded-circle"><i class="la la-beer"></i></div>
                            <div class="icon-box-1_text__3R39g">Serves Alcohol</div>
                        </a></div>
                </div>
            </div>
            <div class="col-sm-6 col-sm-6-clear">
                <div class="icon-box-1_module__uyg5F two-text-ellipsis mt-20 mt-sm-15">
                    <div class="icon-box-1_block1__bJ25J"><a href="https://wilcity.com/listing-tag/shopping/">
                            <div class="icon-box-1_icon__3V5c0 rounded-circle"><i class="la la-shopping-cart"></i></div>
                            <div class="icon-box-1_text__3R39g">Shopping</div>
                        </a></div>
                </div>
            </div>
            <div class="col-sm-6 col-sm-6-clear">
                <div class="icon-box-1_module__uyg5F two-text-ellipsis mt-20 mt-sm-15">
                    <div class="icon-box-1_block1__bJ25J"><a href="https://wilcity.com/listing-tag/skilled/">
                            <div class="icon-box-1_icon__3V5c0 rounded-circle"><i class="la la-hand-peace-o"></i></div>
                            <div class="icon-box-1_text__3R39g">Skilled</div>
                        </a></div>
                </div>
            </div>
            <div class="col-sm-6 col-sm-6-clear">
                <div class="icon-box-1_module__uyg5F two-text-ellipsis mt-20 mt-sm-15">
                    <div class="icon-box-1_block1__bJ25J"><a href="https://wilcity.com/listing-tag/spa/">
                            <div class="icon-box-1_icon__3V5c0 rounded-circle"><i class="la la-female"></i></div>
                            <div class="icon-box-1_text__3R39g">Spa</div>
                        </a></div>
                </div>
            </div>
            <div class="col-sm-6 col-sm-6-clear">
                <div class="icon-box-1_module__uyg5F two-text-ellipsis mt-20 mt-sm-15">
                    <div class="icon-box-1_block1__bJ25J"><a href="https://wilcity.com/listing-tag/state-street/">
                            <div class="icon-box-1_icon__3V5c0 rounded-circle"><i class="la la-line-chart"></i></div>
                            <div class="icon-box-1_text__3R39g">State Street</div>
                        </a></div>
                </div>
            </div>
            <div class="col-sm-6 col-sm-6-clear">
                <div class="icon-box-1_module__uyg5F two-text-ellipsis mt-20 mt-sm-15">
                    <div class="icon-box-1_block1__bJ25J"><a href="https://wilcity.com/listing-tag/tea-tasting/">
                            <div class="icon-box-1_icon__3V5c0 rounded-circle"><i class="la la-lemon-o"></i></div>
                            <div class="icon-box-1_text__3R39g">Tea Tasting</div>
                        </a></div>
                </div>
            </div>
            <div class="col-sm-6 col-sm-6-clear">
                <div class="icon-box-1_module__uyg5F two-text-ellipsis mt-20 mt-sm-15">
                    <div class="icon-box-1_block1__bJ25J"><a href="https://wilcity.com/listing-tag/valet-parking/">
                            <div class="icon-box-1_icon__3V5c0 rounded-circle"><i class="la la-car"></i></div>
                            <div class="icon-box-1_text__3R39g">Valet Parking</div>
                        </a></div>
                </div>
            </div>
            <div class="col-sm-6 col-sm-6-clear">
                <div class="icon-box-1_module__uyg5F two-text-ellipsis mt-20 mt-sm-15">
                    <div class="icon-box-1_block1__bJ25J"><a href="https://wilcity.com/listing-tag/waitstaff/">
                            <div class="icon-box-1_icon__3V5c0 rounded-circle"><i class="la la-street-view"></i></div>
                            <div class="icon-box-1_text__3R39g">Waitstaff</div>
                        </a></div>
                </div>
            </div>
            <div class="col-sm-6 col-sm-6-clear">
                <div class="icon-box-1_module__uyg5F two-text-ellipsis mt-20 mt-sm-15">
                    <div class="icon-box-1_block1__bJ25J"><a href="https://wilcity.com/listing-tag/wheelchair-accessible/">
                            <div class="icon-box-1_icon__3V5c0 rounded-circle"><i class="la la-wheelchair"></i></div>
                            <div class="icon-box-1_text__3R39g">Wheelchair Accessible</div>
                        </a></div>
                </div>
            </div>
            <div class="col-sm-6 col-sm-6-clear">
                <div class="icon-box-1_module__uyg5F two-text-ellipsis mt-20 mt-sm-15">
                    <div class="icon-box-1_block1__bJ25J"><a href="https://wilcity.com/listing-tag/wifi/">
                            <div class="icon-box-1_icon__3V5c0 rounded-circle"><i class="la la-wifi"></i></div>
                            <div class="icon-box-1_text__3R39g">Wifi</div>
                        </a></div>
                </div>
            </div>
        </div>
    </div>
</div>
