<div class="wilcity-sidebar-item-price-range content-box_module__333d9">
    <header class="content-box_header__xPnGx clearfix">
        <div class="wil-float-left">
            <h4 class="content-box_title__1gBHS"><i class="la la-envelope-square"></i><span>Price Range</span></h4>
        </div>
    </header>
    <div class="content-box_body__3tSRB wilcity-price-range-cheap">
        <div class="price-range_module__348HY clearfix">
            <div class="price-range_sign__3thLa"><i class="la la-dollar"></i></div>
            <div class="price-range_group__23kMb">
                <div class="price-range_sign2__3xNkO">$</div>
                <div class="price-range_range__19Fut color-primary">
                    <div class="price-range_from__3iV-6"><sup class="price-range_supSig__1pMDY right_space">$</sup>10</div>
                    <div class="price-range_arrow__3LnNe"><i class="la la-arrow-right"></i></div>
                    <div class="price-range_to__2Tf3W"><sup class="price-range_supSig__1pMDY right_space">$</sup>20</div>
                </div>
            </div>
        </div>
    </div>
</div>
