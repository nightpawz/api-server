<div class="wilcity-sidebar-item-term-box wilcity-sidebar-item-listing_cat content-box_module__333d9">
    <header class="content-box_header__xPnGx clearfix">
        <div class="wil-float-left">
            <h4 class="content-box_title__1gBHS"><i class="la la-sitemap"></i><span>Categories</span></h4>
        </div>
    </header>
    <div class="content-box_body__3tSRB">
        <div class="row">
            <div class="col-sm-6 col-sm-6-clear">
                <div class="icon-box-1_module__uyg5F two-text-ellipsis mt-20 mt-sm-15">
                    <div class="icon-box-1_block1__bJ25J">
                        <a href="#">
                            {{-- <div class="icon-box-1_icon__3V5c0 rounded-circle" style="background-color: rgb(0, 107, 247);">
                                <i class="la la-cutlery"></i>
                            </div> --}}
                            <div class="icon-box-1_text__3R39g">{{ $listing->category->name ?? '' }}</div>
                        </a></div>
                </div>
            </div>
        </div>
    </div>
</div>
