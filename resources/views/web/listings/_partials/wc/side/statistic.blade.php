<div class="wilcity-sidebar-item-statistic content-box_module__333d9">
    <header class="content-box_header__xPnGx clearfix">
        <div class="wil-float-left">
            <h4 class="content-box_title__1gBHS"><i class="la la-bar-chart"></i><span>Statistic</span></h4>
        </div>
    </header>
    <div class="content-box_body__3tSRB">
        <div class="row">
            <div class="col-sm-6 col-sm-6-clear">
                <div class="icon-box-1_module__uyg5F two-text-ellipsis mt-20 mt-sm-15">
                    <div class="icon-box-1_block1__bJ25J">
                        <div class="icon-box-1_icon__3V5c0 rounded-circle"><i class="la la-eye"></i></div>
                        <div class="icon-box-1_text__3R39g">6737 Views</div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-sm-6-clear">
                <div class="icon-box-1_module__uyg5F two-text-ellipsis mt-20 mt-sm-15">
                    <div class="icon-box-1_block1__bJ25J">
                        <div class="icon-box-1_icon__3V5c0 rounded-circle"><i class="la la-star-o"></i></div>
                        <div class="icon-box-1_text__3R39g">22 Ratings</div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-sm-6-clear">
                <div class="icon-box-1_module__uyg5F two-text-ellipsis mt-20 mt-sm-15">
                    <div class="icon-box-1_block1__bJ25J">
                        <div class="icon-box-1_icon__3V5c0 rounded-circle"><i class="la la-heart-o"></i></div>
                        <div class="icon-box-1_text__3R39g">60 Favorites</div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-sm-6-clear">
                <div class="icon-box-1_module__uyg5F two-text-ellipsis mt-20 mt-sm-15">
                    <div class="icon-box-1_block1__bJ25J">
                        <div class="icon-box-1_icon__3V5c0 rounded-circle"><i class="la la-share"></i></div>
                        <div class="icon-box-1_text__3R39g">30 Shares</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
