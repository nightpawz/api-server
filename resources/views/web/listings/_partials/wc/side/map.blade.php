<div class="content-box_module__333d9">
    <header class="content-box_header__xPnGx clearfix">
        <div class="wil-float-left">
            <h4 class="content-box_title__1gBHS"><i class="la la-map-marker"></i> <span>Map</span></h4>
        </div>
    </header>
    <div class="content-box_body__3tSRB">
        <div id="wilcity-sidebar-map" data-latlng="41.396572,2.149053" class="wilcity-single-map" style="position: relative; overflow: hidden;">
            <div style="height: 100%; width: 100%; position: absolute; top: 0px; left: 0px; background-color: rgb(229, 227, 223);">
                <div class="gm-err-container">
                    <div class="gm-err-content">
                        <div class="gm-err-icon"><img src="https://maps.gstatic.com/mapfiles/api-3/images/icon_error.png" draggable="false" style="user-select: none;"></div>
                        <div class="gm-err-title">Ups! Ada sesuatu yang salah.</div>
                        <div class="gm-err-message">Halaman ini tidak memuat Google Maps dengan benar. Lihat konsol JavaScript untuk mengetahui detail teknisnya.</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
