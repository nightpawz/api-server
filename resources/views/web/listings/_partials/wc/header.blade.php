<header class="listing-detail_header__18Cfs">
    <div class="listing-detail_img__3DyYX pos-a-full bg-cover"
        style="background-image: url(&quot;{{ $listing->banner_url !== '' ? $listing->banner_url : asset('img/default-listing-banner.jpg') }}&quot;);">
        <img src="{{ $listing->banner_url !== '' ? $listing->banner_url : asset('img/default-listing-banner.jpg') }}"
            alt="{{ $listing->name }}">
    </div>
    <div class="wil-overlay" style="background-color: rgba(37, 44, 65, 0.35);"></div>
</header>
