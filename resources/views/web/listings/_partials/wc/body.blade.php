<div class="listing-detail_body__287ZB" style="transform: none;">
    <div class="container" style="transform: none;">
        <div id="single-home" data-tab-key="home" class="single-tab-content wilcity-js-toggle-group">
            <div class="listing-detail_row__2UU6R clearfix">
                <div data-additional-class="wil-sidebarRight" class="wil-colLarge wil-sidebarRight">
                    <div class="mb-20">
                        <div data-col-xs-gap="20" class="row"></div>
                    </div>

                    @include('web.listings._partials.wc.body.rating')

                    @include('web.listings._partials.wc.body.sale')

                    @include('web.listings._partials.wc.body.description')

                    @include('web.listings._partials.wc.body.features')

                    @include('web.listings._partials.wc.body.photos')

                    @include('web.listings._partials.wc.body.videos')

                    @include('web.listings._partials.wc.body.events')

                    @include('web.listings._partials.wc.body.reviews')

                    @include('web.listings._partials.wc.body.products')

                </div>
                <div data-tab-key="home" class="wil-colSmall wilcity-js-toggle-group">

                    @include('web.listings._partials.wc.side.interest')

                    {{-- @include('web.listings._partials.wc.side.products') --}}

                    @include('web.listings._partials.wc.side.info')

                    {{-- @include('web.listings._partials.wc.side.price') --}}

                    @include('web.listings._partials.wc.side.category')

                    {{-- @include('web.listings._partials.wc.side.statistic') --}}

                    {{-- @include('web.listings._partials.wc.side.tags') --}}

                    {{-- @include('web.listings._partials.wc.side.map') --}}

                </div>
            </div>
        </div>
            <div id="single-content" class="single-tab-content hidden">
                <div class="listing-detail_row__2UU6R clearfix">
                    <div class="content-box_module__333d9 pos-r">
                        <div class="content-box_body__3tSRB">
                            <div></div>
                        </div>
                        <div class="full-load">
                            <div class="pill-loading_module__3LZ6v pos-a-center">
                                <div class="pill-loading_loader__3LOnT"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="single-tags" class="single-tab-content hidden">
                <div class="content-box_module__333d9 pos-r">
                    <div class="align-center mt-20" style="display: none;"><i class="la la-frown-o" style="font-size: 40px;"></i>
                        <p></p>
                    </div>
                    <div class="content-box_body__3tSRB">
                        <div data-col-xs-gap="10" class="row" style="display: none;"></div>
                    </div>
                    <div class="full-load" style="display: none;">
                        <div class="pill-loading_module__3LZ6v pos-a-center">
                            <div class="pill-loading_loader__3LOnT"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="single-photos" class="single-tab-content hidden">
                <div class="content-box_module__333d9 pos-r">
                    <header class="content-box_header__xPnGx clearfix">
                        <div class="wil-float-left">
                            <h4 class="content-box_title__1gBHS"><i class="la la-image"></i><span>Photos</span></h4>
                        </div>
                    </header>
                    <div class="align-center mt-20" style="display: none;"><i class="la la-frown-o" style="font-size: 40px;"></i>
                        <p></p>
                    </div>
                    <div class="content-box_body__3tSRB">
                        <div class="gallery_module__2AbLA ">
                            <div data-col-xs-gap="5" data-col-sm-gap="10" class="row"></div>
                        </div>
                    </div>
                    <div class="full-load" style="display: none;">
                        <div class="pill-loading_module__3LZ6v pos-a-center">
                            <div class="pill-loading_loader__3LOnT"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="single-videos" class="single-tab-content hidden">
                <div class="content-box_module__333d9 pos-r">
                    <header class="content-box_header__xPnGx clearfix">
                        <div class="wil-float-left">
                            <h4 class="content-box_title__1gBHS"><i class="la la-video-camera"></i><span>Videos</span></h4>
                        </div>
                    </header>
                    <div class="align-center mt-20" style="display: none;"><i class="la la-frown-o" style="font-size: 40px;"></i>
                        <p></p>
                    </div>
                    <div class="content-box_body__3tSRB">
                        <div class="gallery_module__2AbLA">
                            <div data-col-xs-gap="5" data-col-sm-gap="10" class="row"></div>
                        </div>
                    </div>
                    <div class="full-load" style="display: none;">
                        <div class="pill-loading_module__3LZ6v pos-a-center">
                            <div class="pill-loading_loader__3LOnT"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="single-events" class="single-tab-content hidden">
                <div class="content-box_module__333d9">
                    <header class="content-box_header__xPnGx clearfix">
                        <div class="wil-float-left">
                            <h4 class="content-box_title__1gBHS"><i class="la la-calendar-check-o"></i><span>Events</span></h4>
                        </div>
                    </header>
                    <div class="content-box_body__3tSRB pos-r">
                        <div data-col-xs-gap="10" class="row" style="display: none;"></div>
                        <div class="align-center mt-20" style="display: none;"><i class="la la-frown-o" style="font-size: 40px;"></i>
                            <p></p>
                        </div>
                        <div id="wilcity-infinite-events"></div>
                        <div class="full-load" style="display: none;">
                            <div class="pill-loading_module__3LZ6v pos-a-center">
                                <div class="pill-loading_loader__3LOnT"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="single-reviews" class="single-tab-content hidden" style="transform: none;">
                <div class="listing-detail_row__2UU6R clearfix" style="transform: none;">
                    <div class="wil-colLarge js-sticky pos-r" style="position: relative; overflow: visible; box-sizing: border-box; min-height: 1px;">
                        <!---->
                        <div class="theiaStickySidebar" style="padding-top: 0px; padding-bottom: 0px; position: static; transform: none;">
                            <div class="alert_module__Q4QZx alert_danger__2ajVf " style="display: none;">
                                <div class="alert_icon__1bDKL"><i class="la la-frown-o"></i></div>
                                <div class="alert_content__1ntU3"></div>
                                <!---->
                            </div>
                        </div>
                    </div>
                    <div class="wil-colSmall js-sticky" style="position: relative; overflow: visible; box-sizing: border-box; min-height: 1px;">
                        <div class="theiaStickySidebar" style="padding-top: 0px; padding-bottom: 0px; position: static; transform: none;">
                            <div class="content-box_module__333d9">
                                <header class="content-box_header__xPnGx clearfix">
                                    <div class="wil-float-left">
                                        <h4 class="content-box_title__1gBHS"><i class="la la-star-o"></i><span><span class="color-primary">20</span> Reviews For Chontaduro Barcelona</span></h4>
                                    </div>
                                </header>
                                <div class="content-box_body__3tSRB">
                                    <div class="rated-info-group_module__20kAf">
                                        <div class="rated-info-group_body__2yvB5">
                                            <div class="rated-info_module__KsMQP">
                                                <div class="rated-info_title__2Oido">Quality</div>
                                                <div class="rated-info_wrap__AI5nf">
                                                    <div class="rated-info_progressBar__1pCWE">
                                                        <div class="rated-info_bar__1T7U7" style="left: 74%;"></div>
                                                    </div>
                                                </div>
                                                <div class="rated-info_overallRating__1Js4A">7.4</div>
                                            </div>
                                            <div class="rated-info_module__KsMQP">
                                                <div class="rated-info_title__2Oido">Location</div>
                                                <div class="rated-info_wrap__AI5nf">
                                                    <div class="rated-info_progressBar__1pCWE">
                                                        <div class="rated-info_bar__1T7U7" style="left: 70%;"></div>
                                                    </div>
                                                </div>
                                                <div class="rated-info_overallRating__1Js4A">7.0</div>
                                            </div>
                                            <div class="rated-info_module__KsMQP">
                                                <div class="rated-info_title__2Oido">Space</div>
                                                <div class="rated-info_wrap__AI5nf">
                                                    <div class="rated-info_progressBar__1pCWE">
                                                        <div class="rated-info_bar__1T7U7" style="left: 71%;"></div>
                                                    </div>
                                                </div>
                                                <div class="rated-info_overallRating__1Js4A">7.1</div>
                                            </div>
                                            <div class="rated-info_module__KsMQP">
                                                <div class="rated-info_title__2Oido">Service</div>
                                                <div class="rated-info_wrap__AI5nf">
                                                    <div class="rated-info_progressBar__1pCWE">
                                                        <div class="rated-info_bar__1T7U7" style="left: 69%;"></div>
                                                    </div>
                                                </div>
                                                <div class="rated-info_overallRating__1Js4A">6.9</div>
                                            </div>
                                            <div class="rated-info_module__KsMQP">
                                                <div class="rated-info_title__2Oido">Price</div>
                                                <div class="rated-info_wrap__AI5nf">
                                                    <div class="rated-info_progressBar__1pCWE">
                                                        <div class="rated-info_bar__1T7U7" style="left: 69%;"></div>
                                                    </div>
                                                </div>
                                                <div class="rated-info_overallRating__1Js4A">6.9</div>
                                            </div>
                                        </div>
                                        <div class="rated-info-group_footer__2mcef">
                                            <h3>Average Rating</h3>
                                            <div class="rated-small_module__1vw2B rated-info-group_rated__29vEF pos-a-center-right">
                                                <div data-rated="7.1" class="rated-small_wrap__2Eetz">
                                                    <div class="rated-small_overallRating__oFmKR">7.1</div>
                                                    <div class="rated-small_ratingWrap__3lzhB">
                                                        <div class="rated-small_maxRating__2D9mI">10</div>
                                                        <div class="rated-small_ratingOverview__2kCI_">Poor</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><a href="#" class="wil-btn wil-btn--primary wil-btn--round wil-btn--md wil-btn--block"><i class="la la-star-o"></i> Add a review </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
