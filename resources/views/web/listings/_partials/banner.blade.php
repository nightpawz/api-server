<header class="fe-listing-banner">
    <div class="img-cover-container" style="background-image: url(&quot;{{ $listing->banner_url }}&quot;);">
        {{-- <img
            src="https://i1.wp.com/wilcity.com/wp-content/uploads/2018/05/Barcelona_5_c.jpg?fit=1024%2C684&amp;ssl=1"
            alt="{{ $listing->name }}"
        width="1024" height="684"
        src-orig="https://i1.wp.com/wilcity.com/wp-content/uploads/2018/05/Barcelona_5_c.jpg?fit=1024%2C684&amp;ssl=1"
        scale="1"> --}}
    </div>
    <div class="fe-overlay" style="background-color: rgba(37, 44, 65, 0.7);"></div>
</header>

<div class="fe-listing-info">
    <div class="container">
        <div class="listing-info-left">
            <div class="listing-info-box">
                <div class="ls-logo" style="background-image: url(&quot;{{ $listing->logo_url }}&quot;);"><a href="{{ route('listings.show', $listing->slug) }}">
                    <img src="{{ $listing->logo_url }}"
                        alt="{{ $listing->name }}" class="hidden" scale="0"></a>
                    </div>
            </div>
            <div class="listing-info-title">
                <h1 class="listing-detail_title__2cR-R"><span class="listing-detail_text__31u2P">Chontaduro Barcelona<span class="listing-detail_claim__10fsw color-primary"><i class="la la-check"></i><span>Claimed</span></span></span></h1> <span
                    class="listing-detail_tagline__3u_9y">Chontaduro is a new restaurant experience in Barcelona</span>
            </div>
        </div>
        <div class="listing-info-right">
            <div class="listing-detail_rightButton__30xaS clearfix">
                <div class="listing-detail_rightItem__2CjTS wilcity-single-tool-favorite"><a data-post-id="2018" href="#" class="wil-btn wil-btn--border wil-btn--round wil-btn--sm wilcity-js-favorite"><i class="color-quaternary la la-heart-o"></i>
                        Favorite </a></div>
                <div class="listing-detail_rightItem__2CjTS wilcity-single-tool-share"><a href="#" data-toggle-button="share" data-body-toggle="true" class="wil-btn wil-btn--border wil-btn--round wil-btn--sm" style="user-select: none;"><i class="color-primary la la-share"></i>
                        Share </a>
                    <div data-toggle-content="share" class="listing-detail_shareContent__2nr-2">
                        <ul class="list_module__1eis9 list-none list_social__31Q0V list_medium__1aT2c list_abs__OP7Og arrow--top-right ">
                            <li class="list_item__3YghP"><a href="//facebook.com/sharer.php?u=https://wilcity.com/listing/chontaduro-barcelona/&amp;t=Chontaduro%20Barcelona" target="_blank" class="list_link__2rDA1 text-ellipsis color-primary--hover"><span
                                        class="list_icon__2YpTp"><i class="fa fa-facebook"></i></span><span class="list_text__35R07">Facebook</span></a></li>
                            <li class="list_item__3YghP"><a href="//twitter.com/intent/tweet?text=Chontaduro%20Barcelona-https://wilcity.com/listing/chontaduro-barcelona/&amp;source=webclient" target="_blank" class="list_link__2rDA1 text-ellipsis color-primary--hover"><span
                                        class="list_icon__2YpTp"><i class="fa fa-twitter"></i></span><span class="list_text__35R07">Twitter</span></a></li>
                            <li class="list_item__3YghP"><a href="//plus.google.com/share?url=https://wilcity.com/listing/chontaduro-barcelona/&amp;title=Chontaduro%20Barcelona&amp;source=webclient" target="_blank" class="list_link__2rDA1 text-ellipsis color-primary--hover"><span
                                        class="list_icon__2YpTp"><i class="fa fa-google-plus"></i></span><span class="list_text__35R07">Google+</span></a></li>
                            <li class="list_item__3YghP"><a href="//www.tumblr.com/share/link?url=https://wilcity.com/listing/chontaduro-barcelona/&amp;name=Chontaduro%20Barcelona&amp;source=webclient" target="_blank" class="list_link__2rDA1 text-ellipsis color-primary--hover"><span
                                        class="list_icon__2YpTp"><i class="fa fa-tumblr"></i></span><span class="list_text__35R07">Tumblr</span></a></li>
                            <li class="list_item__3YghP"><a href="//vk.com/share.php?url=https://wilcity.com/listing/chontaduro-barcelona/&amp;description=Chontaduro%20Barcelona&amp;source=webclient" target="_blank" class="list_link__2rDA1 text-ellipsis color-primary--hover"><span
                                        class="list_icon__2YpTp"><i class="fa fa-vk"></i></span><span class="list_text__35R07">Vk</span></a></li>
                            <li class="list_item__3YghP">
                                <!---->
                            </li>
                            <li class="list_item__3YghP">
                                <!---->
                            </li>
                            <li class="list_item__3YghP">
                                <!---->
                            </li>
                            <li class="list_item__3YghP">
                                <!---->
                            </li>
                            <li class="list_item__3YghP">
                                <!---->
                            </li>
                            <li class="list_item__3YghP">
                                <!---->
                            </li>
                            <li class="list_item__3YghP">
                                <!---->
                            </li>
                            <li class="list_item__3YghP"><a href="//pinterest.com/pin/create/button/?url=https://wilcity.com/listing/chontaduro-barcelona/&amp;description=https://wilcity.com/listing/chontaduro-barcelona/&amp;source=webclient" target="_blank"
                                    class="list_link__2rDA1 text-ellipsis color-primary--hover"><span class="list_icon__2YpTp"><i class="fa fa-pinterest"></i></span><span class="list_text__35R07">Pinterest</span></a></li>
                            <li class="list_item__3YghP">
                                <!---->
                            </li>
                            <li class="list_item__3YghP">
                                <!---->
                            </li>
                            <li class="list_item__3YghP">
                                <!---->
                            </li>
                            <li class="list_item__3YghP"><a href="//www.stumbleupon.com/submit?url=https://wilcity.com/listing/chontaduro-barcelona/&amp;title=Chontaduro%20Barcelona&amp;source=webclient" target="_blank" class="list_link__2rDA1 text-ellipsis color-primary--hover"><span
                                        class="list_icon__2YpTp"><i class="fa fa-stumbleupon"></i></span><span class="list_text__35R07">Stumbleupon</span></a></li>
                            <li class="list_item__3YghP">
                                <!---->
                            </li>
                            <li class="list_item__3YghP"><a href="//www.linkedin.com/shareArticle?mini=true&amp;url=https://wilcity.com/listing/chontaduro-barcelona/&amp;title=Chontaduro%20Barcelona&amp;source=webclient" target="_blank" class="list_link__2rDA1 text-ellipsis color-primary--hover"><span
                                        class="list_icon__2YpTp"><i class="fa fa-linkedin"></i></span><span class="list_text__35R07">Linkedin</span></a></li>
                            <li class="list_item__3YghP">
                                <!---->
                            </li>
                            <li class="list_item__3YghP">
                                <!---->
                            </li>
                            <li class="list_item__3YghP">
                                <!---->
                            </li>
                            <li class="list_item__3YghP">
                                <!---->
                            </li>
                            <li class="list_item__3YghP"><a href="mailto:?Subject=Chontaduro%20Barcelona&amp;Body=https://wilcity.com/listing/chontaduro-barcelona/" class="list_link__2rDA1 text-ellipsis color-primary--hover"><span class="list_icon__2YpTp"><i
                                            class="fa fa-envelope"></i></span><span class="list_text__35R07">Email</span></a></li>
                            <li class="list_item__3YghP"><a href="#" data-shortlink="https://wilcity.com/listing/chontaduro-barcelona/" class="list_link__2rDA1 text-ellipsis color-primary--hover"><span class="list_icon__2YpTp"><i class="fa fa-link"></i></span><span
                                        class="list_text__35R07">Copy Link</span></a></li>
                        </ul>
                    </div>
                </div>
                <div class="listing-detail_rightItem__2CjTS wilcity-single-tool-inbox"><a href="#" class="wil-btn wil-btn--border wil-btn--round wil-btn--sm"><i class="la la-envelope color-1"></i>Inbox
                    </a></div>
            </div>
            <div class="listing-detail_rightDropdown__3J1qK">
                <div class="dropdown_module__J_Zpj">
                    <div data-toggle-button="dropdown" data-body-toggle="true" class="dropdown_threeDots__3fa2o" style="user-select: none;"><span class="dropdown_dot__3I1Rn"></span><span class="dropdown_dot__3I1Rn"></span><span class="dropdown_dot__3I1Rn"></span></div>
                    <div data-toggle-content="dropdown" class="dropdown_itemsWrap__2fuze">
                        <ul class="list_module__1eis9 list-none list_small__3fRoS list_abs__OP7Og arrow--top-right">
                            <li class="list_item__3YghP"><a href="#" class="list_link__2rDA1 text-ellipsis color-primary--hover"><span class="list_icon__2YpTp"><i class="la la-star-o"></i></span><span class="list_text__35R07">Write a review</span></a></li>
                            <li class="list_item__3YghP"><a href="#" class="list_link__2rDA1 text-ellipsis color-primary--hover"><span class="list_icon__2YpTp"><i class="la la-flag-o"></i></span><span class="list_text__35R07">Report</span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
