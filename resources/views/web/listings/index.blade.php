@extends('web.layouts.app')

@section('title', $page->title ?? '')

@section('content')

    <div class="container jb-subcategories">
        <div class="row">

            <div class="col-xs-12 col-md-9">

                <div class="row">
                    @foreach ($listings as $listing)

                        @include('web._jb.card-subcategory-listing', ['listing' => $listing])

                    @endforeach

                </div>

            </div>

        </div>
    </div>

@endsection
