@extends('web.layouts.app')

@section('title', $listing->name)

@push('head')
    <link href="{{ asset('css/wc.css') }}" rel="stylesheet">
@endpush

@section('content')

    <div class="fe-listing-detail">

        @include('web.listings._partials.wc.header')

        @include('web.listings._partials.wc.detail')

        @include('web.listings._partials.wc.navtop')

        @include('web.listings._partials.wc.body')

    </div>


@endsection
