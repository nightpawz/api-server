<div class="col-6 col-sm-6 col-md-4 col-lg-3 xs-no-gutter">

    @wcardicon([
        'image_url' => $category->image_url,
        'icon_url' => $category->icon_url,
        'action' => route('categories.show', $category->slug)
    ])

        <span class="fe-title">{{ $category->name ?? '' }}</span>
        <h3 class="fe-meta">{{ $category->childs_count }} {{ __('Subcategories') }}</h3>

    @endwcardicon

</div>
