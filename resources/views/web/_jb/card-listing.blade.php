<div class="col-lg-4 col-md-4 col-sm-6 col-6 no-gutter">

    <article class="fe-card-listing">
        <div class="card-header">
            <div class="card-banner">
                <a href="{{ route('listings.show', $listing->slug) }}">
                    @wimg([
                        'src' => $listing->thumbnail_url ?? '',
                        'alt' => $listing->name ?? '',
                        'ratio' => 'p-57'
                    ])

                    <span class="overlay"></span>
                </a>
            </div>
            <div class="card-logo-container">
                <div class="card-logo">
                    <a href="{{ route('listings.show', $listing->slug) }}">
                        @wimglogo([
                            'src' => $listing->logo_url ?? '',
                            'alt' => $listing->name ?? '',
                            'ratio' => 'r-1-1'
                        ])
                    </a>
                </div>

                <div class="card-logo-bg" style="background-image: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjwhLS0gR2VuZXJhdG9yOiBBZG9iZSBJbGx1c3RyYXRvciAyMS4wLjAsIFNWRyBFeHBvcnQgUGx1Zy1JbiAuIFNWRyBWZXJzaW9uOiA2LjAwIEJ1aWxkIDApICAtLT4NCjxzdmcgdmVyc2lvbj0iMS4xIiBpZD0iTGF5ZXJfMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgeD0iMHB4IiB5PSIwcHgiDQoJIHZpZXdCb3g9IjAgMCA3NiAzMSIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgNzYgMzE7IiB4bWw6c3BhY2U9InByZXNlcnZlIj4NCjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+DQoJLnN0MHtmaWxsOiNGRkZGRkY7fQ0KPC9zdHlsZT4NCjx0aXRsZT5iZ19zb3VyY2VfYmFkZ2VfMTwvdGl0bGU+DQo8cGF0aCBjbGFzcz0ic3QwIiBkPSJNNTcuNywxMS40Yy0xLjQtMS40LTIuNy0yLjktNC4xLTQuNGMtMC4yLTAuMy0wLjUtMC41LTAuOC0wLjhjLTAuMi0wLjItMC4zLTAuMy0wLjUtMC41bDAsMA0KCUM0OC42LDIuMiw0My41LDAsMzgsMFMyNy40LDIuMiwyMy42LDUuN2wwLDBjLTAuMiwwLjItMC4zLDAuMy0wLjUsMC41Yy0wLjMsMC4zLTAuNSwwLjUtMC44LDAuOGMtMS40LDEuNS0yLjcsMy00LjEsNC40DQoJYy01LDUuMS0xMS43LDYuMS0xOC4zLDYuM1YzMWg5LjRoOC45aDM5LjRoNC45SDc2VjE3LjZDNjkuNCwxNy40LDYyLjcsMTYuNSw1Ny43LDExLjR6Ii8+DQo8L3N2Zz4NCg==)"></div>
            </div>
        </div>
        <div class="card-body">
            <a href="{{ route('listings.show', $listing->slug) }}">
                <h2 class="card-title text-elipsis">{{ $listing->name ?? '' }}</h2>
            </a>
            <div class="card-excerpt text-elipsis">{{ $listing->excerpt ?? '' }}</div>
            <div class="card-contact">
                <a class="text-elipsis"><i class="fa fa-map-marker"></i> {{ $listing->address ?? '' }}</a>
                <a class="text-elipsis"><i class="fa fa-phone"></i> {{ $listing->phone ?? '' }}</a>
            </div>
        </div>
        <div class="card-footer">
            <div class="left-menu">
                <div class="icon-box">
                    <a href="#">
                        {{-- <img src="{{ $listing->category->icon_url ?? '' }}" alt=""> --}}
                        <span>{{ $listing->category->name ?? '' }}</span>
                    </a>
                </div>
            </div>
        </div>
    </article>


</div>
