<div class="col-6 col-sm-6 col-md-6 col-lg-4 no-gutter">

    <div class="fe-subcategory">

        <a href="{{ route('listings.category', $category->slug) }}">
            <div class="fe-body">

                <div class="fe-content">
                    <h3 class="fe-title">{{ $category->name ?? '' }}</h3>
                    <p class="fe-meta">{{ $category->listings_count }} {{ __('Listings') }}</p>
                </div>

                <span class="fe-arrow">
                    <i class="fa fa-long-arrow-right"></i>
                </span>
            </div>
        </a>

    </div>

</div>
