<div class="fe-swiper-outter {{ $class ?? '' }}">
    <!-- Slider main container -->
    <div class="swiper-container">
        <!-- Additional required wrapper -->
        <div class="swiper-wrapper">
            <!-- Slides -->

            {{ $slot }}
        </div>

        @if ($navigation ?? false)
            <!-- If we need navigation buttons -->
            <div class="fe-swiper-control swiper-control-prev">
                <div class="control-wrapper">
                    <i class="fa fa-angle-left"></i>
                </div>
            </div>
            <div class="fe-swiper-control swiper-control-next">
                <div class="control-wrapper">
                    <i class="fa fa-angle-right"></i>
                </div>
            </div>
        @endif

    </div>

    @if ($pagination ?? false)
        <!-- If we need pagination -->
        <div class="swiper-pagination"></div>
    @endif



    @if ($scrollbar ?? false)
        <!-- If we need scrollbar -->
        <div class="swiper-scrollbar"></div>
    @endif

</div>
