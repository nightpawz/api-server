<div class="swiper-slide {{ $class ?? '' }}"
    @if (isset($width))
        style="width: {{ $width }}"
    @endif
>{{ $slot }}</div>