<section class="fe-section {{ $class ?? '' }}">
    <div class="container fe-section-heading">
        <h2 class="heading-background-text">{{ $bg_text ?? '' }}</h2>
        <div class="fe-heading-text">
            <h3 class="heading-title">{{ $title ?? '' }}</h3>
            <p class="heading-subtitle">{{ $subtitle ?? '' }}</p>
        </div>
    </div>


    {{ $slot }}
</section>
