<div class="carousel-item {{ isset($active) && $active === true ? 'active' : '' }}"> 
    <img class="d-block img-fluid w-100" src="{{ $src ?? '' }}">
    <div class="carousel-caption">
        {{ $slot }}
    </div>
</div>