<div class="carousel slide carousel-fade {{ $class ?? '' }}" data-ride="carousel" id="{{ $id }}">
    <div class="carousel-inner">

        {{ $slot }}

    </div> 

    @if (isset($controls) && $controls === true)
        <a class="carousel-control-prev" href="#{{ $id }}" role="button" data-slide="prev"> 
            <span class="carousel-control-prev-icon"></span> 
            <span class="sr-only">Previous</span> 
        </a> 
        <a class="carousel-control-next" href="#{{ $id }}" role="button" data-slide="next"> 
            <span class="carousel-control-next-icon"></span> 
            <span class="sr-only">Next</span> 
        </a> 
    @endif
</div>