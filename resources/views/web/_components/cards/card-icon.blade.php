<div class="fe-card-icon {{ $class ?? '' }}">
    <a href="{{ $action ?? '' }}">

        @isset($image_url)
            <div class="fe-cover" style="background-image: url('{{ $image_url ?? '' }}')">
                <div class="fe-overlay"></div>

                <div class="icon">
                    <img src="{{ $icon_url ?? '' }}">
                </div>
            </div>
        @endisset
        <div class="fe-body">
            {{ $slot }}
            <span class="fe-arrow">
                <i class="fa fa-long-arrow-right"></i>
            </span>
        </div>
    </a>
</div>
