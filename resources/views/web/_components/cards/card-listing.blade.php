{{-- <article class="listing_module__2EnGq wil-shadow mb-30 js-grid-item js-listing-module">
    <div class="js-grid-item-body listing_firstWrap__36UOZ" style="height: 368px;">
        <header class="listing_header__2pt4D"> <a href="https://wilcity.com/listing/chontaduro-barcelona/">
                <div class="listing_img__3pwlB pos-a-full bg-cover" style="background-image: url(https://i1.wp.com/wilcity.com/wp-content/uploads/2018/05/Barcelona_5_c.jpg?fit=449%2C300&amp;ssl=1);"> <img src="https://i1.wp.com/wilcity.com/wp-content/uploads/2018/05/Barcelona_5_c.jpg?fit=449%2C300&amp;ssl=1"
                        alt="Chontaduro Barcelona"></div>
                <div class="listing_rated__1y7qV">
                    <div class="rated-small_module__1vw2B rated-small_style-2__3lb7d">
                        <div class="rated-small_wrap__2Eetz" data-rated="7.4" data-tenmode="7.4">
                            <div class="rated-small_overallRating__oFmKR">7.4</div>
                            <div class="rated-small_ratingWrap__3lzhB">
                                <div class="rated-small_maxRating__2D9mI">10</div>
                                <div class="rated-small_ratingOverview__2kCI_">Poor</div>
                            </div>
                        </div>
                    </div>
                </div>
            </a></header>
        <div class="listing_body__31ndf"> <a class="listing_goo__3r7Tj" href="https://wilcity.com/listing/chontaduro-barcelona/">
                <div class="listing_logo__PIZwf bg-cover" style="background-image: url(https://wilcity.com/wp-content/uploads/2018/08/Atlanta_1_logo_.jpg);"></div>
            </a>
            <h2 class="listing_title__2920A text-ellipsis"> <a href="https://wilcity.com/listing/chontaduro-barcelona/">Chontaduro Barcelona</a></h2>
            <div class="listing_tagline__1cOB3 text-ellipsis">Chontaduro is a new restaurant experience in Barcelona</div>
            <div class="listing_meta__6BbCG vertical"> <a class="text-ellipsis google-address" href="https://wilcity.com/map-style/?title=Chontaduro+Barcelona&amp;lat=41.396572&amp;lng=2.149053" data-tooltip="Carrer d'Aribau, 254, 08006 Barcelona, Spain">
                    <i class="la la-map-marker color-primary"></i>Carrer d'Aribau, 254, 08006 Barcelona, Spain </a> <a class="text-ellipsis phone-number" href="tel:1123456789"> <i class="la la-phone color-primary"></i>1123456789 </a></div>
        </div>
    </div>
    <footer class="js-grid-item-footer listing_footer__1PzMC" style="height: 57px;">
        <div class="text-ellipsis">
            <div class="icon-box-1_module__uyg5F two-text-ellipsis icon-box-1_style2__1EMOP">
                <div class="icon-box-1_block1__bJ25J"><a href="https://wilcity.com/listing-cat/restaurants/">
                        <div class="icon-box-1_icon__3V5c0 rounded-circle" style="background-color: #006bf7"><i class="la la-cutlery"></i></div>
                        <div class="icon-box-1_text__3R39g">Restaurants</div>
                    </a></div>
                <div class="icon-box-1_block2__1y3h0 wilcity-listing-hours"><span class="color-quaternary">Closed</span></div>
            </div>
        </div>
        <div class="listing_footerRight__2398w"> <a class="wilcity-preview-gallery" href="#" data-tooltip="Gallery" data-tooltip-placement="top" data-gallery="https://i2.wp.com/wilcity.com/wp-content/uploads/2018/05/Tokyo_2_b.jpg?fit=1024%2C642&amp;ssl=1,https://i0.wp.com/wilcity.com/wp-content/uploads/2018/05/Tokyo_2_c.jpg?fit=1024%2C678&amp;ssl=1,https://i2.wp.com/wilcity.com/wp-content/uploads/2018/05/Tokyo_2_d.jpg?fit=1024%2C666&amp;ssl=1,https://i0.wp.com/wilcity.com/wp-content/uploads/2018/05/Singapore_1_a.jpg?fit=1024%2C665&amp;ssl=1"><i
                    class="la la-search-plus"></i></a> <a class="wilcity-js-favorite" data-post-id="2018" href="#" data-tooltip="Save to my favorites" data-tooltip-placement="top"><i class="la la-heart-o"></i></a></div>
    </footer>
</article> --}}

<article class="fe-card-listing">
    <div class="card-header">
        @wimg([
            'src' => $src ?? '',
            'alt' => $alt ?? '',
            'ratio' => 'r-4-3'
        ])
    </div>
    <div class="card-body">

    </div>
    <div class="card-footer">
        <div class="left-menu">
            <div class="icon-box">
                <div class="category">
                    <a href="#">
                        {{-- <img src="{{ $src ?? '' }}" alt="{{ $alt ?? '' }}" class="img-icon"> --}}
                        <span></span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</article>
