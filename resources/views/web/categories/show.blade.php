@extends('web.layouts.app')

@section('title', $category->name)

@section('content')

    <div class="container jb-subcategories">
        <div class="row">

            <div class="col-12 no-gutter">
                @include('web.categories._partials.top-banner')
            </div>

            <div class="col-xs-12 col-md-9">

                <div class="row">

                    @foreach ($category->childs()->withCount('listings')->get() as $category)

                        @include('web._jb.card-subcategory', ['category' => $category])

                        @foreach ($category->getBanners('side') as $banner)

                            @if($loop->parent->iteration % 6 == 0)

                                @include('web.categories._partials.side-banner-card', ['banner' => $banner])

                            @endif

                            @break($loop->iteration == 2)

                        @endforeach

                    @endforeach

                </div>

            </div>

            <div class="col-xs-12 col-md-3 d-none d-sm-none d-md-block d-lg-block">
                @include('web.categories._partials.side-banner')
            </div>
        </div>
    </div>

@endsection
