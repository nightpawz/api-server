<div class="col-6 d-none d-sm-none d-md-block d-lg-block">
    <div class="jb-side-banner-card banner-mobile">
        @wimg([
            'src' => $banner->image_url,
            'ratio' => 'jb-side-banner-img'
        ])
    </div>
</div>
