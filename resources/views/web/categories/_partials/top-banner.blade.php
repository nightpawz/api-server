@php
    $banners = $category->getBanners('top');
@endphp

@if ($banners->isNotEmpty())

<div class="jb-top-banner">

        <div id="topbanner" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                @foreach ($banners as $item)
                    <li data-target="#topbanner" data-slide-to="{{ $loop->index }}" class="{{ $loop->first ? 'active' : '' }}"></li>
                @endforeach
            </ol>

            <div class="carousel-inner">
                @foreach ($banners as $item)
                    <div class="carousel-item {{ $loop->first ? 'active' : '' }}">
                        @wimg([
                            'src' => $item->image_url,
                            'ratio' => 'r-16-5'
                        ])
                    </div>
                @endforeach
            </div>

            <a class="carousel-control-prev" href="#topbanner" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#topbanner" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>

</div>

@endif
