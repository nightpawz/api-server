@php
    $banners_side = $category->getBanners('side');
@endphp


<div class="row">
    @foreach ($banners_side as $item)
        <div class="col-12">
            <div class="jb-side-banner-card">
                @wimg([
                    'src' => $item->image_url,
                    'ratio' => 'jb-side-banner-img'
                ])
                {{-- <img src="{{ $item->image_url }}" alt="" style="height: auto; width: 100%"> --}}
            </div>
        </div>
    @endforeach
    <div class="col-12">
        <div class="jb-side-banner-card">
            {{-- @wimg([
                'src' => asset('img/default-side-banner.jpg'),
                'ratio' => 'jb-side-banner-img'
            ]) --}}
            <img src="{{ asset('img/default-side-banner.jpg') }}" alt="" style="height: auto; width: 100%">
        </div>
    </div>
</div>
