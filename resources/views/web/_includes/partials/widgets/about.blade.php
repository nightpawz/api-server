<section class="fe-widget">
    <h2 class="widget-title">{{ __('About') }}</h2>

    <div class="widget-body">
        <section class="fe-widget">
            <a href="/">
                <img height="80" src="{{ asset('img/logo-footer.png') }}" class="img-logo">
            </a>
        </section>

        <p class="widget-text">PT Cerise merupakan perusahaan yang bergerak dibidang Teknologi Informasi sejak tahun 2011 di Kota Yogyakarta. Mempunyai beberapa layanan seperti Direktori Bisnis (JogjaBagus) dan Jasa Pembuatan Website.</p>
    </div>

</section>
