<section class="fe-widget">
    <h2 class="widget-title">Discover</h2>
    <div class="widget-body">
        <ul class="menu">
            <li class="menu-item">
                <a href="#" class="menu-link">Category</a>
            </li>
            <li class="menu-item">
                <a href="#" class="menu-link">Listings</a>
            </li>
            <li class="menu-item">
                <a href="#" class="menu-link">About</a>
            </li>
            <li class="menu-item">
                <a href="#" class="menu-link">Contact</a>
            </li>
        </ul>
    </div>
</section>
