<section class="fe-widget">

    <div class="widget-body">
        <div class="fe-stats">
            <div class="stats-item">
                <div class="icon-wrap">
                    <i class="fa fa-map-marker"></i>
                </div>
                <div class="stats-text">
                    <p class="stats-text-title">Taman Siswa Business Center (TSBC) Slot B.2 Jl. Taman Siswa No 160 Yogyakarta</p>
                </div>
            </div>

            <div class="stats-item">
                <div class="icon-wrap">
                    <i class="fa fa-envelope"></i>
                </div>
                <div class="stats-text">
                    <p class="stats-text-title">info@cerise.id</p>
                </div>
            </div>

            <div class="stats-item">
                <div class="icon-wrap">
                    <i class="fa fa-phone"></i>
                </div>
                <div class="stats-text">
                    <p class="stats-text-title">(0274) 4393423 / (0274) 2870514</p>
                </div>
            </div>
        </div>
    </div>
</section>
