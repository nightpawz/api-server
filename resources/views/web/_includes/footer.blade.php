<footer class="fe-footer jb-footer">
    <div class="container">
        <div class="row">

            <div class="col-md-4">
                @include('web._includes.partials.widgets.navigation')
                {{-- @include('web._includes.partials.widgets.info') --}}
            </div>

            <div class="col-md-4">
                @include('web._includes.partials.widgets.gallery')
            </div>

            <div class="col-md-4">
                @include('web._includes.partials.widgets.about')
            </div>

            {{-- <div class="col-md-3 col-lg-3">
                @include('web._includes.partials.widgets.about')

            </div> --}}
        </div>

        <div class="row">
            <div class="col-12">

                <div class="fe-footer-copy">
                    <p>Copyright © 2018 {{ setting('app.name') }}.</p>
                </div>

            </div>
        </div>
    </div>



</footer>
