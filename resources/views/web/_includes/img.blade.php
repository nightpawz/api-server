<div class="fe-img-container {{ $ratio ?? 'r-4-3' }} {{ $class ?? '' }}">
    <img src="{{ $src ?? '' }}" alt="{{ $alt ?? '' }}">
</div>

{{--
 * @param string $src : The image source url
 * @param string $alt : The alt text displayed when image is not available
 * @param string $ratio : The class ratio of the image
 *
--}}
