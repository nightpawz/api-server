<div class="fe-img-container {{ $ratio ?? 'r-4-3' }}">
    <span class="alt">{{ $alt[0] ?? '' }}</span>
    <img src="{{ $src !== '' ? $src : asset('img/default-listing-logo.png') }}" alt="">
</div>

{{--
 * @param string $src : The image source url
 * @param string $alt : The alt text displayed when image is not available
 * @param string $ratio : The class ratio of the image
 *
--}}
