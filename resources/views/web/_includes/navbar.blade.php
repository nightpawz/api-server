<nav class="navbar navbar-expand-md navbar-light fixed-top fe-navbar {{ isset($page) && $page->view == 'home' ? '' : 'always-sticky' }}" id="sticky-navbar">
    <div class="container-fluid">
        <a class="navbar-brand text-primary" href="{{ route('home') }}">
            <img src="{{ asset('img/logo-jb.png') }}" alt="{{ setting('app.name') }} Logo" class="img-brand">
        </a>
        <button class="navbar-toggler navbar-toggler-right border-0" type="button" data-toggle="collapse" data-target="#navbar4">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbar4">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item"><a class="nav-link" href="/"><i class="fa fa-home"></i> Home</a></li>
                <li class="nav-item"><a class="nav-link" href="{{ route('categories.index') }}">Category</a></li>
                <li class="nav-item"><a class="nav-link" href="{{ route('about') }}">About</a></li>
                <li class="nav-item"><a class="nav-link" href="{{ route('contact') }}">Contact</a></li>
                |
                @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                    @if ($localeCode !== app()->getLocale())
                        <li class="nav-item">
                            <a rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}" class="nav-link text-uppercase">
                                {{ $localeCode }}
                            </a>
                        </li>
                    @endif
                @endforeach
            </ul>
        <a href="{{ route('login') }}" class="btn btn-outline-primary fe-btn-outline ml-20">{{ __('Login') }}</a>
            {{-- <a href="javascript:void(0)" class="btn navbar-btn ml-md-2 btn-light">Register</a> --}}
        </div>
    </div>
</nav>
