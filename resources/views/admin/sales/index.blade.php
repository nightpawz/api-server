@extends('admin.layouts.app')

@section('title', __('Sales'))

@section('content')

<div class="row">
    <div class="sixteen wide column">

        @uisegment

            @slot('header')
                <h5 class="ui header">{{ __('Sales') }}</h5>
                <a href="{{ route('admin.sales.create') }}" class="ui basic button button-right">
                    <i class="fa fa-plus"></i>
                    {{ __('Add Sales') }}
                </a>
            @endslot

            @uidatatable([
                'url' => route('admin.sales.datatable', ['role' => 'sales']),
                'columns' => [
                    '#' => 'DT_RowIndex',
                    __('Name') => 'name',
                    __('Email') => 'email',
                    __('Action') => 'action'
                ]
            ])

        @enduisegment

    </div>
</div>

@endsection
