@extends('admin.layouts.app')

@section('title', __('Edit Member'))

@section('content')

<div class="row">
    <div class="sixteen wide column">

        @uisegment(['space' => true])

            @slot('header')
                <h5 class="ui header">{{ __('Edit Member') }}</h5>
            @endslot

            @uiform([
                'action' => route('admin.members.update', $member),
                'method' => 'PUT'
            ])

                @uiinput([
                    'name' => 'name',
                    'label' => __('Name'),
                    'value' => $member->name ?? ''
                ])

                @uiinput([
                    'name' => 'email',
                    'label' => __('Email'),
                    'type' => 'email',
                    'value' => $member->email ?? ''
                ])

                @uiinput([
                    'name' => 'password',
                    'label' => __('Password'),
                    'type' => 'password'
                ])

                @uiinput([
                    'name' => 'password_confirmation',
                    'label' => __('Password Confirmation'),
                    'type' => 'password'
                ])


            @enduiform

        @enduisegment

    </div>
</div>

@endsection
