@extends('admin.layouts.app')

@section('title', __('Create Sales'))

@section('content')

<div class="row">
    <div class="sixteen wide column">

        @uisegment(['space' => true])

            @slot('header')
                <h5 class="ui header">{{ __('Create Sales') }}</h5>
            @endslot

            @uiform([
                'action' => route('admin.sales.store'),
                'method' => 'POST'
            ])

                @uiinput([
                    'name' => 'name',
                    'label' => __('Name')
                ])

                @uiinput([
                    'name' => 'email',
                    'label' => __('Email'),
                    'type' => 'email'
                ])

                @uiinput([
                    'name' => 'password',
                    'label' => __('Password'),
                    'type' => 'password'
                ])

                @uiinput([
                    'name' => 'password_confirmation',
                    'label' => __('Password Confirmation'),
                    'type' => 'password'
                ])


            @enduiform

        @enduisegment

    </div>
</div>

@endsection
