@uisegment([
    'class' => 'mb-20',
    'space' => true
])

    @slot('header')
        <h5 class="ui header">{{ __('Application') }}</h5>
    @endslot


    @uiform([
        'action' => route('admin.settings.store'),
        'method' => 'POST',
        'file' => true
    ])

    <div class="ui grid">

        <div class="row">
            <div class="sixteen wide column centered">

                @uisegment([
                    'header' => __('Profile')
                ])

                    @uiinputimage([
                        'name' => 'logo',
                        'src' => asset(setting('app.logo'))
                    ])

                    @uiinput([
                        'label' => __('Name'),
                        'name' => "app[name]",
                        'placeholder' => __('Application Name'),
                        'value' => setting('app.name')
                    ])

                    @uitextarea([
                        'label' => __('Description'),
                        'name' => "app[description]",
                        'placeholder' => __('Description'),
                        'value' => setting('app.description')
                    ])

                @enduisegment



            </div>
        </div>

    </div>

    @enduiform




@enduisegment
