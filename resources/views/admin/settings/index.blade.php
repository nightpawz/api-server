@extends('admin.layouts.app')

@section('title', __('Settings'))

@section('content')

<div class="row">
    <div class="sixteen wide column">

        @include('admin.settings._sections.app')

    </div>
</div>

@endsection
