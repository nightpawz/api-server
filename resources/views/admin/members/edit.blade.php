@extends('admin.layouts.app')

@section('title', __('Edit Member'))

@section('content')

<div class="row">
    <div class="sixteen wide column">

        @uisegment(['space' => true])

            @slot('header')
                <h5 class="ui header">{{ __('Edit Member') }}</h5>
            @endslot

            @uiform([
                'action' => route('admin.members.update', $member),
                'method' => 'PUT'
            ])

                @uiinput([
                    'name' => 'name',
                    'label' => __('Name'),
                    'value' => $member->name ?? ''
                ])

                @uiinput([
                    'name' => 'company',
                    'label' => __('Company'),
                    'value' => $member->company ?? ''
                ])

                @uiinput([
                    'name' => 'phone',
                    'label' => __('Phone'),
                    'value' => $member->phone ?? ''
                ])

                @uiinput([
                    'name' => 'email',
                    'label' => __('Email'),
                    'type' => 'email',
                    'value' => $member->email ?? ''
                ])

            @enduiform

        @enduisegment

    </div>
</div>

@endsection
