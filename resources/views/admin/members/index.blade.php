@extends('admin.layouts.app')

@section('title', __('Members'))

@section('content')

<div class="row">
    <div class="sixteen wide column">

        @uisegment

            @slot('header')
                <h5 class="ui header">{{ __('Members') }}</h5>
                <a href="{{ route('admin.members.create') }}" class="ui basic button button-right">
                    <i class="fa fa-plus"></i>
                    {{ __('Add Member') }}
                </a>
            @endslot

            @uidatatable([
                'url' => route('admin.members.datatable', ['role' => 'member']),
                'columns' => [
                    '#' => 'DT_RowIndex',
                    __('Name') => 'name',
                    __('Email') => 'email',
                    __('Action') => 'action'
                ]
            ])

        @enduisegment

    </div>
</div>

@endsection
