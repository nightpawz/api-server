@extends('admin.layouts.app')

@section('title', __('Create Member'))

@section('content')

<div class="row">
    <div class="sixteen wide column">

        @uisegment(['space' => true])

            @slot('header')
                <h5 class="ui header">{{ __('Create Member') }}</h5>
            @endslot

            @uiform([
                'action' => route('admin.members.store'),
                'method' => 'POST'
            ])

                @uiinput([
                    'name' => 'name',
                    'label' => __('Name')
                ])

                @uiinput([
                    'name' => 'company',
                    'label' => __('Company')
                ])

                @uiinput([
                    'name' => 'phone',
                    'label' => __('Phone')
                ])

                @uiinput([
                    'name' => 'email',
                    'label' => __('Email'),
                    'type' => 'email'
                ])

            @enduiform

        @enduisegment

    </div>
</div>

@endsection
