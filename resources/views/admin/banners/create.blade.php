@extends('admin.layouts.app')

@section('title', __('Add Image'))

@section('content')

    <div class="row">
        <div class="sixteen wide column">

            @uisegment([
                'class' => 'mb-20',
                'space' => true
            ])

                @slot('header')
                    <h5 class="ui header">{{ __('Add Image') }}</h5>
                @endslot

                @uiform([
                    'action' => route('admin.banners.store'),
                    'method' => 'POST'
                ])
                    <input type="hidden" name="group" value="{{ Request::input('group') }}">
                    <input type="hidden" name="category_id" value="{{ Request::input('category_id') }}">

                    @uicropit([
                        'w' => Request::input('w'),
                        'h' => Request::input('h'),
                        'name' => 'image',
                        'label' => __('Image')
                    ])

                    @uidaterange([
                        'label' => __('Display Date'),
                        'name_start' => 'display_date',
                        'name_end' => 'expired_date'
                    ])

                @enduiform


            @enduisegment


        </div>
    </div>

@endsection
