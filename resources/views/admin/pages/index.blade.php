@extends('admin.layouts.app')

@section('title', $page->name)

@section('content')

<div class="row">
    <div class="sixteen wide column">

        @include('admin.pages._sections.seo')

        @foreach ($page->sections()->onlyParents()->get() as $section)
            @includeif("admin.pages._sections.{$page->view}-{$section->name}", ['section' => $section])
        @endforeach

    </div>
</div>

@endsection
