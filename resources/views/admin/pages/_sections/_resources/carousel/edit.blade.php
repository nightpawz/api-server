@extends('admin.layouts.app')

@section('title', __('Edit Image'))

@section('content')

    <div class="row">
        <div class="sixteen wide column">

            @uisegment([
                'class' => 'mb-20',
                'space' => true
            ])

                @slot('header')
                    <h5 class="ui header">{{ __('Edit Image') }}</h5>
                @endslot

                @sectionform([
                    'action' => route('admin.sections.carousel.update', $section),
                    'method' => 'PUT',
                    'input' => [
                        'image' => true,
                        'image_width' => 1583,
                        'image_height' => 789
                    ],
                    'section' => $section
                ])

            @enduisegment


        </div>
    </div>

@endsection
