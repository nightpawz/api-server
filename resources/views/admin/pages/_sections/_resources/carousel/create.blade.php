@extends('admin.layouts.app')

@section('title', __('Add Image'))

@section('content')

    <div class="row">
        <div class="sixteen wide column">

            @uisegment([
                'class' => 'mb-20',
                'space' => true
            ])

                @slot('header')
                    <h5 class="ui header">{{ __('Add Image') }}</h5>
                @endslot

                @sectionform([
                    'action' => route('admin.sections.carousel.store'),
                    'method' => 'POST',
                    'input' => [
                        'image' => true,
                        'image_width' => 1583,
                        'image_height' => 789
                    ]
                ])

            @enduisegment


        </div>
    </div>

@endsection
