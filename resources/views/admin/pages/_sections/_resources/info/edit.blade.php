@extends('admin.layouts.app')

@section('title', __('Edit Card'))

@section('content')

    <div class="row">
        <div class="sixteen wide column">

            @uisegment([
                'class' => 'mb-20',
                'space' => true
            ])

                @slot('header')
                    <h5 class="ui header">{{ __('Edit Card') }}</h5>
                @endslot

                @sectionform([
                    'action' => route('admin.sections.info.update', $section),
                    'method' => 'PUT',
                    'input' => [
                        'image' => true,
                        'image_width' => 540,
                        'image_height' => 308,
                        'content_editor' => true,
                    ],
                    'section' => $section
                ])

            @enduisegment


        </div>
    </div>

@endsection
