@extends('admin.layouts.app')

@section('title', __('Add Card'))

@section('content')

    <div class="row">
        <div class="sixteen wide column">

            @uisegment([
                'class' => 'mb-20',
                'space' => true
            ])

                @slot('header')
                    <h5 class="ui header">{{ __('Add Card') }}</h5>
                @endslot

                @sectionform([
                    'action' => route('admin.sections.info.store'),
                    'method' => 'POST',
                    'input' => [
                        'image' => true,
                        'image_width' => 540,
                        'image_height' => 308,
                        'content' => true,
                    ]
                ])

            @enduisegment


        </div>
    </div>

@endsection
