@extends('admin.layouts.app')

@section('title', __('Edit Card'))

@section('content')

    <div class="row">
        <div class="sixteen wide column">

            @uisegment([
                'class' => 'mb-20',
                'space' => true
            ])

                @slot('header')
                    <h5 class="ui header">{{ __('Edit Card') }}</h5>
                @endslot

                @sectionform([
                    'action' => route('admin.sections.feature.update', $section),
                    'method' => 'PUT',
                    'input' => [
                        'font_icon' => true,
                        'title' => true,
                        'subtitle' => true
                    ],
                    'section' => $section
                ])

            @enduisegment


        </div>
    </div>

@endsection
