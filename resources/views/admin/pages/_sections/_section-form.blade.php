@uiform([
    'action' => $action,
    'method' => $method
])

<input type="hidden" name="parent_id" value="{{ $parent_id ?? '' }}">
<input type="hidden" name="page_id" value="{{ $page_id ?? '' }}">

<div class="ui grid">

    <div class="row">
        <div class="sixteen wide column centered">

            @if(array_key_exists('image', $input))

                @uicropit([
                    'w' => $input['image_width'],
                    'h' => $input['image_height'],
                    'name' => 'image',
                    'label' => __('Image'),
                    'src' => $section->image_url ?? ''
                ])

            @endif

            @if(array_key_exists('basic_image', $input))

                @uiinputimage([
                    'w' => $input['image_width'],
                    'h' => $input['image_height'],
                    'name' => 'basic_image',
                    'label' => __('Image'),
                    'src' => $section->image_url ?? ''
                ])

            @endif

            @if(array_key_exists('font_icon', $input))

                @uiinput([
                    'label' => __('Font Icon'),
                    'name' => "font_icon",
                    'placeholder' => __('Icon'),
                    'value' => $section->font_icon ?? ''
                ])

            @endif

            @if(array_keys_exists(['title', 'subtitle', 'bg_text', 'content', 'content_editor'], $input))

                <div class="ui top attached tabular menu">
                    @foreach (config('translatable.locales') as $lang)
                        <a class="item {{ $loop->first ? 'active' : '' }}" data-tab="{{ $lang }}">{{ $lang }}</a>
                    @endforeach
                </div>

                @foreach (config('translatable.locales') as $lang)
                    <div class="ui fade attached tab segment {{ $loop->first ? 'active' : '' }} mb-20" data-tab="{{ $lang }}">

                        @if(array_key_exists('bg_text', $input))
                            @uiinput([
                                'label' => __('Background Text'),
                                'name' => "translations[{$lang}][bg_text]",
                                'placeholder' => __('bg_text'),
                                'value' => isset($section) && $section->hasTranslation($lang) ? $section->translate($lang)->bg_text : old("translations.{$lang}.bg_text")
                            ])
                        @endif

                        @if(array_key_exists('title', $input))
                            @uiinput([
                                'label' => __('Title'),
                                'name' => "translations[{$lang}][title]",
                                'placeholder' => __('Title'),
                                'value' => isset($section) && $section->hasTranslation($lang) ? $section->translate($lang)->title : old("translations.{$lang}.title")
                            ])
                        @endif

                        @if(array_key_exists('subtitle', $input))
                            @uiinput([
                                'label' => __('Subtitle'),
                                'name' => "translations[{$lang}][subtitle]",
                                'placeholder' => __('Subtitle'),
                                'value' => isset($section) && $section->hasTranslation($lang) ? $section->translate($lang)->subtitle : old("translations.{$lang}.subtitle")
                            ])
                        @endif

                        @if(array_key_exists('content_editor', $input))
                            @uieditor([
                                'label' => __('Content'),
                                'name' => "translations[{$lang}][content]",
                                'placeholder' => __('Content'),
                                'value' => isset($section) && $section->hasTranslation($lang) ? $section->translate($lang)->content : old("translations.{$lang}.content")
                            ])
                        @endif

                        @if(array_key_exists('content', $input))
                            @uitextarea([
                                'label' => __('Content'),
                                'name' => "translations[{$lang}][content]",
                                'placeholder' => __('Content'),
                                'value' => isset($section) && $section->hasTranslation($lang) ? $section->translate($lang)->content : old("translations.{$lang}.content")
                            ])
                        @endif


                    </div>
                @endforeach

            @endif


        </div>
    </div>

</div>

@enduiform
