@uisegment([
    'class' => 'mb-20',
    'space' => true
])

    @slot('header')
        <h5 class="ui header">{{ __('Info') }}</h5>
    @endslot


    @sectionform([
        'action' => route('admin.sections.update', $section),
        'method' => 'PUT',
        'input' => [
            'bg_text' => true,
            'title' => true,
            'subtitle' => true
        ],
        'page_id' => $section->page_id,
        'section' => $section
    ])

    @uisegment([
        'class' => 'mb-20',
        'header' => __('Cards')
    ])

        @uisortable

            @foreach ($section->childs as $item)

                @uisortableitem
                    <img src="{{ $item->image_url }}" alt="" style="margin-right: 20px;">

                    <div class="text">
                        <p class="subtitle" style="">{!! $item->content ?? '' !!}</p>
                    </div>

                    <div class="action">
                        @uibtnedit(['action' => route('admin.sections.info.edit', $item)])
                        @uibtndelete(['action' => route('admin.sections.info.destroy', $item)])
                    </div>
                @enduisortableitem

            @endforeach

            <a href="{{ route('admin.sections.info.create', [
                    'parent_id' => $section->id,
                    'page_id' => $section->page->id
                ]) }}"
                class="fluid ui button">
                {{ __('Add Card') }}
           </a>

        @enduisortable

    @enduisegment


@enduisegment
