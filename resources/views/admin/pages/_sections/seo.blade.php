@uisegment([
    'class' => 'mb-20',
    'space' => true
])

    @slot('header')
        <h5 class="ui header">{{ __('SEO Information') }}</h5>
    @endslot

    @uiform([
    'action' => route('admin.pages.update', $page),
    'method' => 'POST'
    ])

    <div class="ui grid">

        <div class="row">
            <div class="sixteen wide column centered">

                @uicropit([
                    'w' => 800,
                    'h' => 600,
                    'name' => 'image',
                    'label' => __('Image'),
                    'src' => $page->image_url
                ])

                <div class="ui top attached tabular menu">
                    @foreach (config('translatable.locales') as $lang)
                        <a class="item {{ $loop->first ? 'active' : '' }}" data-tab="{{ $lang }}">{{ $lang }}</a>
                    @endforeach
                </div>

                @foreach (config('translatable.locales') as $lang)
                    <div class="ui fade attached tab segment {{ $loop->first ? 'active' : '' }} mb-20" data-tab="{{ $lang }}">

                        @uiinput([
                            'label' => __('Name'),
                            'name' => "translations[{$lang}][name]",
                            'placeholder' => __('Name'),
                            'value' => $page->name ?? ''
                        ])

                        @uiinput([
                            'label' => __('Meta Title'),
                            'name' => "translations[{$lang}][meta_title]",
                            'placeholder' => __('Meta Title'),
                            'value' => $page->meta_title ?? ''
                        ])

                        @uitextarea([
                            'label' => __('Meta Description'),
                            'name' => "translations[{$lang}][meta_description]",
                            'placeholder' => __('Meta Description'),
                            'value' => $page->meta_description ?? ''
                        ])
                    </div>
                @endforeach

            </div>
        </div>

    </div>

    @enduiform

@enduisegment
