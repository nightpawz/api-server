@uisegment([
    'class' => 'mb-20',
    'space' => true
])

    @slot('header')
        <h5 class="ui header">{{ __('Partners') }}</h5>
    @endslot


    @sectionform([
        'action' => route('admin.sections.update', $section),
        'method' => 'PUT',
        'input' => [
            'bg_text' => true,
            'title' => true,
            'subtitle' => true
        ],
        'page_id' => $section->page_id,
        'section' => $section
    ])

    @uisegment([
        'class' => 'mb-20',
        'header' => __('Partners')
    ])

        @uisortable

            @foreach (Modules\Eris\Entitis\Partner::all() as $item)

                @uisortableitem
                    <img src="{{ $partner->image_url }}" alt="" style="margin-right: 20px;">

                    <div class="text">
                        <p class="title">{{ $partner->name ?? '' }}</p>
                    </div>

                    <div class="action" style="min-width: 100px;">
                        @uibtnedit(['action' => route('admin.sections.feature.edit', $item)])
                        @uibtndelete(['action' => route('admin.sections.feature.destroy', $item)])
                    </div>
                @enduisortableitem

            @endforeach

            <a href="{{ route('admin.sections.feature.create', [
                    'parent_id' => $section->id,
                    'page_id' => $section->page->id
                ]) }}"
                class="fluid ui button">
                {{ __('Add Card') }}
           </a>

        @enduisortable

    @enduisegment


@enduisegment
