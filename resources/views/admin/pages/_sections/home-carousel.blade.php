@uisegment([
    'class' => 'mb-20',
    'space' => true
])

    @slot('header')
        <h5 class="ui header">{{ __('Carousel') }}</h5>
    @endslot


    @sectionform([
        'action' => route('admin.sections.update', $section),
        'method' => 'PUT',
        'input' => [
            'title' => true,
            'subtitle' => true
        ],
        'page_id' => $section->page_id,
        'section' => $section
    ])

    @uisegment([
        'class' => 'mb-20',
        'header' => __('Carousel Item')
    ])

        @uisortable

            @foreach ($section->childs as $item)

                @uisortableitem
                    <img src="{{ $item->image_url }}" alt="">

                    <div class="text">
                        <p class="title">{{ $item->title ?? '' }}</p>
                        <p class="subtitle">{{ $item->subtitle ?? '' }}</p>
                    </div>

                    <div class="action">
                        @uibtnedit(['action' => route('admin.sections.carousel.edit', $item)])
                        @uibtndelete(['action' => route('admin.sections.carousel.destroy', $item)])
                    </div>
                @enduisortableitem

            @endforeach

            <a href="{{ route('admin.sections.carousel.create', [
                    'parent_id' => $section->id,
                    'page_id' => $section->page->id
                ]) }}"
                class="fluid ui button">
                {{ __('Add Image') }}
           </a>

        @enduisortable

    @enduisegment


@enduisegment
