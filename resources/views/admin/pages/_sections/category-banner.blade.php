@uisegment([
    'class' => 'mb-20',
    'space' => true
])

    @slot('header')
        <h5 class="ui header">{{ __('Banner') }}</h5>
    @endslot


    @sectionform([
        'action' => route('admin.sections.update', $section),
        'method' => 'PUT',
        'input' => [
            'image' => true,
            'image_width' => 1583,
            'image_height' => 333,
            'title' => true,
            'subtitle' => true
        ],
        'page_id' => $section->page_id,
        'section' => $section
    ])


@enduisegment
