@uisegment([
    'class' => 'mb-20',
    'space' => true
])

    @slot('header')
        <h5 class="ui header">{{ __('Listing') }}</h5>
    @endslot


    @sectionform([
        'action' => route('admin.sections.update', $section),
        'method' => 'PUT',
        'input' => [
            'bg_text' => true,
            'title' => true,
            'subtitle' => true
        ],
        'page_id' => $section->page_id,
        'section' => $section
    ])


@enduisegment
