@extends('admin.layouts.app')

@section('title', __('Categories'))

@section('content')

<div class="row">
    <div class="sixteen wide column">

        @uisegment

            @slot('header')
                <h5 class="ui header">{{ __('Categories') }}</h5>
                <a href="{{ route('admin.categories.create') }}" class="ui basic button button-right">
                    <i class="fa fa-plus"></i>
                    {{ __('Add Category') }}
                </a>
            @endslot

            @uidatatable([
                'url' => route('admin.categories.datatable'),
                'columns' => [
                    '#' => 'DT_RowIndex',
                    __('Thumbnail') => 'thumbnail',
                    __('Name') => 'name',
                    __('Action') => 'action'
                ]
            ])

        @enduisegment

    </div>
</div>

@endsection
