@extends('admin.layouts.app')

@section('title', __('Category') . $category->name)

@section('content')

<div class="row">
    <div class="sixteen wide column">

        @foreach (['banners', 'subcategories'] as $key)

            @include('admin.categories._partials.' . $key)

        @endforeach

    </div>
</div>

@endsection
