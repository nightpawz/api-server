@uisegment
    @slot('header')
        <h5 class="ui header">{{ __('Subcategories') }}</h5>
        <a href="{{ route('admin.categories.create', ['parent_id' => $category->id]) }}" class="ui basic button button-right">
            <i class="fa fa-plus"></i>
            {{ __('Add Subcategory') }}
        </a>
    @endslot

    @uidatatable([
        'url' => route('admin.subcategories.datatable', ['parent_id' => $category->id]),
        'columns' => [
            '#' => 'DT_RowIndex',
            // __('Thumbnail') => 'thumbnail',
            __('Name') => 'name',
            __('Action') => 'action'
        ]
    ])
@enduisegment
