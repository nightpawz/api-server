@uisegment
    @slot('header')
        <h5 class="ui header">{{ __('Banner') }}</h5>
    @endslot

    <div class="ui grid">
        <div class="row">

            @php
                $group_data = [
                    'top' => [
                        'h' => 350,
                        'w' => 1110
                    ],
                    'side' => [
                        'w' => 255,
                        'h' => 539
                    ]
                ];
            @endphp

            @foreach ($group_data as $group => $data)
                <div class="eight wide column">

                    @uisegment
                        @slot('header')
                            <h5 class="ui header">{{ __(ucfirst($group) . " Banner") }}</h5>
                        @endslot

                        @uisortable

                            @foreach ($category->getBanners($group) as $item)

                                @uisortableitem
                                    <img src="{{ $item->image_url }}" alt="">

                                    <div class="text">
                                        <p>{{ __('Expired Date') }} {{ $item->expired_date }}</p>
                                    </div>

                                    <div class="action">
                                        @uibtnedit(['action' => route('admin.banners.edit', $item)])
                                        @uibtndelete(['action' => route('admin.banners.destroy', $item)])
                                    </div>
                                @enduisortableitem

                            @endforeach

                            <a href="{{ route('admin.banners.create', [
                                'category_id' => $category->id,
                                'group' => $group,
                                'h' => $data['h'],
                                'w' => $data['w']
                            ]) }}"
                                class="fluid ui button">
                                {{ __('Add Image') }}
                           </a>

                        @enduisortable

                    @enduisegment

                </div>
            @endforeach

        </div>
    </div>

@enduisegment
