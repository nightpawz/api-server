@extends('admin.layouts.app')

@section('title', __('Edit Category'))

@section('content')

<div class="row">
    <div class="sixteen wide column">

        @uisegment

        @slot('header')
            <h5 class="ui header">{{ __('Edit Category') }}</h5>
        @endslot

        @uiform([
            'action' => route('admin.categories.update', $category),
            'method' => 'PUT',
            'file' => true
        ])

        <div class="ui grid">

            <div class="row">
                <div class="six wide column">
                    @uicropit([
                        'w' => 300,
                        'h' => 300,
                        'name' => 'image',
                        'label' => __('Thumbnail'),
                        'src' => $category->image_url
                    ])

                    @uiinputimage([
                        'w' => 300,
                        'h' => 300,
                        'name' => 'icon',
                        'label' => __('Icon'),
                        'src' => $category->icon_url
                    ])
                </div>

                <div class="ten wide column">

                    <div class="field">

                        @uisearch([
                            'name' => 'parent_id',
                            'placeholder' => __('Parent Category'),
                            'url' => route('admin.api.categories.index', ['parent' => 1]),
                            'value' => $category->parent_id ?? '',
                            'value_text' => $category->parent->name ?? ''
                        ])

                        <br>
                        <label>{{ __('Category Information') }}</label>

                        <div class="ui top attached tabular menu">
                            @foreach (config('translatable.locales') as $lang)
                                <a class="item {{ $loop->first ? 'active' : '' }}" data-tab="{{ $lang }}">{{ $lang }}</a>
                            @endforeach
                        </div>

                        @foreach (config('translatable.locales') as $lang)
                            <div class="ui bottom attached tab segment {{ $loop->first ? 'active' : '' }}" data-tab="{{ $lang }}">
                                @uiinput([
                                    'name' => "translations[{$lang}][name]",
                                    'placeholder' => __('Name'),
                                    'value' => $category->hasTranslation($lang) ? $category->translate($lang)->name : ''
                                ])
                            </div>
                        @endforeach
                    </div>

                </div>

            </div>

        </div>

        @enduiform

        @enduisegment

    </div>
</div>

@endsection
