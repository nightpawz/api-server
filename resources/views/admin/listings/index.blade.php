@extends('admin.layouts.app')

@section('title', __('Listings'))

@section('content')

<div class="row">
    <div class="sixteen wide column">

        @uisegment

            @slot('header')
                <h5 class="ui header">{{ __('Listings') }}</h5>
                <a href="{{ route('admin.listings.create') }}" class="ui basic button button-right">
                    <i class="fa fa-plus"></i>
                    {{ __('Add Listing') }}
                </a>
            @endslot

            @uidatatable([
                'url' => route('admin.listings.datatable'),
                'columns' => [
                    '#' => 'DT_RowIndex',
                    __('Thumbnail') => 'thumbnail',
                    __('Name') => 'name',
                    __('Owner') => 'owner',
                    __('Contact') => 'contact',
                    __('Expired Date') => 'expired_date',
                    __('Action') => 'action'
                ]
            ])

        @enduisegment

    </div>
</div>

@endsection
