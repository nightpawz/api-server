@extends('admin.layouts.app')

@section('title', __('Create Listings'))

@section('content')

<div class="row">
    <div class="sixteen wide column">

        @uisegment

        @slot('header')
            <h5 class="ui header">{{ __('Create Listing') }}</h5>
        @endslot

        @uiform([
            'action' => route('admin.listings.store'),
            'method' => 'POST',
            'file' => true
        ])

        <div class="ui grid">

            <div class="row">
                <div class="ten wide column">
                    @uicropit([
                        'w' => 1583,
                        'h' => 584,
                        'name' => 'listing[banner]',
                        'label' => __('Banner')
                    ])
                </div>

                <div class="six wide column">
                    @uicropit([
                        'w' => 470,
                        'h' => 300,
                        'name' => 'listing[thumbnail]',
                        'label' => __('Thumbnail')
                    ])
                </div>
            </div>

            <div class="row">
                <div class="ten wide column">
                    @include('admin.listings._partials.listing-information')
                </div>
                <div class="six wide column">

                    <div class="field">
                        {{-- @include('admin.listings._partials.owner-information') --}}
                    </div>

                    <div class="field mt-20" style="margin-top: 20px">
                        {{-- @include('admin.listings._partials.deal-information') --}}
                    </div>


                </div>

            </div>
        </div>



        @enduiform


        @enduisegment

    </div>
</div>

@endsection
