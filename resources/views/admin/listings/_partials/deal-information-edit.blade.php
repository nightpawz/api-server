@uisegment([
    'header' => __('Deal Information')
])

    {{-- @php
        $deal = Modules\JB\Entities\Deal::where([
            'dealable_type' => 'Modules\JB\Entities\Listing',
            'dealable_id' => $listing->id
        ])->first();
    @endphp --}}

    {{-- <input type="hidden" name="deal_id" value="{{ $deal->id ?? '' }}"> --}}

    <div class="two fields">

        {{-- @uisearch([
            'name' => 'deal[sales_id]',
            'placeholder' => __('Sales'),
            'url' => route('admin.api.users.index', ['role' => 'sales']),
            'value' => $deal->sales_id ?? '',
            'value_text' => $deal->sales_name ?? ''
        ])

        <div class="field">
            <a href="" class="ui button fluid">{{ __('Add Sales') }}</a>
        </div> --}}
    </div>

    @uidaterange([
        'label' => __('Display Date'),
        'name_start' => 'display_date',
        'name_end' => 'expired_date',
        'value_start' => $listing->register_date,
        'value_end' => $listing->expired_date
    ])

    @uiinput([
        'name' => 'listing[no_contract]',
        'placeholder' => __('No. Contract'),
        'label' => __('No. Contract'),
        'value' => $listing->no_contract ?? ''
    ])

    @uiinput([
        'name' => 'listing[hit_count]',
        'placeholder' => __('Hits'),
        'label' => __('Hits'),
        'type' => 'number',
        'value' => $listing->hit_count ?? ''
    ])

    {{-- <div class="field">
        @uiinputgroup([
            'class' => 'js__input-listing-price',
            'name' => 'deal[price]',
            'type' => 'number',
            'label_right' => 'Rp.',
            'label_left' => ', 00-',
            'value' => $deal->item_price ?? 0
        ])
    </div> --}}
@enduisegment
