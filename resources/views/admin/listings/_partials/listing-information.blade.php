@uisegment([
    'header' => __('Listing Information')
])

<div class="field">

    @uiinputimage([
        'name' => 'listing[logo]',
        'label' => __('Logo'),
        'src' => $listing->logo_url ?? ''
    ])

    <div class="two fields">

        @uiinput([
            'name' => 'listing[name]',
            'placeholder' => __('Name'),
            'value' => $listing->name ?? old('listing.name')
        ])

        @uisearch([
            'name' => 'listing[category_id]',
            'placeholder' => __('Category'),
            'url' => route('admin.api.categories.index', ['childs' => 1]),
            'value' => $listing->category_id ?? old('listing.category_id'),
            'value_text' => $listing->category->name ?? ''
        ])

    </div>

    <div class="ui top attached tabular menu">
        @foreach (config('translatable.locales') as $lang)
            <a class="item {{ $loop->first ? 'active' : '' }}" data-tab="{{ $lang }}">{{ $lang }}</a>
        @endforeach
    </div>

    @foreach (config('translatable.locales') as $lang)
        <div class="ui bottom attached tab segment {{ $loop->first ? 'active' : '' }}" data-tab="{{ $lang }}">
            @uieditor([
                'name' => "listing[translations][{$lang}][description]",
                'placeholder' => __('Description'),
                'value' => isset($listing) && $listing->hasTranslation($lang) ? $listing->translate($lang)->description : old("listing.translations.{$lang}.description")
            ])
        </div>
    @endforeach
</div>

<div class="field">
    <label>{{ __('Contact Information') }}</label>
    <div class="two fields">

        @uiinput([
            'name' => 'listing[phone]',
            'placeholder' => __('Phone'),
            'value' => $listing->phone ?? old('listing.phone')
        ])

        @uiinput([
            'name' => 'listing[email]',
            'placeholder' => __('Email'),
            'type' => 'email',
            'value' => $listing->email ?? old('listing.email')
        ])

    </div>

    <div class="two fields">

        @uitextarea([
            'name' => 'listing[address]',
            'placeholder' => __('Address'),
            'rows' => 4,
            'value' => $listing->address ?? old('listing.address')
        ])

        <div class="field">
            @uiinput([
                'name' => 'listing[latlong]',
                'placeholder' => __('Google Maps Place Id'),
                'class' => 'mb-1em',
                'value' => $listing->latlong ?? old('listing.latlong')
            ])

            @uiinput([
                'name' => 'listing[site_url]',
                'placeholder' => __('Website'),
                'value' => $listing->site_url ?? old('listing.site_url')
            ])
        </div>

    </div>
</div>

@enduisegment
