@uisegment([
    'header' => __('Owner Information')
])

    <div class="two fields">

        @uisearch([
            'name' => 'listing[owner_id]',
            'placeholder' => __('Owner'),
            'url' => route('admin.api.members.index'),
            'value' => $listing->owner->id ?? '',
            'value_text' => $listing->owner->name ?? ''
        ])

        <div class="field">
            <a target="_blank" href="{{ route('admin.members.create') }}" class="ui button fluid">{{ __('Add Owner') }}</a>
        </div>
    </div>

@enduisegment
