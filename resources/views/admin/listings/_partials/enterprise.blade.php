@uisegment

    @slot('header')
        <h5 class="ui header">{{ __('Top List') }}</h5>
        <a href="{{ route('admin.enterprises.create') }}" class="ui basic button button-right">
            <i class="fa fa-plus"></i>
            {{ __('Add Item') }}
        </a>
    @endslot

    @uidatatable([
        'url' => route('admin.enterprises.datatable'),
        'columns' => [
            '#' => 'DT_RowIndex',
            __('Name') => 'listing',
            __('Expired Date') => 'expired_date',
            __('Action') => 'action'
        ]
    ])

@enduisegment
