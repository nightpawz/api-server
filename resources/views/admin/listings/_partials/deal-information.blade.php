@uisegment([
    'header' => __('Deal Information')
])

    <div class="two fields">

        @uisearch([
            'name' => 'deal[sales_id]',
            'placeholder' => __('Sales'),
            'url' => route('admin.api.users.index', ['role' => 'sales'])
        ])

        <div class="field">
            <a target="_blank" href="{{ route('admin.sales.create') }}" class="ui button fluid">{{ __('Add Sales') }}</a>
        </div>
    </div>

    @uiinput([
        'name' => 'listing[no_contract]',
        'placeholder' => __('No. Contract'),
        'value' => $listing->no_contract ?? ''
    ])

    <div class="field">
        @php
            $price_lists = Modules\JB\Entities\PriceList::where('group', 'jb-listing-price')->get();
        @endphp

        @uiselect([
            'label' => __('Duration'),
            'name' => 'deal[price_list_id]',
            'options' => mapUiSelectArray($price_lists, 'id', 'text')
        ])
    </div>

    <div class="field">
        @uicheckbox([
            'input_class' => 'js__input-listing-price-toggler',
            'label' => __('Input Price Manually'),
            'name' => 'deal[custom_price]'
        ])

        @uiinputgroup([
            'class' => 'js__input-listing-price d-none',
            'name' => 'deal[price]',
            'type' => 'number',
            'label_right' => 'Rp.',
            'label_left' => ', 00-'
        ])
    </div>
@enduisegment

@push('footer')
    <script type="text/javascript">
        $('.js__input-listing-price-toggler').on('change', function() {
            if(this.checked) {
                $('.js__input-listing-price').show()
            } else {
                $('.js__input-listing-price').hide()
            }
        })
    </script>
@endpush
