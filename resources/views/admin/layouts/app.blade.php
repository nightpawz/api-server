@extends('ui::layout')

@section('logo', asset(setting('app.logo')))

@push('sidebar')

    <a href="{{ route('admin.dashboard.index') }}" class="item"><i class="fa fa-tachometer"></i> {{ __('Dashboard') }}</a>

    <div class="item">
        <div class="header"><i class="fa fa-bookmark"></i> {{ __('Pages') }}</div>
        <div class="menu">
            <a href="{{ route('admin.pages.home') }}" class="item">{{ __('Home') }}</a>
            <a href="{{ route('admin.pages.category') }}" class="item">{{ __('Category') }}</a>
            <a href="{{ route('admin.pages.about') }}" class="item">{{ __('About') }}</a>
            <a href="{{ route('admin.pages.contact') }}" class="item">{{ __('Contact') }}</a>
        </div>
    </div>

    <div class="item">
        <div class="header"><i class="fa fa-bookmark"></i> {{ __('Business') }}</div>
        <div class="menu">
            <a href="{{ route('admin.plans.index') }}" class="item">{{ __('Plan') }}</a>
            <a href="{{ route('admin.categories.index') }}" class="item">{{ __('Categories') }}</a>
            <a href="{{ route('admin.listings.index') }}" class="item">{{ __('Listings') }}</a>
            <a href="{{ route('admin.members.index') }}" class="item">{{ __('Members') }}</a>
            <a href="{{ route('admin.sales.index') }}" class="item">{{ __('Sales') }}</a>
        </div>
    </div>

    <div class="item">
        <div class="header"><i class="fa fa-bookmark"></i> {{ __('System') }}</div>
        <div class="menu">
            <a href="{{ route('admin.settings.index') }}" class="item">{{ __('Settings') }}</a>
        </div>
    </div>

@endpush
