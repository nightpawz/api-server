@extends('admin.layouts.app')

@section('title', __('Top List'))

@section('content')

    <div class="row">
        <div class="sixteen wide column">

            @uisegment([
                'class' => 'mb-20',
                'space' => true
            ])

                @slot('header')
                    <h5 class="ui header">{{ __('Add Item') }}</h5>
                @endslot

                @uiform([
                    'action' => route('admin.enterprises.store'),
                    'method' => 'POST',
                ])
                    <input type="hidden" name="group" value="{{ Request::input('group') }}">

                    @uiselect([
                        'name' => 'listing_id',
                        'placeholder' => __('Select Listing'),
                        'options' => mapUiSelectArray($listings, 'id', 'name', 'image_url'),
                        'label' => __('Select Listing')
                    ])

                    @uidaterange([
                        'label' => __('Display Date'),
                        'name_start' => 'display_date',
                        'name_end' => 'expired_date'
                    ])

                @enduiform

            @enduisegment


        </div>
    </div>

@endsection
