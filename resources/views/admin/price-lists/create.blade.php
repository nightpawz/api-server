@extends('admin.layouts.app')

@section('title', __('Create Price List'))

@section('content')

<div class="row">
    <div class="sixteen wide column">

        @uisegment(['space' => true])

        @slot('header')
            <h5 class="ui header">{{ __('Create Price List') }}</h5>
        @endslot

            @uiform([
                'action' => route('admin.price-lists.store'),
                'method' => 'POST'
            ])

                @include('admin.price-lists._partials.form')

            @enduiform


        @enduisegment

    </div>
</div>

@endsection
