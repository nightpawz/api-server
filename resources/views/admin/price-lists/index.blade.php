@extends('admin.layouts.app')

@section('title', __('Price Lists'))

@section('content')

<div class="row">
    <div class="sixteen wide column">

        @uisegment

            @slot('header')
                <h5 class="ui header">{{ __('Price Lists') }}</h5>
                <a href="{{ route('admin.price-lists.create') }}" class="ui basic button button-right">
                    <i class="fa fa-plus"></i>
                    {{ __('Add Price') }}
                </a>
            @endslot

            @uidatatable([
                'url' => route('admin.price-lists.datatable'),
                'columns' => [
                    '#' => 'DT_RowIndex',
                    __('Group') => 'group',
                    __('Duration') => 'duration',
                    __('Unit') => 'unit',
                    __('Price') => 'price',
                    __('Action') => 'action'
                ]
            ])

        @enduisegment

    </div>
</div>

@endsection
