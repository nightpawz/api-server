@uiinput([
    'name' => 'group',
    'label' => __('Group'),
    'value' => 'jb-listing-price'
])

@uiinput([
    'name' => 'duration',
    'label' => __('Duration'),
    'type' => 'number',
    'value' => $price_list->duration ?? old('duration')
])

@uiselect([
    'name' => 'unit',
    'label' => __('Unit'),
    'value' => $price_list->unit ?? old('unit'),
    'options' => [
        [
            'id' => 'day',
            'text' => __('Day')
        ],
        [
            'id' => 'month',
            'text' => __('Month')
        ]
    ]
])

@uiinput([
    'name' => 'price',
    'label' => __('Price'),
    'type' => 'number',
    'value' => $price_list->price ?? old('price')
])
