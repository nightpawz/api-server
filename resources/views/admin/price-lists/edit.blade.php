@extends('admin.layouts.app')

@section('title', __('Edit Price List'))

@section('content')

<div class="row">
    <div class="sixteen wide column">

        @uisegment(['space' => true])

        @slot('header')
            <h5 class="ui header">{{ __('Edit Price List') }}</h5>
        @endslot

            @uiform([
                'action' => route('admin.price-lists.update', $price_list),
                'method' => 'PUT'
            ])

                @include('admin.price-lists._partials.form')

            @enduiform


        @enduisegment

    </div>
</div>

@endsection
