import Swiper from 'swiper';

$(document).ready(function () {
    //initialize swiper when document ready
    var feSwiper = new Swiper ('.swiper-container', {
        slidesPerView: 4,
        spaceBetween: 15,
        breakpoints: {
            320: {
                slidesPerView: 1,        
                spaceBetween: 5
            },
            480: {
                slidesPerView: 1,        
                spaceBetween: 5
            },
            640: {
                slidesPerView: 2,        
                spaceBetween: 5
            },
            768: {
                slidesPerView: 2,        
                spaceBetween: 10
            },
            1024: {
                slidesPerView: 3
            },
            1366: {
                slidesPerView: 4
            },
            1600: {
                slidesPerView: 4
            },
            1920: {
                slidesPerView: 5
            }
        }
    })

    
    $(".swiper-control-prev").click(function(e){
        e.preventDefault();
        // Swipes to the next slide
        feSwiper.slidePrev();
    });

    $(".swiper-control-next").click(function(e){
        e.preventDefault();
        // Swipes to the next slide
        feSwiper.slideNext();
    });

});