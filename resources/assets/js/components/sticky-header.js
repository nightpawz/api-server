// When the user scrolls the page, execute stickyNav
window.onscroll = function() {
    stickyNav()
};

// Get the navbar
var navbar = document.getElementById("sticky-navbar")

// Add the sticky class to the navbar when you reach its scroll position. Remove "sticky" when you leave the scroll position, offset is in pixels
function stickyNav() {
  if (window.pageYOffset >= 100) {
    navbar.classList.add("sticky")
  } else {
    navbar.classList.remove("sticky");
  }
}