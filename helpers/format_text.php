<?php

/**
 * Format number into IDR Currency.
 *
 * @param int
 * @return string
*/
function format_idr($int)
{
    $int = number_format($int, 0, ",", ".");

    return "Rp. $int";
}

 ?>
