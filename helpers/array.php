<?php

/**
 * Block comment
 *
 * @param type
 * @return void
*/
function mapUiSelectArray($collections, $id, $text, $image_url = '')
{
    $a = [];

    foreach ($collections as $i) {
        $a[] = [
            'id' => $i[$id] ?? '',
            'text' => $i[$text] ?? '',
            'image_url' => $i[$image_url] ?? ''
        ];
    }

    return $a;
}

function array_keys_exists_all($keys, $array) {
    if (count(array_intersect_key(array_flip(), $array)) === count($keys)) {
        return true;
    }

    return false;
}

function array_keys_exists($keys, $array) {
    foreach ($keys as $key) {
        if(array_key_exists($key, $array)) {
            return true;
        }
    }
    return false;
}

?>
