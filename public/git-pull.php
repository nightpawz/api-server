<?php

ignore_user_abort(true);

const DISCORD_CHANNEL = 'https://discordapp.com/api/webhooks/539291498097803292/M9Jz2oBS2MpZJ3auLLQOh5h0CFd-iK2-tDXIMJl2V9zU2MQrBRM64U3dPYmc3v_wNudx';

/**
 * Send log message to discord channel
 * @param String $message
 */
function sendToDiscord($message)
{
    sleep(1);

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, DISCORD_CHANNEL);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, ['content' => $message]);
    curl_setopt($ch, CURLOPT_HTTPHEADER, [
        "Length" => strlen( $message ),
        "Content-Type" => "application/json"
    ]);
    // receive server response ...
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $server_output = curl_exec ($ch);

    curl_close ($ch);
}

function parseToDiscordMessage($string)
{
    $messages = str_split($string, 1500);

    foreach($messages as $message) {
        sendToDiscord('```'.$message.'```');
    }
}

function syscall ($cmd, $cwd) {
    $descriptorspec = array(
        1 => array('pipe', 'w'), // stdout is a pipe that the child will write to
        2 => array('pipe', 'w') // stderr
    );
    $resource = proc_open($cmd, $descriptorspec, $pipes, $cwd);
    if (is_resource($resource)) {
        $output = stream_get_contents($pipes[2]);
        $output .= PHP_EOL;
        $output .= stream_get_contents($pipes[1]);
        $output .= PHP_EOL;
        fclose($pipes[1]);
        fclose($pipes[2]);
        proc_close($resource);
        return $output;
    }
}

function runCommandAndSend($cmd) {
    $cmd = sprintf($cmd);
    $result = syscall($cmd, '/home/fei/www/funel-imaginer');
    $output = '';

    echo($result);

    parseToDiscordMessage($result);
}

sendToDiscord("Updating http://preview.jogjabagus.id");

runCommandAndSend('git pull');

// runCommandAndSend('php artisan migrate:fresh');

// runCommandAndSend('php artisan module:seed');

echo("Finished");
?>
