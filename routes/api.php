<?php

Route::get('/ph/categories', 'PhantomController@categories');
Route::get('/ph/subcategories', 'PhantomController@subcategories');
Route::post('/ph/categories', 'PhantomController@storeCategory');
Route::post('/ph/categories-en', 'PhantomController@storeCategoryEn');
Route::post('/ph/listings', 'PhantomController@storeListing');
Route::post('/ph/listings/update', 'PhantomController@updateListing');
Route::get('/ph/listings', 'PhantomController@listings');
