<?php

Route::get('/', 'DashboardController@index')->name('dashboard.index');

/**
 * Datatable routes
 *
 * @param type
 * @return void
*/
$dt_resource = [
    'listings' => 'ListingController',
    'categories' => 'CategoryController',
    'enterprises' => 'EnterpriseController',
    'subcategories' => 'SubcategoryController',
    'price-lists' => 'PriceListController',
    'members' => 'MemberController',
    'sales' => 'SalesController'
];

foreach ($dt_resource as $key => $value) {
    Route::get("/{$key}/datatable", "$value@datatable")->name("{$key}.datatable");
}


/**
 * Static page routes
 *
 * @param type
 * @return void
*/
$static_pages = [
    'home',
    'category',
    'about',
    'contact'
];

foreach ($static_pages as $slug) {
    Route::get("/pages/{$slug}", "PageController@{$slug}")->name("pages.{$slug}");
}

Route::post("/pages/{page}", "PageController@update")->name("pages.update");
/**
 * Section resources routes
 *
 * @param type
 * @return void
*/

Route::group([
    'prefix' => "sections",
    'as' => "sections.",
    "namespace" => "Section"
], function() {

    $section_resources = [
        'carousel' => 'CarouselController',
        'feature' => 'FeatureController',
        'info' => 'InfoContentController'
    ];

    foreach ($section_resources as $key => $value) {

        Route::get("/{$key}/create", "{$value}@create")->name("{$key}.create");
        Route::post("/{$key}/create", "{$value}@store")->name("{$key}.store");
        Route::get("/{$key}/edit/{section}", "{$value}@edit")->name("{$key}.edit");
        Route::put("/{$key}/update/{section}", "{$value}@update")->name("{$key}.update");
        Route::delete("/{$key}/destroy/{section}", "{$value}@destroy")->name("{$key}.destroy");

    }

});

Route::put('sections/{section}', 'SectionController@update')->name('sections.update');


/**
 * Setting routes
 *
 * @param type
 * @return void
*/
Route::get('/settings', 'SettingController@index')->name('settings.index');
Route::post('/settings', 'SettingController@store')->name('settings.store');


/**
 * Resources Rutes
 *
 * @param type
 * @return void
*/
Route::resources([
    'categories' => 'CategoryController',
    'listings' => 'ListingController',
    'members' => 'MemberController',
    'enterprises' => 'EnterpriseController',
    'banners' => 'BannerController',
    'partners' => 'PartnerController',
    'price-lists' => 'PriceListController',
    'sales' => 'SalesController',
    'plans' => 'PlanController'
]);
