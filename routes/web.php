<?php

Route::get('/', 'PageController@index')->name('home');
Route::get('/about-us', 'PageController@about')->name('about');
Route::get('/contact-us', 'PageController@contact')->name('contact');
Route::post('/contact-us', 'PageController@storeMessage')->name('contact.store');

Route::get('/categories', 'CategoryController@index')->name('categories.index');
Route::get('/categories/{category}', 'CategoryController@show')->name('categories.show');
// Route::get('/categories/{category}/{subcategory}', 'CategoryController@subcategory')->name('categories.show');
Route::get('/listings', 'ListingController@index')->name('listings.index');
Route::get('/listings/category/{slug}', 'ListingController@category')->name('listings.category');
Route::get('/listings/location/{slug}', 'ListingController@location')->name('listings.location');
Route::get('/listings/{slug}', 'ListingController@show')->name('listings.show');
